FROM gradle:8.6-jdk21
WORKDIR ./app
COPY ./ ./
RUN gradle clean build
ENTRYPOINT ["java", "-jar", "build/libs/MimosaCMS.jar"]