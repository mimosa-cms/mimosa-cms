package ua.com.webbrains.mimosa.cms.models.security;

import lombok.Builder;

import java.util.List;

@Builder
public record RolesAndPermissions(
        List<String> admin,
        List<String> moderator,
        List<String> user,
        List<String> guest,
        List<String> emailApproved
) {
}
