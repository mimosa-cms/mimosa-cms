package ua.com.webbrains.mimosa.cms.configs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.services.AdminService;
import ua.com.webbrains.mimosa.cms.services.CategoryService;

@Component
@RequiredArgsConstructor
@Slf4j
public class StartupDataConfig implements ApplicationListener<ContextRefreshedEvent> {
    private final AdminService adminService;
    private final CategoryService categoryService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        setupAdmin();
        setupCategories();
    }

    private void setupAdmin() {
        adminService.setupApplicationAdmin();
        log.info("Admin has been loaded successfully");
    }

    private void setupCategories() {
        categoryService.loadCategories();
        log.info("Categories has been loaded successfully");
    }
}
