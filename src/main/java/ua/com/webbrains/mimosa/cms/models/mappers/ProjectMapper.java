package ua.com.webbrains.mimosa.cms.models.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;

@Mapper(componentModel = "spring")
public interface ProjectMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "owner", ignore = true)
    @Mapping(target = "coworkers", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "posts", ignore = true)
    Project dtoToEntity(CreateProjectRequestDto dto);

    @Mapping(target = "ownerUsername", source = "entity.owner.username")
    ProjectResponseDto entityToDto(Project entity, Integer postsAmount, Integer maxPostsAmount);
}
