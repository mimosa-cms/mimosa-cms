package ua.com.webbrains.mimosa.cms.services;

/**
 * Service for handling admin operations
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface AdminService {
    void setupApplicationAdmin();
}
