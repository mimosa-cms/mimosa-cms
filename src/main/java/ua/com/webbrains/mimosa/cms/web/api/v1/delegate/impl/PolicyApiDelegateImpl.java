package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.PolicyApiDelegate;
import ua.com.webbrains.mimosa.cms.services.PolicyService;

@Component
@RequiredArgsConstructor
public class PolicyApiDelegateImpl implements PolicyApiDelegate {
    private final PolicyService policyService;

    @Override
    public ResponseEntity<String> getPrivacyPolicy(String language) {
        return ResponseEntity.ok(policyService.getPrivacyPolicy(language));
    }
}
