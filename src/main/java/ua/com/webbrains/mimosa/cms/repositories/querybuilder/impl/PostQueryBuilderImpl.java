package ua.com.webbrains.mimosa.cms.repositories.querybuilder.impl;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.PostFilterParameter;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.PostSortParameter;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;
import ua.com.webbrains.mimosa.cms.repositories.querybuilder.PostQueryBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static ua.com.webbrains.mimosa.cms.models.parameters.impl.Direction.ASC;
import static ua.com.webbrains.mimosa.cms.models.parameters.impl.Direction.DESC;

@Component
@RequiredArgsConstructor
public class PostQueryBuilderImpl implements PostQueryBuilder {
    private static final String PROJECT_ID_FIELD_NAME = "projectId";

    @Override
    public CriteriaQuery<Post> buildSearchProcessor(CriteriaBuilder cb, UUID projectId, Map<PostFilterParameter, String> filterParameters) {
        CriteriaQuery<Post> query = cb.createQuery(Post.class);
        Root<Post> root = query.from(Post.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb.equal(root.get(PROJECT_ID_FIELD_NAME), projectId));

        for (Map.Entry<PostFilterParameter, String> entry : filterParameters.entrySet()) {
            PostFilterParameter filterParameter = entry.getKey();
            String value = entry.getValue();

            switch (filterParameter) {
                case LIKES:
                case VIEWS:
                    predicates.add(cb.equal(root.get(filterParameter.getFieldName()), value));
                    break;
                case TAGS:
                    predicates.add(root.get(filterParameter.getFieldName()).in((Object[]) value.split(";")));
                    break;
                default:
                    predicates.add(cb.like(cb.lower(root.get(filterParameter.getFieldName())), "%" + value.toLowerCase() + "%"));
            }
        }

        query.where(predicates.toArray(new Predicate[0]));
        return query;
    }

    @Override
    public Sort buildSortProcessor(Map<PostSortParameter, String> sortingParameter) {
        return Sort.by(sortingParameter.entrySet().stream().map(element -> switch (element.getValue()) {
            case ASC -> Sort.Order.asc(element.getKey().getFieldName());
            case DESC -> Sort.Order.desc(element.getKey().getFieldName());
            default -> throw new DataNotValidException(MimosaExceptionMessage.SORTING_PARAMETER_NOT_VALID.getMessage());
        }).toList());
    }
}
