package ua.com.webbrains.mimosa.cms.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.NotModified304Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataExistException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.generated.dto.DeleteProfileRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateUserInformationRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UserInformationResponseDto;
import ua.com.webbrains.mimosa.cms.models.mappers.UserMapper;
import ua.com.webbrains.mimosa.cms.repositories.UserRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.ImageDeleteService;
import ua.com.webbrains.mimosa.cms.services.RegistrationService;
import ua.com.webbrains.mimosa.cms.services.UserService;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final AuthoritiesService authoritiesService;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    private final RegistrationService registrationService;
    private final ImageDeleteService imageDeleteService;

    @Override
    public UserInformationResponseDto findAuthenticatedUserInfo() {
        User currentUser = findCurrentUserEntityById();
        return userMapper.entityToInformationResponseDto(currentUser);
    }

    @Override
    @Transactional
    public UserInformationResponseDto updateUserInformation(UpdateUserInformationRequestDto dto) {
        User currentUser = findCurrentUserEntityById();
        UpdateUserInformationRequestDto currentInformationUserDto = userMapper.entityToUpdateInformationResponseDto(currentUser);

        if (currentInformationUserDto.equals(dto)) {
            throw new NotModified304Exception(MimosaExceptionMessage.PROFILE_IS_NOT_MODIFIED.getMessage());
        }

        String newEmail = dto.email();
        Optional<User> emailUserOptional = userRepository.findByEmail(newEmail);
        if (emailUserOptional.isPresent() && !emailUserOptional.get().getId().equals(currentUser.getId())) {
            throw new DataExistException(MimosaExceptionMessage.USER_BY_CREDENTIALS_EXISTS.getMessage());
        }

        Optional<User> usernameUserOptional = userRepository.findByUsername(dto.username());
        if (usernameUserOptional.isPresent() && !usernameUserOptional.get().getId().equals(currentUser.getId())) {
            throw new DataExistException(MimosaExceptionMessage.USER_BY_CREDENTIALS_EXISTS.getMessage());
        }

        boolean isEmailChanged = false;
        if (!currentUser.getEmail().equals(newEmail)) {
            currentUser.setEmail(newEmail);
            currentUser.setIsEmailApproved(false);
            isEmailChanged = true;
        }

        currentUser.setUsername(dto.username());
        currentUser.setName(dto.name());
        currentUser.setSurname(dto.surname());
        currentUser.setPhoneNumber(dto.phoneNumber());
        currentUser.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));

        currentUser = userRepository.save(currentUser);

        if (isEmailChanged) {
            registrationService.sendEmailVerificationLetterToUser(currentUser);
        }

        return userMapper.entityToInformationResponseDto(currentUser);
    }

    @Override
    public User findCurrentUser() {
        return findCurrentUserEntityById();
    }

    @Override
    public User findUserByUsernameOrEmail(String usernameOrEmail) {
        return findUserByUsernameOrEmail(usernameOrEmail, usernameOrEmail);
    }

    private User findUserByUsernameOrEmail(String email, String username) {
        return userRepository.findByUsernameOrEmail(username, email)
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));
    }

    private User findCurrentUserEntityById() {
        String currentUserId = authoritiesService.receiveCurrentUserId();
        return userRepository.findById(UUID.fromString(currentUserId))
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteCurrentUserProfile(DeleteProfileRequestDto deleteProfileRequestDto) {
        User currentUser = findCurrentUserEntityById();

        if (!passwordEncoder.matches(deleteProfileRequestDto.password(), currentUser.getPassword())) {
            throw new Forbidden403Exception(MimosaExceptionMessage.PASSWORD_DOES_NOT_MATCH.getMessage());
        }

        userRepository.delete(currentUser);
        imageDeleteService.deleteAvatar(currentUser.getId());
    }
}
