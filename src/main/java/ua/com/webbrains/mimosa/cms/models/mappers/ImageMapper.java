package ua.com.webbrains.mimosa.cms.models.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ua.com.webbrains.mimosa.cms.generated.dto.AvatarMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImageMetaResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Avatar;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Image;

@Mapper(componentModel = "spring")
public interface ImageMapper {
    AvatarMetaResponseDto entityToDto(Avatar avatar);

    @Mapping(target = "postId", ignore = true)
    ImageMetaResponseDto entityToDto(Image image);
}
