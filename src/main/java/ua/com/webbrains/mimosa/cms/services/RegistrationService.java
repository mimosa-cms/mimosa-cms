package ua.com.webbrains.mimosa.cms.services;

import org.springframework.security.access.prepost.PreAuthorize;
import ua.com.webbrains.mimosa.cms.generated.dto.EmailApproveRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;

/**
 * Service responsible for full flow of registration
 */
public interface RegistrationService {
    RegisterUserResponseDto registerUser(RegisterUserRequestDto dto);

    @PreAuthorize("hasRole('ROLE_UPDATE_PROFILE')")
    void sendEmailVerificationLetter();

    @PreAuthorize("hasRole('ROLE_UPDATE_PROFILE')")
    void sendEmailVerificationLetterToUser(User user);

    @PreAuthorize("hasRole('ROLE_UPDATE_PROFILE')")
    void approveUserEmail(EmailApproveRequestDto emailApproveRequestDto);
}
