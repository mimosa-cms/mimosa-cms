package ua.com.webbrains.mimosa.cms.models.parameters.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.models.parameters.FilterParameter;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum PostFilterParameter implements FilterParameter {
    TITLE("title", "title"),
    DESCRIPTION("description", "description"),
    AUTHORS("authors", "authors"),
    POST_STATUS("post_status", "postStatus"),
    CREATION_DATE("creation_date", "createdAt"),
    UPDATE_DATE("update_date", "updatedAt"),
    LIKES("likes", "likes"),
    VIEWS("views", "views"),
    CATEGORY("category", "category"),
    TAGS("tags", "tags");

    private final String value;
    private final String fieldName;

    public static PostFilterParameter fromString(String value) {
        return Arrays.stream(values())
                .filter((parameter -> value.equalsIgnoreCase(parameter.getValue())))
                .findFirst()
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.FILTER_VALUE_NOT_FOUND.getMessage())));
    }
}
