package ua.com.webbrains.mimosa.cms.services.security;

import ua.com.webbrains.mimosa.cms.models.security.JwtUserDetails;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Service for performing actions with JWT tokens
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface JwtService {
    String createAccessToken(String userId, List<String> permissions);

    String createRefreshToken(String userId);

    boolean validateAccessToken(String fullAccessToken);

    boolean validateRefreshToken(String refreshToken);

    JwtUserDetails parseAccessToken(String fullAccessToken);

    /** Returns userId from refresh token */
    String parseRefreshToken(String refreshToken);

    String createPostManagementApiKey(UUID projectId, OffsetDateTime offsetDateTime);
}
