package ua.com.webbrains.mimosa.cms.services;

import java.util.UUID;

public interface ImageDeleteService {

    void deleteImages(UUID post);

    void deleteAvatar(UUID userId);
}
