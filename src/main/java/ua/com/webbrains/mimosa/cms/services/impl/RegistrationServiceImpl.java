package ua.com.webbrains.mimosa.cms.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Conflict409Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.TooManyRequests429Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataExistException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.EmailApproveRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserResponseDto;
import ua.com.webbrains.mimosa.cms.models.mappers.UserMapper;
import ua.com.webbrains.mimosa.cms.repositories.EmailVerificationRepository;
import ua.com.webbrains.mimosa.cms.repositories.UserRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.EmailVerification;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.EmailService;
import ua.com.webbrains.mimosa.cms.services.RegistrationService;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.VERIFICATION_EMAIL_DELAY;
import static ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage.USER_BY_CREDENTIALS_EXISTS;

@Service
@RequiredArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {
    private static final String EMAIL_APPROVAL_SUBJECT = "Mimosa CMS Email Verification";
    private static final String EMAIL_VERIFICATION_TEMPLATE_PATH = "emails/emailVerificationTemplate";

    private final EmailService emailService;
    private final AuthoritiesService authoritiesService;
    private final UserRepository userRepository;
    private final EmailVerificationRepository emailVerificationRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final TemplateEngine templateEngine;

    @Value("${application.frontend.url}")
    private String frontEndUrl;

    @Value("${application.frontend.verification-endpoint}")
    private String verificationEndpoint;

    @Override
    @Transactional
    public RegisterUserResponseDto registerUser(RegisterUserRequestDto dto) {
        if (userRepository.findByUsernameOrEmail(dto.username(), dto.email()).isPresent()) {
            throw new DataExistException(USER_BY_CREDENTIALS_EXISTS.getMessage());
        }

        User registeringUser = userMapper.dtoToEntity(dto);

        registeringUser.setPassword(passwordEncoder.encode(registeringUser.getPassword()));
        registeringUser.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
        registeringUser.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));
        User savedUser = userRepository.save(registeringUser);

        sendVerificationLetter(savedUser);
        return userMapper.entityToRegisterResponseDto(savedUser);
    }

    @Override
    @Transactional
    public void sendEmailVerificationLetter() {
        String currentUserId = authoritiesService.receiveCurrentUserId();
        User currentUser = userRepository.findById(UUID.fromString(currentUserId))
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));
        sendVerificationLetter(currentUser);
    }

    @Transactional
    @Override
    public void sendEmailVerificationLetterToUser(User user) {
        sendVerificationLetter(user);
    }

    private void sendVerificationLetter(User user) {
        if (user.getIsEmailApproved()) {
            throw new Conflict409Exception(MimosaExceptionMessage.EMAIL_ALREADY_VERIFIED.getMessage());
        }

        String currentUserEmail = user.getEmail();
        Timestamp currentTimestamp = Timestamp.valueOf(LocalDateTime.now());
        String verificationToken;
        Optional<EmailVerification> emailVerification = emailVerificationRepository.findByEmail(currentUserEmail);
        EmailVerification emailVerificationRecord;

        if (emailVerification.isEmpty()) {
            verificationToken = UUID.randomUUID().toString();
            emailVerificationRecord = EmailVerification.builder().email(currentUserEmail).verificationToken(UUID.fromString(verificationToken)).build();
        } else {
            emailVerificationRecord = emailVerification.get();
            Timestamp emailSendAt = emailVerificationRecord.getSendAt();
            long differenceInMillis = Math.abs(currentTimestamp.getTime() - emailSendAt.getTime());

            if (differenceInMillis < VERIFICATION_EMAIL_DELAY) {
                throw new TooManyRequests429Exception(MimosaExceptionMessage.CANNOT_SEND_VERIFICATION_EMAIL_IN_LESS_THAN_3_MINUTES.getMessage());
            }

            verificationToken = emailVerificationRecord.getVerificationToken().toString();
        }

        Context context = new Context();
        context.setVariable("verificationLink", frontEndUrl + verificationEndpoint + "?token=" + verificationToken);
        String content = templateEngine.process(EMAIL_VERIFICATION_TEMPLATE_PATH, context);

        emailService.sendEmail(user.getEmail(), EMAIL_APPROVAL_SUBJECT, content);
        emailVerificationRecord.setSendAt(currentTimestamp);
        emailVerificationRepository.save(emailVerificationRecord);
    }

    @Override
    @Transactional
    public void approveUserEmail(EmailApproveRequestDto emailApproveRequestDto) {
        String currentUserId = authoritiesService.receiveCurrentUserId();
        User currentUser = userRepository.findById(UUID.fromString(currentUserId))
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));
        EmailVerification emailVerification = emailVerificationRepository.findByVerificationToken(emailApproveRequestDto.verificationToken())
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.CANNOT_VERIFY_EMAIL_BY_THIS_TOKEN.getMessage()));

        if (currentUser.getIsEmailApproved()) {
            throw new Conflict409Exception(MimosaExceptionMessage.EMAIL_ALREADY_VERIFIED.getMessage());
        } else if (!currentUser.getEmail().equals(emailVerification.getEmail())) {
            throw new DataNotValidException(MimosaExceptionMessage.TOKEN_IS_NOT_VALID.getMessage());
        }

        currentUser.setIsEmailApproved(true);
        emailVerificationRepository.delete(emailVerification);
    }
}
