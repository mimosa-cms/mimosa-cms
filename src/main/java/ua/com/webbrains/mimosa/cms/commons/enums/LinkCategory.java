package ua.com.webbrains.mimosa.cms.commons.enums;

import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.LinkCategoryDto;

import java.util.Arrays;
import java.util.List;

public enum LinkCategory {
    LINKEDIN,
    GITHUB,
    GITLAB,
    STACKOVERFLOW,
    TWITTER,
    FACEBOOK,
    INSTAGRAM,
    YOUTUBE,
    REDDIT,
    MEDIUM,
    DEVTO,
    PRODUCTHUNT,
    ANGELLIST,
    BEHANCE,
    DRIBBBLE,
    PINTEREST,
    SLIDESHARE,
    FLICKR,
    TUMBLR,
    VIMEO,
    SNAPCHAT,
    WHATSAPP,
    TELEGRAM,
    DISCORD,
    STEAM,
    SPOTIFY,
    SOUNDCLOUD,
    LINKTREE,
    STRAVA,
    TIKTOK,
    MICROSOFT,
    APPLE,
    GOOGLE,
    AMAZON,
    EBAY,
    PAYPAL,
    SLACK,
    DROPBOX,
    TRELLO,
    NOTION,
    JIRA,
    CONFLUENCE,
    ZOOM,
    MEETUP,
    DISCOVERY,
    PANDORA,
    SPOTIFYPREMIUM,
    UBER,
    STACKEXCHANGE,
    QUORA,
    WIKIPEDIA,
    BITBUCKET,
    SOUNDHOUND,
    BEATS,
    NONE;

    public static LinkCategory fromString(String value) {
        return Arrays.stream(values())
                .filter(category -> category.name().equalsIgnoreCase(value))
                .findFirst()
                .orElseThrow(() -> new DataNotValidException(String.format(MimosaExceptionMessage.LINK_CATEGORY_NOT_VALID.getMessage(), value)));
    }

    public static List<LinkCategoryDto> toDtoList() {
        return Arrays.stream(LinkCategory.values()).map(category -> LinkCategoryDto.builder().name(category.name()).build()).toList();
    }
}
