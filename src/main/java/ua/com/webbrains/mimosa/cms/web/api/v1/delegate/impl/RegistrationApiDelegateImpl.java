package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.RegistrationApiDelegate;
import ua.com.webbrains.mimosa.cms.generated.dto.EmailApproveRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserResponseDto;
import ua.com.webbrains.mimosa.cms.services.RegistrationService;

@Component
@RequiredArgsConstructor
public class RegistrationApiDelegateImpl implements RegistrationApiDelegate {
    private final RegistrationService registrationService;

    @Override
    public ResponseEntity<RegisterUserResponseDto> userRegister(RegisterUserRequestDto registerUserRequestDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(registrationService.registerUser(registerUserRequestDto));
    }

    @Override
    public ResponseEntity<Void> emailApprove(EmailApproveRequestDto emailApproveRequestDto) {
        registrationService.approveUserEmail(emailApproveRequestDto);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> emailApproveSend() {
        registrationService.sendEmailVerificationLetter();
        return ResponseEntity.ok().build();
    }
}
