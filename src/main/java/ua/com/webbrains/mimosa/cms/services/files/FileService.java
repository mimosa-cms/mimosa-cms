package ua.com.webbrains.mimosa.cms.services.files;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    String uploadPostImage(String uniqueIdentifier, MultipartFile file);

    String uploadAvatarImage(String uniqueIdentifier, MultipartFile file);

    Resource retrievePostImage(String filename);

    Resource retrieveAvatarImage(String filename);

    void deletePostImage(String filename);

    void deleteAvatarImage(String filename);
}
