package ua.com.webbrains.mimosa.cms.models.parameters.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.models.parameters.SortParameter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum PostSortParameter implements SortParameter {
    TITLE("title", "title"),
    DESCRIPTION("description", "description"),
    AUTHORS("authors", "authors"),
    POST_STATUS("post_status", "postStatus"),
    CREATION_DATE("creation_date", "createdAt"),
    UPDATE_DATE("update_date", "updatedAt"),
    LIKES("likes", "likes"),
    VIEWS("views", "views"),
    CATEGORY("category", "category");

    private final String value;
    private final String fieldName;

    public static PostSortParameter fromString(String value) {
        return Arrays.stream(values())
                .filter((parameter -> value.equalsIgnoreCase(parameter.getValue())))
                .findFirst()
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.SORTING_PARAMETER_NOT_VALID.getMessage())));
    }
}
