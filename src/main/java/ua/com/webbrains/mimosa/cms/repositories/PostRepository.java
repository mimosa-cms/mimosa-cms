package ua.com.webbrains.mimosa.cms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.webbrains.mimosa.cms.models.PostType;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;

import java.util.Optional;
import java.util.UUID;

public interface PostRepository extends JpaRepository<Post, UUID> {
    Page<Post> findPostsByProjectId(UUID projectId, Pageable pageable);

    @EntityGraph(attributePaths = {"postDetails"})
    Optional<Post> findPostByIdAndProjectIdAndPostType(UUID id, UUID projectId, PostType postType);

    @EntityGraph(attributePaths = {"postDetails"})
    Optional<Post> findPostByIdAndProjectId(UUID id, UUID projectId);

    Integer countAllByProjectId(UUID projectId);
}
