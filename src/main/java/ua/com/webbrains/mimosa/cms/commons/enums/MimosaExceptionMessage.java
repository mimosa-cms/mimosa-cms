package ua.com.webbrains.mimosa.cms.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MimosaExceptionMessage {
    DTO_IS_NOT_VALID("Data that is being send to the server is not valid"),
    USER_BY_CREDENTIALS_NOT_FOUND("User with such credentials not found"),
    USER_NOT_FOUND("User not found"),
    USER_BY_CREDENTIALS_EXISTS("User with such credentials already exists"),
    USER_NOT_AUTHORIZED("User is not authorized"),
    TOKEN_IS_NOT_VALID("Token is not valid"),
    FAILED_TO_SEND_EMAIL("Failed to send email"),
    EMAIL_ALREADY_VERIFIED("Email already verified"),
    CANNOT_VERIFY_EMAIL_BY_THIS_TOKEN("Can not verify email by this token"),
    CANNOT_SEND_VERIFICATION_EMAIL_IN_LESS_THAN_3_MINUTES("Cannot send verification email in less than 3 minutes after previous attempt"),
    CANNOT_SEND_PASSWORD_RECOVERY_IN_LESS_THAN_3_MINUTES("Cannot send password recovery email in less than 3 minutes after previous attempt"),
    PASSWORD_RECOVERY_TOKEN_IS_EXPIRED("Password recovery token is expired, generate a new one"),
    REPEAT_PASSWORD_AND_PASSWORD_DOES_NOT_MATCH_THE_SAME_VALUE("Repeat password and password does not match the same value"),
    PASSWORD_DOES_NOT_MATCH("Password is incorrect. Try again later."),
    PASSWORD_RECOVERY_TOKEN_IS_NOT_VALID("Password recovery token is not valid"),
    PASSWORD_RECOVERY_REQUEST_NOT_FOUND("Password recovery request not found, try to create a new one"),
    PASSWORD_CANNOT_BE_THE_SAME_AS_PREVIOUS_ONE("Password cannot be the same as previous one"),
    ROLE_NOT_FOUND("Role %s not found"),
    PROFILE_IS_NOT_MODIFIED("Profile is not modified"),
    PROJECT_WITH_THIS_LINK_EXIST("Project with this link %s already exists in your account"),
    REACHED_AMOUNT_OF_FREE_PROJECTS("User reached amount of free projects"),
    PROJECT_NOT_FOUND("Project %s not found for current account"),
    POST_TEXT_TOO_LARGE("Post text is too large, and must be not greater than %s symbols"),
    REACHED_AMOUNT_OF_FREE_POSTS("Reached amount of free posts for this project"),
    LINK_NOT_VALID("Link %s is not valid"),
    LINK_CATEGORY_NOT_VALID("Link category %s is not valid"),
    POST_STATUS_NOT_VALID("Post status is not valid"),
    AUTHOR_IS_NOT_VALID("Author %s is not valid"),
    POST_NOT_FOUND("Post is not found"),
    POST_DETAILS_NOT_FOUND("Post details not found"),
    FILTER_VALUE_NOT_FOUND("Filter value is not found"),
    POTENTIAL_SQL_ATTACK_DETECTED("Potential attack detected in this value %s. Try again with normal value :)"),
    SORTING_PARAMETER_NOT_VALID("Sorting parameter not valid, must be asc or desc."),
    UNABLE_TO_LOAD_CATEGORIES_FROM_FILE("Unable to load categories from file: %s"),
    WRONG_POST_TYPE_FOR_THE_ACTION("Unable to update post details, because post type is %s."),
    TOO_MANY_POST_BLOCKS_AMOUNT("Too many blocks for this post. It mast not be greater than $s."),
    POST_STRUCTURE_NOT_VALID("Post structure is not valid. Try again with correct field values."),
    FAILED_TO_UPLOAD_FILE("Failed to upload file"),
    IMAGES_COLLECTION_NOT_VALID("Images collection is empty or contains more than 10 elements"),
    UNSUPPORTED_CONTENT_TYPE("Unsupported content type: %s"),
    IMAGE_NOT_FOUND("Image not found"),
    FAILED_TO_RETRIEVE_IMAGE("Failed to retrieve post image"),
    FAILED_TO_DELETE_IMAGE("Failed to delete post image"),
    AVATAR_NOT_FOUND("Avatar not found"),
    IMAGE_NOT_VALID("Image is not valid"),
    INVITE_ALREADY_EXISTS("Invite for this user already exists already exists"),
    INVITE_NOT_FOUND("Invite not found"),
    INVALID_INVITE_LINK("Invalid invite link: %s"),
    POST_CONTAINS_IMAGES("Post contains images, delete them first"),
    POLICY_LANGUAGE_MESSAGE("Policy language should be 'en' or 'ua'."),
    VERIFICATION_CODE_IS_REQUIRED("Verification code is required"),
    INVALID_VERIFICATION_CODE("Invalid verification code"),
    CANNOT_GENERATE_QR_CODE("Cannot generate QR code, because 2fa already enabled");

    private final String message;
}
