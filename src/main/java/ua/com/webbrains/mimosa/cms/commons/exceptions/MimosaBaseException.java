package ua.com.webbrains.mimosa.cms.commons.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * Unchecked exception for response to user purposes
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
@Getter
public abstract class MimosaBaseException extends RuntimeException {
    private final HttpStatus httpStatus;

    public MimosaBaseException(String message, Exception cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public MimosaBaseException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
