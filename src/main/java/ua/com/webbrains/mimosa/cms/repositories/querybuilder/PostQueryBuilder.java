package ua.com.webbrains.mimosa.cms.repositories.querybuilder;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import org.springframework.data.domain.Sort;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.PostFilterParameter;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.PostSortParameter;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;

import java.util.Map;
import java.util.UUID;

public interface PostQueryBuilder {
    CriteriaQuery<Post> buildSearchProcessor(CriteriaBuilder cb, UUID projectId, Map<PostFilterParameter, String> filterParameters);

    Sort buildSortProcessor(Map<PostSortParameter, String> sortingParameter);
}
