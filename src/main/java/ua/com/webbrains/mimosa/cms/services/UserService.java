package ua.com.webbrains.mimosa.cms.services;

import org.springframework.security.access.prepost.PreAuthorize;
import ua.com.webbrains.mimosa.cms.generated.dto.DeleteProfileRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateUserInformationRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UserInformationResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;

/**
 * Service for handling actions with profiles
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface UserService {
    @PreAuthorize("hasRole('ROLE_READ_PROFILE')")
    UserInformationResponseDto findAuthenticatedUserInfo();

    @PreAuthorize("hasRole('ROLE_UPDATE_PROFILE')")
    UserInformationResponseDto updateUserInformation(UpdateUserInformationRequestDto dto);

    @PreAuthorize("hasRole('ROLE_READ_PROFILE')")
    User findCurrentUser();

    User findUserByUsernameOrEmail(String usernameOrEmail);

    @PreAuthorize("hasRole('ROLE_DELETE_PROFILE')")
    void deleteCurrentUserProfile(DeleteProfileRequestDto deleteProfileRequestDto);
}
