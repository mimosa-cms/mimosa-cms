package ua.com.webbrains.mimosa.cms.services.security;

import org.springframework.security.access.prepost.PreAuthorize;

import java.time.OffsetDateTime;
import java.util.UUID;

public interface ProjectManagementApiKeyService {
    @PreAuthorize("hasAuthority('ROLE_READ_OWN_PROJECTS')")
    String generateProjectManagementApiKey(UUID projectId, OffsetDateTime expirationDate);
}
