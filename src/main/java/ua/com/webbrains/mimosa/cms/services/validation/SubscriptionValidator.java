package ua.com.webbrains.mimosa.cms.services.validation;

import ua.com.webbrains.mimosa.cms.generated.dto.BlockBasedPostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostRequestDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;

public interface SubscriptionValidator {
    void validatePostForSubscriptionLimits(Project project, PostRequestDto postCreationDto);

    void validatePostForSubscriptionLimits(Project project, BlockBasedPostRequestDto postCreationDto);

    boolean isProjectIssuedByPremiumUser(Project project);
}
