package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.ProjectsApiDelegate;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectsResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.services.ProjectService;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ProjectsApiDelegateImpl implements ProjectsApiDelegate {
    private final ProjectService projectService;

    @Override
    public ResponseEntity<CreateProjectResponseDto> createUserProject(CreateProjectRequestDto createProjectRequestDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(projectService.createProject(createProjectRequestDto));
    }

    @Override
    public ResponseEntity<ProjectsResponseDto> getUserProjects(Integer size, Integer page, String filter, String sort) {
        return ResponseEntity.ok(projectService.findAllUserProjects(size, page, filter, sort));
    }

    @Override
    public ResponseEntity<Void> deleteUserProject(UUID id) {
        projectService.deleteProject(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<ProjectResponseDto> getUserProject(UUID id) {
        return ResponseEntity.ok(projectService.findProject(id));
    }

    @Override
    public ResponseEntity<ProjectResponseDto> updateUserProject(UUID id, UpdateProjectRequestDto updateProjectRequestDto) {
        return ResponseEntity.ok(projectService.updateProject(id, updateProjectRequestDto));
    }
}
