package ua.com.webbrains.mimosa.cms.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.com.webbrains.mimosa.cms.repositories.AvatarRepository;
import ua.com.webbrains.mimosa.cms.repositories.ImageRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Avatar;
import ua.com.webbrains.mimosa.cms.services.ImageDeleteService;
import ua.com.webbrains.mimosa.cms.services.files.FileService;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ImageDeleteServiceImpl implements ImageDeleteService {
    private final ImageRepository imageRepository;
    private final FileService fileService;
    private final AvatarRepository avatarRepository;

    @Override
    public void deleteImages(UUID post) {
        imageRepository.findAllByPostId(post).forEach(image -> {
            fileService.deletePostImage(image.getFilename());
            imageRepository.delete(image);
        });
    }

    @Override
    public void deleteAvatar(UUID userId) {
        Optional<Avatar> avatarOptional = avatarRepository.findByUserId(userId);
        avatarOptional.ifPresent(avatar -> {
            fileService.deleteAvatarImage(avatar.getFilename());
            avatarRepository.delete(avatar);
        });
    }
}
