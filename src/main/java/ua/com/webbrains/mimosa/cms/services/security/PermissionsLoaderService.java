package ua.com.webbrains.mimosa.cms.services.security;

import ua.com.webbrains.mimosa.cms.models.security.RolesAndPermissions;

/**
 * Interface that defines method for working with permissions
 */
public interface PermissionsLoaderService {
    RolesAndPermissions loadRolesAndPermissionsFromResources();
}
