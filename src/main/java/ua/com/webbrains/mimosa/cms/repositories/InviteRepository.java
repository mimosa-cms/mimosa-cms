package ua.com.webbrains.mimosa.cms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Invite;

import java.util.Optional;
import java.util.UUID;

public interface InviteRepository extends JpaRepository<Invite, UUID> {
    Optional<Invite> findInviteByProjectIdAndUserId(UUID projectId, UUID userId);
}
