package ua.com.webbrains.mimosa.cms.services.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.generated.dto.CoworkersResponseCoworkersInnerDto;
import ua.com.webbrains.mimosa.cms.generated.dto.CoworkersResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.InviteRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Invite;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.CoworkersService;
import ua.com.webbrains.mimosa.cms.services.EmailService;
import ua.com.webbrains.mimosa.cms.services.ProjectService;
import ua.com.webbrains.mimosa.cms.services.UserService;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CoworkersServiceImpl implements CoworkersService {
    private static final String INVITE_SUBJECT = "You have been invited to the project";
    private static final String INVITE_TEMPLATE_PATH = "emails/inviteVerificationTemplate";

    private final InviteRepository inviteRepository;
    private final EmailService emailService;
    private final TemplateEngine templateEngine;
    private final UserService userService;
    private final ProjectService projectService;

    @Value("${application.frontend.url}")
    private String frontEndUrl;
    @Value("${application.frontend.invite-endpoint}")
    private String inviteEndpoint;

    @Override
    @Transactional
    public void acceptInvite(UUID token) {
        User currentUser = userService.findCurrentUser();
        Invite invite = inviteRepository.findById(token)
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.INVITE_NOT_FOUND.getMessage()));

        if (!invite.getUser().getId().equals(currentUser.getId())) {
            throw new Forbidden403Exception(MimosaExceptionMessage.INVALID_INVITE_LINK.getMessage().formatted("This link corresponds to another user"));
        }

        Project project = invite.getProject();

        project.getCoworkers().add(currentUser);
        inviteRepository.delete(invite);
    }

    @Override
    @Transactional
    public void addCoworkerToProject(UUID projectId, String coworker) {
        User currentUser = userService.findCurrentUser();
        User guest = userService.findUserByUsernameOrEmail(coworker);
        Project existingProject = projectService.findProjectByIdAndOwnerId(projectId, currentUser.getId());

        validateInviteRequest(existingProject, currentUser, guest);

        Invite invite = Invite.builder()
                .token(UUID.randomUUID())
                .project(existingProject)
                .user(guest)
                .build();

        inviteRepository.save(invite);

        sendEmail(currentUser.getUsername(), guest.getEmail(), existingProject.getName(), invite.getToken());
    }

    private void validateInviteRequest(Project project, User owner, User guest) {
        if (inviteRepository.findInviteByProjectIdAndUserId(project.getId(), guest.getId()).isPresent()) {
            throw new Forbidden403Exception(MimosaExceptionMessage.INVITE_ALREADY_EXISTS.getMessage());
        }

        if (owner.getId().equals(guest.getId())) {
            throw new Forbidden403Exception("You can't invite yourself");
        }
    }

    private void sendEmail(String ownerUsername, String userEmail, String projectName, UUID token) {
        Context context = new Context();
        context.setVariable("inviteLink", frontEndUrl + inviteEndpoint + "?token=" + token);
        context.setVariable("projectName", projectName);
        context.setVariable("ownerUsername", ownerUsername);
        String content = templateEngine.process(INVITE_TEMPLATE_PATH, context);
        emailService.sendEmail(userEmail, INVITE_SUBJECT, content);
    }

    @Override
    @Transactional
    public void deleteCoworkerFromProject(UUID project, String coworker) {
        User currentUser = userService.findCurrentUser();
        Project existingProject = projectService.findProjectByIdAndOwnerId(project, currentUser.getId());
        User coworkerEntity = userService.findUserByUsernameOrEmail(coworker);

        existingProject.getCoworkers().remove(coworkerEntity);
    }

    @Override
    @Transactional
    public CoworkersResponseDto getProjectCoworkers(UUID project) {
        User currentUser = userService.findCurrentUser();
        Project existingProject = projectService.findProjectByIdAndOwnerId(project, currentUser.getId());

        return CoworkersResponseDto.builder()
                .coworkers(existingProject.getCoworkers().stream().map(
                        user -> CoworkersResponseCoworkersInnerDto.builder()
                                .username(user.getUsername())
                                .build()
                ).toList())
                .build();
    }
}
