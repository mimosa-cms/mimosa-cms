package ua.com.webbrains.mimosa.cms.services;

import org.springframework.security.access.prepost.PreAuthorize;
import ua.com.webbrains.mimosa.cms.generated.dto.PostCategoriesResponseDto;

public interface CategoryService {
    void loadCategories();

    @PreAuthorize("hasRole('READ_POSTS')")
    PostCategoriesResponseDto retrieveCategories(int page, int size, String search);
}
