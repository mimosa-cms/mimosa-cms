package ua.com.webbrains.mimosa.cms.configs;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import ua.com.webbrains.mimosa.cms.configs.properties.PostgresqlProperties;

@Configuration
@RequiredArgsConstructor
public class DebeziumConfig {
    private final PostgresqlProperties postgresqlProperties;

}
