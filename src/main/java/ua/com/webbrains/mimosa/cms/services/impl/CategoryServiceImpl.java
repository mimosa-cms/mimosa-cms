package ua.com.webbrains.mimosa.cms.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.OperationFailedException;
import ua.com.webbrains.mimosa.cms.generated.dto.PostCategoriesResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.CategoryRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Category;
import ua.com.webbrains.mimosa.cms.services.CategoryService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.CATEGORIES_FILE;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final ObjectMapper objectMapper;
    private final CategoryRepository categoryRepository;

    @Override
    public void loadCategories() {
        ClassPathResource classPathResource = new ClassPathResource(CATEGORIES_FILE);
        try (InputStream categoriesInputStream = classPathResource.getInputStream()) {
            List<String> categories = objectMapper.readValue(categoriesInputStream, List.class);
            List<Category> categoryList = categories.stream().distinct().map(categoryName -> new Category(UUID.randomUUID(), categoryName)).toList();
            categoryRepository.deleteAll();
            categoryRepository.saveAll(categoryList);
        } catch (IOException | ClassCastException e) {
            throw new OperationFailedException(MimosaExceptionMessage.UNABLE_TO_LOAD_CATEGORIES_FROM_FILE.getMessage().formatted(e.getMessage()), e);
        }
    }

    @Override
    public PostCategoriesResponseDto retrieveCategories(int page, int size, String search) {
        Page<Category> categoryPage;
        PageRequest pageRequest = PageRequest.of(page, size);
        if (!StringUtils.isEmpty(search)) {
            categoryPage = categoryRepository.findAllByNameLike(search, pageRequest);
        } else {
            categoryPage = categoryRepository.findAll(pageRequest);
        }

        return PostCategoriesResponseDto.builder().categories(categoryPage.map(Category::getName).toList()).total(categoryPage.getTotalPages()).build();
    }
}
