package ua.com.webbrains.mimosa.cms.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataExistException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.commons.utils.MultipleUriParameterParser;
import ua.com.webbrains.mimosa.cms.commons.utils.TimeUtil;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectsResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.models.mappers.ProjectMapper;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.ProjectFilterParameter;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.ProjectSortParameter;
import ua.com.webbrains.mimosa.cms.repositories.PostRepository;
import ua.com.webbrains.mimosa.cms.repositories.ProjectRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.repositories.querybuilder.ProjectQueryBuilder;
import ua.com.webbrains.mimosa.cms.services.ProjectService;
import ua.com.webbrains.mimosa.cms.services.UserService;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_FREE_POSTS_PER_PROJECT_AMOUNT;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_FREE_PROJECTS_AMOUNT;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.SORTING_PATTERN;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;
    private final PostRepository postRepository;
    private final UserService userService;
    private final AuthoritiesService authoritiesService;
    private final ProjectMapper projectMapper;
    private final ProjectQueryBuilder projectQueryBuilder;

    @Override
    @Transactional
    public CreateProjectResponseDto createProject(CreateProjectRequestDto dto) {
        User currentUser = userService.findCurrentUser();

        validateProjectCreation(dto, currentUser);

        Project newProject = projectMapper.dtoToEntity(dto)
                .setOwner(currentUser).setCoworkers(new ArrayList<>())
                .setCreatedAt(TimeUtil.now());

        newProject = projectRepository.save(newProject);

        return CreateProjectResponseDto.builder().id(newProject.getId()).build();
    }

    private void validateProjectCreation(CreateProjectRequestDto dto, User user) {
        isAllowedToCreateOrElseThrow(user);

        if (projectRepository.findByLinkAndOwnerId(dto.link(), user.getId()).isPresent()) {
            throw new DataExistException(String.format(MimosaExceptionMessage.PROJECT_WITH_THIS_LINK_EXIST.getMessage(), dto.link()));
        }
    }

    private void isAllowedToCreateOrElseThrow(User user) {
        if (!user.getIsPremium() && projectRepository.countAllByOwnerId(user.getId()) > MAX_FREE_PROJECTS_AMOUNT) {
            throw new Forbidden403Exception(MimosaExceptionMessage.REACHED_AMOUNT_OF_FREE_PROJECTS.getMessage());
        }
    }

    @Override
    public ProjectsResponseDto findAllUserProjects(int size, int page, String filter, String sort) {
        User currentUser = userService.findCurrentUser();
        Page<Project> projectsPage;
        Integer maxProjectsAmount = currentUser.getIsPremium() ? null : MAX_FREE_PROJECTS_AMOUNT;
        Integer projectsAmount = projectRepository.countAllAllowed(currentUser.getId());

        if (!StringUtils.isEmpty(filter) || !StringUtils.isEmpty(sort)) {
            Map<ProjectFilterParameter, String> filterParameters = validateFilterParameters(MultipleUriParameterParser.parseComplexParameter(filter));
            Map<ProjectSortParameter, String> sortParameters = validateSortParameters(MultipleUriParameterParser.parseComplexParameter(sort));

            Specification<Project> specification = projectQueryBuilder.buildSearchProcessor(currentUser.getId(), filterParameters);
            Sort sortParams = projectQueryBuilder.buildSortProcessor(sortParameters);

            projectsPage = projectRepository.findAll(specification, PageRequest.of(page, size, sortParams));
        } else {
            projectsPage = projectRepository.findAllAllowed(currentUser.getId(), PageRequest.of(page, size));
        }

        return ProjectsResponseDto.builder()
                .page(projectsPage.getNumber())
                .size(projectsPage.getSize())
                .projectsAmount(projectsAmount)
                .maxNumberOfProjects(maxProjectsAmount)
                .numberOfPages(projectsPage.getTotalPages())
                .projects(constructProjectsResponseListFromEntities(projectsPage.getContent()))
                .build();
    }

    private List<ProjectResponseDto> constructProjectsResponseListFromEntities(List<Project> projects) {
        return projects.stream().map((projectEntity) -> {
            Integer postsCount = postRepository.countAllByProjectId(projectEntity.getId());
            Integer maxPostsAmount = projectEntity.getOwner().getIsPremium() ? null : MAX_FREE_POSTS_PER_PROJECT_AMOUNT;
            return projectMapper.entityToDto(projectEntity, postsCount, maxPostsAmount);
        }).toList();
    }

    @Override
    @Transactional
    @CacheEvict(value = "project", key = "'projectId-' + #id")
    public void deleteProject(UUID id) {
        String currentUserId = authoritiesService.receiveCurrentUserId();
        Optional<Project> projectOptional = projectRepository.findByAllowedById(UUID.fromString(currentUserId), id);

        if (projectOptional.isEmpty()) {
            throw new DataNotFoundException(String.format(MimosaExceptionMessage.PROJECT_NOT_FOUND.getMessage(), id));
        }

        projectRepository.delete(projectOptional.get());
    }

    @Override
    @Transactional
    @CachePut(value = "project", key = "'projectId-' + #id")
    public ProjectResponseDto updateProject(UUID id, UpdateProjectRequestDto dto) {
        User currentUser = userService.findCurrentUser();
        Project project = findProjectByIdAndUserIdOrElseThrow(currentUser.getId(), id);

        validateProjectUpdate(dto, currentUser.getId(), project);

        project.setLink(dto.link());
        project.setName(dto.name());

        Integer postsCount = postRepository.countAllByProjectId(id);
        Integer maxPostsAmount = project.getOwner().getIsPremium() ? null : MAX_FREE_POSTS_PER_PROJECT_AMOUNT;
        return projectMapper.entityToDto(project, postsCount, maxPostsAmount);
    }

    private void validateProjectUpdate(UpdateProjectRequestDto dto, UUID currentUserId, Project project) {
        Optional<Project> updatingProjectOptional = projectRepository.findByLinkAndOwnerId(dto.link(), currentUserId);
        if (updatingProjectOptional.isPresent() && !project.getId().equals(updatingProjectOptional.get().getId())) {
            throw new DataExistException(String.format(MimosaExceptionMessage.PROJECT_WITH_THIS_LINK_EXIST.getMessage(), dto.link()));
        }
    }

    @Override
    @Cacheable(value = "project", key = "'projectId-' + #id")
    public ProjectResponseDto findProject(UUID id) {
        User currentUser = userService.findCurrentUser();
        Integer postsCount = postRepository.countAllByProjectId(id);
        var projectEntity = findProjectByIdAndUserIdOrElseThrow(currentUser.getId(), id);
        Integer maxPostsAmount = projectEntity.getOwner().getIsPremium() ? null : MAX_FREE_POSTS_PER_PROJECT_AMOUNT;
        return projectMapper.entityToDto(projectEntity, postsCount, maxPostsAmount);
    }

    @Override
    public Project findProjectByIdAndUserId(UUID projectId, UUID userId) {
        return findProjectByIdAndUserIdOrElseThrow(userId, projectId);
    }

    @Override
    public Project findProjectByIdAndOwnerId(UUID projectId, UUID ownerId) {
        return findProjectByIdAndOwnerIdOrElseThrow(ownerId, projectId);
    }

    private Map<ProjectFilterParameter, String> validateFilterParameters(Map<String, String> filterParameters) {
        return filterParameters.entrySet().stream().map(this::validateFilterParameter).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map.Entry<ProjectFilterParameter, String> validateFilterParameter(Map.Entry<String, String> filterParameter) {
        ProjectFilterParameter keyFilter = ProjectFilterParameter.fromString(filterParameter.getKey());
        String valueFilter = filterParameter.getValue();

        return new AbstractMap.SimpleEntry<>(keyFilter, valueFilter);
    }

    private Map<ProjectSortParameter, String> validateSortParameters(Map<String, String> sortParameters) {
        return sortParameters.entrySet().stream().map(this::validateSortParameter).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map.Entry<ProjectSortParameter, String> validateSortParameter(Map.Entry<String, String> sortParameter) {
        ProjectSortParameter keyFilter = ProjectSortParameter.fromString(sortParameter.getKey());
        String valueFilter = sortParameter.getValue();

        if (!valueFilter.matches(SORTING_PATTERN)) {
            throw new DataNotValidException(MimosaExceptionMessage.SORTING_PARAMETER_NOT_VALID.getMessage());
        }

        return new AbstractMap.SimpleEntry<>(keyFilter, valueFilter);
    }

    private Project findProjectByIdAndUserIdOrElseThrow(UUID userId, UUID id) {
        return projectRepository.findByAllowedById(userId, id)
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.PROJECT_NOT_FOUND.getMessage(), id)));
    }

    private Project findProjectByIdAndOwnerIdOrElseThrow(UUID ownerId, UUID id) {
        return projectRepository.findByIdAndOwnerId(id, ownerId)
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.PROJECT_NOT_FOUND.getMessage(), id)));
    }
}
