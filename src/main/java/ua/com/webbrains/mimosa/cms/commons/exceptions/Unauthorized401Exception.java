package ua.com.webbrains.mimosa.cms.commons.exceptions;

import org.springframework.http.HttpStatus;

/** Exception for case of 401 error */
public class Unauthorized401Exception extends MimosaBaseException {
    public Unauthorized401Exception(String message, Exception cause) {
        super(message, cause, HttpStatus.UNAUTHORIZED);
    }

    public Unauthorized401Exception(String message) {
        super(message, HttpStatus.UNAUTHORIZED);
    }
}
