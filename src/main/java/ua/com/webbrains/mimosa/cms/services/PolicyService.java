package ua.com.webbrains.mimosa.cms.services;

public interface PolicyService {
    String getPrivacyPolicy(String language);
}
