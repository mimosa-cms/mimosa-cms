package ua.com.webbrains.mimosa.cms.services;

import org.springframework.security.access.prepost.PreAuthorize;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectsResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;

import java.util.UUID;

/**
 * Service responsible for handling actions with projects
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface ProjectService {
    Project findProjectByIdAndUserId(UUID projectId, UUID userId);

    Project findProjectByIdAndOwnerId(UUID projectId, UUID ownerId);

    @PreAuthorize("hasRole('ROLE_CREATE_PROJECTS') and hasRole('ROLE_EMAIL_APPROVED')")
    CreateProjectResponseDto createProject(CreateProjectRequestDto dto);

    @PreAuthorize("hasRole('ROLE_READ_OWN_PROJECTS') and hasRole('ROLE_EMAIL_APPROVED')")
    ProjectsResponseDto findAllUserProjects(int size, int page, String filter, String sort);

    @PreAuthorize("hasRole('ROLE_DELETE_PROJECTS') and hasRole('ROLE_EMAIL_APPROVED')")
    void deleteProject(UUID id);

    @PreAuthorize("hasRole('ROLE_UPDATE_PROJECTS') and hasRole('ROLE_EMAIL_APPROVED')")
    ProjectResponseDto updateProject(UUID id, UpdateProjectRequestDto dto);

    @PreAuthorize("hasRole('ROLE_READ_OWN_PROJECTS') and hasRole('ROLE_EMAIL_APPROVED')")
    ProjectResponseDto findProject(UUID id);
}
