package ua.com.webbrains.mimosa.cms.configs.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "spring.datasource")
public class PostgresqlProperties {
    private String url;
    private String username;
    private String password;
}
