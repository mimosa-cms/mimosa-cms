package ua.com.webbrains.mimosa.cms.services;

import org.springframework.security.access.prepost.PreAuthorize;
import ua.com.webbrains.mimosa.cms.generated.dto.CoworkersResponseDto;

import java.util.UUID;

public interface CoworkersService {
    void acceptInvite(UUID token);

    @PreAuthorize("hasRole('ROLE_UPDATE_PROJECTS') and hasRole('ROLE_EMAIL_APPROVED')")
    void addCoworkerToProject(UUID project, String coworker);

    @PreAuthorize("hasRole('ROLE_UPDATE_PROJECTS') and hasRole('ROLE_EMAIL_APPROVED')")
    void deleteCoworkerFromProject(UUID project, String coworker);

    @PreAuthorize("hasRole('ROLE_READ_OWN_PROJECTS') and hasRole('ROLE_EMAIL_APPROVED')")
    CoworkersResponseDto getProjectCoworkers(UUID project);
}
