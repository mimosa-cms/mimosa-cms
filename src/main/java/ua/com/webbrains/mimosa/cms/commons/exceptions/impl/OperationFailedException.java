package ua.com.webbrains.mimosa.cms.commons.exceptions.impl;

import ua.com.webbrains.mimosa.cms.commons.exceptions.InternalServer500Exception;

public class OperationFailedException extends InternalServer500Exception {
    public OperationFailedException(String message, Exception cause) {
        super(message, cause);
    }

    public OperationFailedException(String message) {
        super(message);
    }
}
