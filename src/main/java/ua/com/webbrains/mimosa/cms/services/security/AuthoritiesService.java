package ua.com.webbrains.mimosa.cms.services.security;

import org.springframework.security.core.GrantedAuthority;
import ua.com.webbrains.mimosa.cms.commons.enums.Role;

import java.util.List;

/**
 * Service for fetching authorities depending on user data
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface AuthoritiesService {
    List<GrantedAuthority> fetchAuthoritiesFromPermissions(List<String> permissions);

    List<String> fetchPermissionsFromRole(Role role);

    String receiveCurrentUserId();
}
