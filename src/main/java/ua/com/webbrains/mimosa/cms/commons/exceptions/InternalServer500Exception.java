package ua.com.webbrains.mimosa.cms.commons.exceptions;

import org.springframework.http.HttpStatus;

public class InternalServer500Exception extends MimosaBaseException {
    public InternalServer500Exception(String message, Exception cause) {
        super(message, cause, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public InternalServer500Exception(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
