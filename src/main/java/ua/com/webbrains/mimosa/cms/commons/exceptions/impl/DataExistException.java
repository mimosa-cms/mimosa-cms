package ua.com.webbrains.mimosa.cms.commons.exceptions.impl;

import ua.com.webbrains.mimosa.cms.commons.exceptions.Conflict409Exception;

public class DataExistException extends Conflict409Exception {
    public DataExistException(String message, Exception cause) {
        super(message, cause);
    }

    public DataExistException(String message) {
        super(message);
    }
}
