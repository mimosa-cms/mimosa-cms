package ua.com.webbrains.mimosa.cms.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.models.PoliciesModel;
import ua.com.webbrains.mimosa.cms.services.PolicyService;

import java.util.Optional;

@Configuration
@RequiredArgsConstructor
public class PolicyServiceImpl implements PolicyService {
    private final PoliciesModel policiesModel;

    @Override
    public String getPrivacyPolicy(String language) {
        var value = policiesModel.policies().get(language);
        return Optional.ofNullable(value)
                .orElseThrow(() -> new DataNotValidException(MimosaExceptionMessage.POLICY_LANGUAGE_MESSAGE.getMessage()));
    }
}
