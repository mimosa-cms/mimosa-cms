package ua.com.webbrains.mimosa.cms.services.validation.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.BlockBasedPostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostRequestDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.services.validation.SubscriptionValidator;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_BLOCKS_POST_AMOUNT;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_FREE_BLOCKS_POST_AMOUNT;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_FREE_TRIAL_POST_LENGTH;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_POST_LENGTH;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.TEXT_BLOCKS_FIELDS;

@Service
@RequiredArgsConstructor
public class SubscriptionValidatorImpl implements SubscriptionValidator {

    @Override
    public void validatePostForSubscriptionLimits(Project project, PostRequestDto postCreationDto) {
        int maxPostLength;
        if (isOwnerPremium(project)) {
            maxPostLength = MAX_POST_LENGTH;
        } else {
            maxPostLength = MAX_FREE_TRIAL_POST_LENGTH;
        }

        if (postCreationDto.text().length() > maxPostLength) {
            throw new DataNotValidException(String.format(MimosaExceptionMessage.POST_TEXT_TOO_LARGE.getMessage(), maxPostLength));
        }
    }

    @Override
    public void validatePostForSubscriptionLimits(Project project, BlockBasedPostRequestDto postCreationDto) {
        int maxAmountOfBlocks;
        int maxPostLength;

        if (isOwnerPremium(project)) {
            maxAmountOfBlocks = MAX_BLOCKS_POST_AMOUNT;
            maxPostLength = MAX_POST_LENGTH;
        } else {
            maxAmountOfBlocks = MAX_FREE_BLOCKS_POST_AMOUNT;
            maxPostLength = MAX_FREE_TRIAL_POST_LENGTH;
        }

        if (postCreationDto.content().blocks().size() > maxAmountOfBlocks) {
            throw new DataNotValidException(String.format(MimosaExceptionMessage.TOO_MANY_POST_BLOCKS_AMOUNT.getMessage(), MAX_BLOCKS_POST_AMOUNT));
        }
        postCreationDto.content().blocks().forEach(element -> {
            String type = element.type();

            if (TEXT_BLOCKS_FIELDS.containsKey(type)) {
                Map<String, Object> data = element.data();
                List<String> accessibleFields = TEXT_BLOCKS_FIELDS.get(type);

                int textLength = 0;
                try {
                    for (String field : accessibleFields) {
                        String dataValue = (String) data.get(field);
                        if (Objects.nonNull(dataValue)) {
                            textLength += dataValue.length();
                        }
                    }
                } catch (ClassCastException e) {
                    throw new Forbidden403Exception(MimosaExceptionMessage.POST_STRUCTURE_NOT_VALID.getMessage());
                }

                if (textLength > maxPostLength) {
                    throw new DataNotValidException(String.format(MimosaExceptionMessage.POST_TEXT_TOO_LARGE.getMessage(), MAX_POST_LENGTH));
                }
            }
        });
    }

    public boolean isProjectIssuedByPremiumUser(Project project) {
        return isOwnerPremium(project);
    }

    private boolean isOwnerPremium(Project project) {
        return project.getOwner().getIsPremium();
    }
}
