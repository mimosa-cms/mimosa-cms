package ua.com.webbrains.mimosa.cms.services;

import ua.com.webbrains.mimosa.cms.generated.dto.LoginRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LoginResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PasswordResetRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PermissionsResponseDto;

/**
 * Service for performing logic regarding authentication
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface AuthService {
    LoginResponseDto login(LoginRequestDto loginRequest, String verificationCode);

    LoginResponseDto refreshToken(String refreshToken);

    void sendPasswordRecoveryEmail(String emailOrUsername);

    void recoverPassword(PasswordResetRequestDto passwordResetRequestDto);

    PermissionsResponseDto getPermissions();

    byte[] get2faQrCodeUrl();

    void enable2fa(String code);

    void disable2fa(String code);
}
