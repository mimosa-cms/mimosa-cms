package ua.com.webbrains.mimosa.cms.models;

public enum PostType {
    SIMPLE, BLOCK_BASED
}
