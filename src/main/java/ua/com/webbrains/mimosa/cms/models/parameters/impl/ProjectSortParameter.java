package ua.com.webbrains.mimosa.cms.models.parameters.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.models.parameters.SortParameter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum ProjectSortParameter implements SortParameter {
    LINK("link", "link"),
    NAME("name", "name");

    private String value;
    private String fieldName;

    public static ProjectSortParameter fromString(String value) {
        return Arrays.stream(values())
                .filter((parameter -> value.equalsIgnoreCase(parameter.getValue())))
                .findFirst()
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.SORTING_PARAMETER_NOT_VALID.getMessage())));
    }
}
