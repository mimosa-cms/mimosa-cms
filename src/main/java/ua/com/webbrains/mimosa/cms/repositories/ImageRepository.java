package ua.com.webbrains.mimosa.cms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Image;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ImageRepository extends JpaRepository<Image, UUID> {
    List<Image> findAllByPostId(UUID postId);

    Optional<Image> findImageByIdAndPostId(UUID imageId, UUID postId);

    boolean existsByPostId(UUID postId);
}
