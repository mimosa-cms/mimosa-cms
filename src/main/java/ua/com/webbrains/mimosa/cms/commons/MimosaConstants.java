package ua.com.webbrains.mimosa.cms.commons;

import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.Map;

@UtilityClass
public class MimosaConstants {
    public static final String APP_NAME = "MimosaCMS";
    public static final String PERMISSIONS_FILE = "dictionary/permissions.json";
    public static final String CATEGORIES_FILE = "dictionary/categories.json";
    public static final long ACCESS_TOKEN_EXPIRATION = 3600 * 1000;
    public static final long REFRESH_TOKEN_EXPIRATION = 30L * 24 * 3600 * 1000;
    public static final String ACCESS_TOKEN_PREFIX = "Bearer ";
    public static final String ACCESS_TOKEN_HEADER_NAME = "Authorization";
    public static final String QR_CODE_SUFIX = "otpauth://totp/%s:%s?secret=%s&issuer=%s";
    public static final long VERIFICATION_EMAIL_DELAY = 3 * 60 * 1000;
    public static final long PASSWORD_EXPIRATION_TIME = 10 * 60 * 1000;
    public static final int MAX_FREE_PROJECTS_AMOUNT = 3;
    public static final int MAX_FREE_POSTS_PER_PROJECT_AMOUNT = 50;
    public static final int MAX_FREE_TRIAL_POST_LENGTH = 4096;
    public static final int MAX_FREE_BLOCKS_POST_AMOUNT = 40;
    public static final int MAX_BLOCKS_POST_AMOUNT = 100;
    public static final Map<String, List<String>> TEXT_BLOCKS_FIELDS = Map.of(
            "paragraph", List.of("text"),
            "warning", List.of("title", "message"),
            "code", List.of("code"),
            "raw", List.of("html"),
            "quote", List.of("text", "caption"),
            "header", List.of("text")
    );
    public static final int MAX_POST_LENGTH = 10000;
    public static final String LINK_PATTERN = "\\b(?:https?):\\/\\/[-A-Za-z0-9+&@#\\/%?=~_|!:,.;]*[-A-Za-z0-9+&@#\\/%=~_|]";
    public static final String AUTHOR_PATTERN = "^[A-Za-z]+(\\s+[A-Za-z]+)?$";
    public static final String SQL_ATTACK_PATTERN = ".*\\b(?:SELECT|INSERT|UPDATE|DELETE|DROP|UNION|ALTER|CREATE)\\b.*";
    public static final String SORTING_PATTERN = "^(asc|desc)$";
    public static final String POSTS_IMAGES_PATH = "posts" + System.lineSeparator();
    public static final String AVATARS_IMAGES_PATH = "avatars" + System.lineSeparator();
    public static final List<String> VALID_IMAGE_CONTENT_TYPES = List.of("image/jpeg", "image/png", "image/jpg", "image/webp", "image/svg+xml");
    public static final int MAX_IMAGES_AMOUNT_PER_POST = 3;
}
