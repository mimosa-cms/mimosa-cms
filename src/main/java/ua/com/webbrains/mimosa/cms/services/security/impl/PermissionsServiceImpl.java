package ua.com.webbrains.mimosa.cms.services.security.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import ua.com.webbrains.mimosa.cms.models.security.RolesAndPermissions;
import ua.com.webbrains.mimosa.cms.services.security.PermissionsLoaderService;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.PERMISSIONS_FILE;

@Service
@Slf4j
@RequiredArgsConstructor
public class PermissionsServiceImpl implements PermissionsLoaderService {
    private final ObjectMapper objectMapper;

    @Override
    @SneakyThrows
    public RolesAndPermissions loadRolesAndPermissionsFromResources() {
        ClassPathResource classPathResource = new ClassPathResource(PERMISSIONS_FILE);

        return objectMapper.readValue(classPathResource.getInputStream(), RolesAndPermissions.class);
    }
}
