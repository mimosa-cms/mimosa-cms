package ua.com.webbrains.mimosa.cms.services.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.AvatarMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImageMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImagesMetaResponseDto;
import ua.com.webbrains.mimosa.cms.models.mappers.ImageMapper;
import ua.com.webbrains.mimosa.cms.repositories.AvatarRepository;
import ua.com.webbrains.mimosa.cms.repositories.ImageRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Avatar;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Image;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;
import ua.com.webbrains.mimosa.cms.services.ImageDeleteService;
import ua.com.webbrains.mimosa.cms.services.ImageService;
import ua.com.webbrains.mimosa.cms.services.PostsService;
import ua.com.webbrains.mimosa.cms.services.files.FileService;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_IMAGES_AMOUNT_PER_POST;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.VALID_IMAGE_CONTENT_TYPES;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {
    private final AuthoritiesService authoritiesService;
    private final PostsService postsService;
    private final FileService fileService;
    private final ImageRepository imageRepository;
    private final AvatarRepository avatarRepository;
    private final ImageMapper imageMapper;
    private final ImageDeleteService imageDeleteService;

    @Override
    public ImagesMetaResponseDto uploadImages(UUID project, UUID post, List<MultipartFile> images) {
        if (CollectionUtils.isEmpty(images) || images.size() > MAX_IMAGES_AMOUNT_PER_POST) {
            throw new DataNotValidException(MimosaExceptionMessage.IMAGES_COLLECTION_NOT_VALID.getMessage());
        }

        Post currentPost = postsService.findPostEntityFromProject(project, post);

        List<ImageMetaResponseDto> imageMetas = new ArrayList<>();
        images.forEach(imageFile -> {
            if (imageFile == null || imageFile.isEmpty()) {
                throw new DataNotValidException(MimosaExceptionMessage.IMAGE_NOT_VALID.getMessage());
            }

            String contentType = imageFile.getContentType();
            validateContentType(contentType);
            UUID imageId = UUID.randomUUID();
            String filename = fileService.uploadPostImage(imageId.toString(), imageFile);

            Image image = Image.builder()
                    .id(imageId)
                    .post(currentPost)
                    .filename(filename)
                    .size(imageFile.getSize())
                    .mediaType(contentType)
                    .build();
            imageRepository.save(image);
            imageMetas.add(
                    imageMapper.entityToDto(image)
            );
        });

        return ImagesMetaResponseDto.builder()
                .images(imageMetas)
                .build();
    }

    @Override
    public ImagesMetaResponseDto findImagesMeta(UUID project, UUID post) {
        Post currentPost = postsService.findPostEntityFromProject(project, post);

        List<Image> postImages = imageRepository.findAllByPostId(currentPost.getId());

        return ImagesMetaResponseDto.builder()
                .images(postImages.stream().map(imageMapper::entityToDto).toList())
                .build();
    }

    @Override
    public ImageMetaResponseDto findImageMeta(UUID project, UUID post, UUID image) {
        return imageMapper.entityToDto(findImageEntityFromProject(project, post, image));
    }

    @Override
    public Resource findImage(UUID project, UUID post, UUID image) {
        Image postImage = findImageEntityFromProject(project, post, image);
        return fileService.retrievePostImage(postImage.getFilename());
    }

    @Override
    public void deleteImages(UUID project, UUID post, List<UUID> images) {
        Post currentPost = postsService.findPostEntityFromProject(project, post);

        imageDeleteService.deleteImages(currentPost.getId());
    }

    @Override
    public Resource findAvatar() {
        UUID currentUserId = UUID.fromString(authoritiesService.receiveCurrentUserId());
        Avatar avatar = avatarRepository.findByUserId(currentUserId)
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.AVATAR_NOT_FOUND.getMessage()));
        return fileService.retrieveAvatarImage(avatar.getFilename());
    }

    @Override
    public AvatarMetaResponseDto findAvatarMeta() {
        UUID currentUserId = UUID.fromString(authoritiesService.receiveCurrentUserId());
        Avatar avatar = avatarRepository.findByUserId(currentUserId)
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.AVATAR_NOT_FOUND.getMessage()));
        return imageMapper.entityToDto(avatar);
    }

    @Override
    public AvatarMetaResponseDto uploadAvatar(MultipartFile avatar) {
        if (avatar == null || avatar.isEmpty()) {
            throw new DataNotValidException(MimosaExceptionMessage.IMAGE_NOT_VALID.getMessage());
        }

        UUID currentUserId = UUID.fromString(authoritiesService.receiveCurrentUserId());
        String fileId = UUID.randomUUID().toString();
        String contentType = avatar.getContentType();
        validateContentType(contentType);

        avatarRepository.findByUserId(currentUserId).ifPresent(oldAvatar -> {
            avatarRepository.delete(oldAvatar);
            fileService.deleteAvatarImage(oldAvatar.getFilename());
        });

        String filename = fileService.uploadAvatarImage(fileId, avatar);
        Avatar image = new Avatar().setUserId(currentUserId);

        image.setId(UUID.fromString(fileId))
                .setFilename(filename)
                .setSize(avatar.getSize())
                .setMediaType(contentType);

        avatarRepository.save(image);

        return imageMapper.entityToDto(image);
    }

    @Override
    public void deleteAvatar() {
        UUID currentUserId = UUID.fromString(authoritiesService.receiveCurrentUserId());
        imageDeleteService.deleteAvatar(currentUserId);
    }

    private Image findImageEntityFromProject(UUID project, UUID post, UUID image) {
        Post currentPost = postsService.findPostEntityFromProject(project, post);
        return imageRepository.findImageByIdAndPostId(image, currentPost.getId())
                .orElseThrow(() ->
                        new DataNotFoundException(MimosaExceptionMessage.IMAGE_NOT_FOUND.getMessage()));
    }

    private void validateContentType(String contentType) {
        if (StringUtils.isEmpty(contentType) || !VALID_IMAGE_CONTENT_TYPES.contains(contentType)) {
            throw new DataNotValidException(
                    MimosaExceptionMessage.UNSUPPORTED_CONTENT_TYPE.getMessage().formatted(contentType)
            );
        }
    }
}
