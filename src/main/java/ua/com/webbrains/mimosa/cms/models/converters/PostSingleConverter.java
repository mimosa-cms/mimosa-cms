package ua.com.webbrains.mimosa.cms.models.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.dto.PostSingleDto;
import ua.com.webbrains.mimosa.cms.models.mappers.PostMapper;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.PostDetails;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl.BlockBasedPostDetails;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl.SimplePostDetails;

@Component
@RequiredArgsConstructor
public class PostSingleConverter {
    private static final String POST_DETAILS_UNSUPPORTED_TYPE_MESSAGE = "Unsupported post details type: %s";

    private final PostMapper postMapper;

    public PostSingleDto createPostSingleDto(Post post, PostDetails postDetails) {
        if (postDetails instanceof SimplePostDetails simplePostDetails) {
            return postMapper.toPostSingleDto(post, simplePostDetails);
        } else if (postDetails instanceof BlockBasedPostDetails blockBasedPostDetails) {
            return postMapper.toPostSingleDto(post, blockBasedPostDetails);
        } else {
            throw new IllegalArgumentException(POST_DETAILS_UNSUPPORTED_TYPE_MESSAGE.formatted(postDetails.getClass().getSimpleName()));
        }
    }
}
