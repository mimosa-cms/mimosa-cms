package ua.com.webbrains.mimosa.cms.commons.enums;

public enum Role {
  ADMIN,
  MODERATOR,
  USER,
  EMAIL_APPROVED
}
