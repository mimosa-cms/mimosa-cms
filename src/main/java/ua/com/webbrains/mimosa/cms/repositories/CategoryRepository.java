package ua.com.webbrains.mimosa.cms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ua.com.webbrains.mimosa.cms.repositories.entities.Category;

public interface CategoryRepository extends JpaRepository<Category, String> {
    @Query("SELECT c FROM Category c WHERE c.name LIKE %?1%")
    Page<Category> findAllByNameLike(String name, Pageable pageable);
}
