package ua.com.webbrains.mimosa.cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@EnableCaching
@EnableConfigurationProperties
@ConfigurationPropertiesScan
public class MimosaCmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(MimosaCmsApplication.class, args);
    }
}
