package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.CoworkersApiDelegate;
import ua.com.webbrains.mimosa.cms.generated.dto.AddCoworkerRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.CoworkersResponseDto;
import ua.com.webbrains.mimosa.cms.services.CoworkersService;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class CoworkersApiDelegateImpl implements CoworkersApiDelegate {
    private final CoworkersService coworkersService;

    @Override
    public ResponseEntity<Void> acceptCoworkerInvite(UUID token) {
        coworkersService.acceptInvite(token);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> addCoworkerToProject(UUID project, AddCoworkerRequestDto addCoworkerRequestDto) {
        coworkersService.addCoworkerToProject(project, addCoworkerRequestDto.username());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> deleteCoworkerFromProject(UUID project, String coworker) {
        coworkersService.deleteCoworkerFromProject(project, coworker);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<CoworkersResponseDto> getProjectCoworkers(UUID project) {
        return ResponseEntity.ok(coworkersService.getProjectCoworkers(project));
    }
}
