package ua.com.webbrains.mimosa.cms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Avatar;

import java.util.Optional;
import java.util.UUID;

public interface AvatarRepository extends JpaRepository<Avatar, UUID> {
    Optional<Avatar> findByUserId(UUID userId);
}
