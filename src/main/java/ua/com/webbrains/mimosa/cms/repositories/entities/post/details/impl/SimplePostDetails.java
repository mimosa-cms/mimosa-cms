package ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;
import ua.com.webbrains.mimosa.cms.repositories.entities.Link;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.PostDetails;

import java.util.List;

@Entity
@DiscriminatorValue("SIMPLE")
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class SimplePostDetails extends PostDetails {
    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "links", nullable = false, columnDefinition = "jsonb")
    @JdbcTypeCode(SqlTypes.JSON)
    private List<Link> links;
}
