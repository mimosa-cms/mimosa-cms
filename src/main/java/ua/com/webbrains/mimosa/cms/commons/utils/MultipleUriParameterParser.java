package ua.com.webbrains.mimosa.cms.commons.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class used to parse parameter string from url into map
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
@UtilityClass
public class MultipleUriParameterParser {
    private static final String SEPARATOR = ",";
    private static final String PAIR_SEPARATOR = "=";

    /** Parameter should be defined via comas and = character ex: param1=value1,param2=value2 */
    public Map<String, String> parseComplexParameter(String parameter) {
        if (StringUtils.isEmpty(parameter)) {
            return Map.of();
        }

        return Arrays.stream(parameter.replace(" ", "").split(SEPARATOR))
                .map(pair -> pair.split(PAIR_SEPARATOR, 2))
                .collect(Collectors.toMap(keyValue -> keyValue[0], keyValue -> keyValue.length > 1 ? keyValue[1] : ""));
    }
}
