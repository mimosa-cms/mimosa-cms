package ua.com.webbrains.mimosa.cms.repositories.querybuilder;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.ProjectFilterParameter;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.ProjectSortParameter;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;

import java.util.Map;
import java.util.UUID;

public interface ProjectQueryBuilder {
    Specification<Project> buildSearchProcessor(UUID userId, Map<ProjectFilterParameter, String> filters);

    Sort buildSortProcessor(Map<ProjectSortParameter, String> sortingParameters);
}
