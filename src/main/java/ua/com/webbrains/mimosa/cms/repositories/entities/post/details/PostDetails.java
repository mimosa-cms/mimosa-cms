package ua.com.webbrains.mimosa.cms.repositories.entities.post.details;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;

import java.util.UUID;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "post_type", discriminatorType = DiscriminatorType.STRING)
@Table(name = "post_details")
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public abstract class PostDetails {
    @Id
    private UUID id;

    @Column(name = "meta_words")
    private String metaWords;

    @Column(name = "meta_description")
    private String metaDescription;

    @Column(name = "meta_authors")
    private String metaAuthors;

    @OneToOne(mappedBy = "postDetails", fetch = FetchType.LAZY)
    private Post post;
}
