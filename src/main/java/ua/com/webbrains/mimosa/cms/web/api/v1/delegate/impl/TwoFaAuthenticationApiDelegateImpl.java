package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.TwoFaAuthenticationApiDelegate;
import ua.com.webbrains.mimosa.cms.services.AuthService;

@Component
@RequiredArgsConstructor
public class TwoFaAuthenticationApiDelegateImpl implements TwoFaAuthenticationApiDelegate {
    private final AuthService authService;

    @Override
    public ResponseEntity<Void> enable2fa(String code) {
        authService.enable2fa(code);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Resource> get2faQrCode() {
        return ResponseEntity.ok(new ByteArrayResource(authService.get2faQrCodeUrl()));
    }

    @Override
    public ResponseEntity<Void> disable2fa(String code) {
        authService.disable2fa(code);
        return ResponseEntity.ok().build();
    }
}
