package ua.com.webbrains.mimosa.cms.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.annotation.Validated;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.enums.Role;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataExistException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserRequestDto;
import ua.com.webbrains.mimosa.cms.repositories.UserRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.AdminService;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AdminServiceImpl implements AdminService {
    private final UserRepository userRepository;
    private final SmartValidator smartValidator;
    private final PasswordEncoder passwordEncoder;

    @Value("${application.admin.name}")
    private String adminName;

    @Value("${application.admin.surname}")
    private String adminSurname;

    @Value("${application.admin.email}")
    private String adminEmail;

    @Value("${application.admin.username}")
    private String adminUsername;

    @Value("${application.admin.phoneNumber}")
    private String adminPhone;

    @Value("${application.admin.password}")
    private String adminPassword;

    @Override
    @SneakyThrows
    @Transactional
    public void setupApplicationAdmin() {
        RegisterUserRequestDto adminDto = RegisterUserRequestDto.builder()
                .name(adminName)
                .surname(adminSurname)
                .email(adminEmail)
                .username(adminUsername)
                .phoneNumber(adminPhone)
                .password(adminPassword)
                .build();
        validateAdminModel(adminDto);

        User admin = User.builder()
                .name(adminName)
                .surname(adminSurname)
                .email(adminEmail)
                .username(adminUsername)
                .phoneNumber(adminPhone)
                .password(passwordEncoder.encode(adminPassword))
                .createdAt(Timestamp.valueOf(LocalDateTime.now()))
                .updatedAt(Timestamp.valueOf(LocalDateTime.now()))
                .role(Role.ADMIN)
                .isEmailApproved(true)
                .isPremium(true)
                .build();

        Optional<User> optionalUser = userRepository.findByUsernameOrEmail(adminUsername, adminEmail);
        if (optionalUser.isPresent()) {
            if (!optionalUser.get().getRole().equals(Role.ADMIN)) {
                throw new DataExistException(MimosaExceptionMessage.USER_BY_CREDENTIALS_EXISTS.getMessage());
            }

            User existedAdmin = optionalUser.get();
            log.info("Admin with such credentials already exists: " + existedAdmin);

            if (!passwordEncoder.matches(adminPassword, existedAdmin.getPassword())) {
                existedAdmin.setPassword(passwordEncoder.encode(adminPassword));
                userRepository.save(existedAdmin);
                log.info("Old admins password have been set to new one");
            } else {
                log.info("Admins password not updated");
            }
        } else {
            admin = userRepository.save(admin);
            log.info("Admin created: " + admin);
        }
    }

    private void validateAdminModel(@Validated RegisterUserRequestDto admin) {
        DataBinder dataBinder = new DataBinder(admin);
        BindingResult bindingResult = new BeanPropertyBindingResult(dataBinder.getTarget(), dataBinder.getObjectName());
        smartValidator.validate(admin, bindingResult);
        if (bindingResult.hasErrors()) {
            StringBuilder messageBuilder =
                    new StringBuilder("Passed data for admin is not valid, try again with correct data: ").append(System.lineSeparator());
            bindingResult.getFieldErrors()
                    .forEach(element -> messageBuilder.append(element.getDefaultMessage())
                            .append(": ")
                            .append(element.getRejectedValue())
                            .append(System.lineSeparator()));
            throw new DataNotValidException(messageBuilder.toString());
        }
    }
}
