package ua.com.webbrains.mimosa.cms.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jboss.aerogear.security.otp.Totp;
import org.jboss.aerogear.security.otp.api.Base32;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.enums.Role;
import ua.com.webbrains.mimosa.cms.commons.exceptions.BadRequest400Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.TooManyRequests429Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.CredentialsNotValidException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.OperationFailedException;
import ua.com.webbrains.mimosa.cms.commons.utils.QrCodeUtil;
import ua.com.webbrains.mimosa.cms.generated.dto.LoginRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LoginResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PasswordResetRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PermissionsResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.PasswordResetRepository;
import ua.com.webbrains.mimosa.cms.repositories.UserRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.PasswordReset;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.AuthService;
import ua.com.webbrains.mimosa.cms.services.EmailService;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;
import ua.com.webbrains.mimosa.cms.services.security.JwtService;

import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.PASSWORD_EXPIRATION_TIME;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.VERIFICATION_EMAIL_DELAY;
import static ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage.USER_BY_CREDENTIALS_NOT_FOUND;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {
    private static final String PASSWORD_RECOVERY_SUBJECT = "Mimosa CMS Password Recovery";
    private static final String PASSWORD_RECOVERY_TEMPLATE_PATH = "emails/passwordResetTemplate";

    private final UserRepository userRepository;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;
    private final AuthoritiesService authoritiesService;
    private final EmailService emailService;
    private final PasswordResetRepository passwordResetRepository;
    private final TemplateEngine templateEngine;

    @Value("${application.frontend.url}")
    private String frontEndUrl;

    @Value("${application.frontend.password-recovery}")
    private String passwordRecoveryEndpoint;

    @Override
    public LoginResponseDto login(LoginRequestDto loginRequest, String verificationCode) {
        String usernameOrEmail = loginRequest.emailOrUsername();
        String password = loginRequest.password();
        User currentUser = userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .orElseThrow(() -> new DataNotFoundException(USER_BY_CREDENTIALS_NOT_FOUND.getMessage()));

        if (currentUser.getIs2FaEnabled()) {
            if (StringUtils.isEmpty(verificationCode)) {
                throw new DataNotValidException(MimosaExceptionMessage.VERIFICATION_CODE_IS_REQUIRED.getMessage());
            }
            Totp totp = new Totp(currentUser.getTwoFaSecret());
            if (!isValidLong(verificationCode) || !totp.verify(verificationCode)) {
                throw new DataNotValidException(MimosaExceptionMessage.INVALID_VERIFICATION_CODE.getMessage());
            }
        }

        return handleLoginWithout2Fa(currentUser, password);
    }

    private boolean isValidLong(String code) {
        try {
            Long.parseLong(code);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private LoginResponseDto handleLoginWithout2Fa(User currentUser, String password) {
        if (!passwordEncoder.matches(password, currentUser.getPassword())) {
            throw new CredentialsNotValidException();
        }

        List<String> allPermissions = new ArrayList<>(authoritiesService.fetchPermissionsFromRole(currentUser.getRole()));

        if (currentUser.getIsEmailApproved()) {
            allPermissions.addAll(authoritiesService.fetchPermissionsFromRole(Role.EMAIL_APPROVED));
        }

        return LoginResponseDto.builder()
                .accessToken(jwtService.createAccessToken(currentUser.getId().toString(), allPermissions))
                .refreshToken(jwtService.createRefreshToken(currentUser.getId().toString()))
                .build();
    }

    @Override
    @Transactional
    public byte[] get2faQrCodeUrl() {
        User currentUser = userRepository.findById(UUID.fromString(authoritiesService.receiveCurrentUserId()))
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));
        if (currentUser.getIs2FaEnabled()) {
            throw new DataNotValidException(MimosaExceptionMessage.CANNOT_GENERATE_QR_CODE.getMessage());
        }
        try {
            String code = currentUser.getTwoFaSecret() == null ? Base32.random() : currentUser.getTwoFaSecret();
            currentUser.setTwoFaSecret(code);
            byte[] qrCode = QrCodeUtil.generateQrUrl(currentUser.getEmail(), code);
            userRepository.save(currentUser);
            return qrCode;
        } catch (UnsupportedEncodingException e) {
            throw new OperationFailedException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional
    public void enable2fa(String code) {
        User currentUser = userRepository.findById(UUID.fromString(authoritiesService.receiveCurrentUserId()))
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));
        if (!currentUser.getIs2FaEnabled()) {
            Totp totp = new Totp(currentUser.getTwoFaSecret());
            if (!totp.verify(code)) {
                throw new DataNotValidException(MimosaExceptionMessage.INVALID_VERIFICATION_CODE.getMessage());
            }
            currentUser.setIs2FaEnabled(true);
            userRepository.save(currentUser);
        }
    }

    @Override
    @Transactional
    public void disable2fa(String code) {
        User currentUser = userRepository.findById(UUID.fromString(authoritiesService.receiveCurrentUserId()))
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));
        if (currentUser.getIs2FaEnabled()) {
            Totp totp = new Totp(currentUser.getTwoFaSecret());
            if (!totp.verify(code)) {
                throw new DataNotValidException(MimosaExceptionMessage.INVALID_VERIFICATION_CODE.getMessage());
            }
            currentUser.setIs2FaEnabled(false);
            userRepository.save(currentUser);
        }
    }

    @Override
    public LoginResponseDto refreshToken(String refreshToken) {
        String userId = jwtService.parseRefreshToken(refreshToken);
        User currentUser = userRepository.findById(UUID.fromString(userId))
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));

        List<String> permissions = new ArrayList<>(authoritiesService.fetchPermissionsFromRole(currentUser.getRole()));
        if (currentUser.getIsEmailApproved()) {
            permissions.addAll(authoritiesService.fetchPermissionsFromRole(Role.EMAIL_APPROVED));
        }

        return LoginResponseDto.builder().refreshToken(refreshToken).accessToken(jwtService.createAccessToken(userId, permissions)).build();
    }

    @Override
    @Transactional
    public void sendPasswordRecoveryEmail(String emailOrUsername) {
        String email = userRepository.findByUsernameOrEmail(emailOrUsername, emailOrUsername)
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()))
                .getEmail();
        Timestamp currentTimestamp = Timestamp.valueOf(LocalDateTime.now());
        UUID verificationToken;
        Optional<PasswordReset> passwordResetOptional = passwordResetRepository.findByEmail(email);
        PasswordReset passwordResetRecord;

        if (passwordResetOptional.isEmpty()) {
            Timestamp expirationTimestamp = Timestamp.valueOf(LocalDateTime.now());
            verificationToken = UUID.randomUUID();
            passwordResetRecord = PasswordReset.builder().email(email).expirationDate(expirationTimestamp).verificationToken(verificationToken).build();
        } else {
            passwordResetRecord = passwordResetOptional.get();
            Timestamp expirationTime = passwordResetRecord.getExpirationDate();
            Timestamp sendAt = passwordResetRecord.getSendAt();
            long sendingDifferenceInMillis = Math.abs(currentTimestamp.getTime() - sendAt.getTime());
            long expirationTimeDifferenceInMillis = currentTimestamp.getTime() - expirationTime.getTime();

            if (sendingDifferenceInMillis <= VERIFICATION_EMAIL_DELAY) {
                throw new TooManyRequests429Exception(MimosaExceptionMessage.CANNOT_SEND_PASSWORD_RECOVERY_IN_LESS_THAN_3_MINUTES.getMessage());
            }

            if (expirationTimeDifferenceInMillis >= PASSWORD_EXPIRATION_TIME) {
                verificationToken = passwordResetRecord.getVerificationToken();
            } else {
                verificationToken = UUID.randomUUID();
                passwordResetRecord.setVerificationToken(verificationToken);
            }
        }

        Context context = new Context();
        context.setVariable("recoveryLink", frontEndUrl + passwordRecoveryEndpoint + "?token=" + verificationToken + "&email=" + email);
        String content = templateEngine.process(PASSWORD_RECOVERY_TEMPLATE_PATH, context);

        emailService.sendEmail(email, PASSWORD_RECOVERY_SUBJECT, content);
        passwordResetRecord.setSendAt(currentTimestamp);
        passwordResetRepository.save(passwordResetRecord);
    }

    @Override
    public PermissionsResponseDto getPermissions() {
        UUID userId = UUID.fromString(authoritiesService.receiveCurrentUserId());
        Role userRole = userRepository.findById(userId).orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()))
                .getRole();
        List<String> permissions = authoritiesService.fetchPermissionsFromRole(userRole);
        return PermissionsResponseDto.builder().permissions(permissions).build();
    }

    @Override
    @Transactional
    public void recoverPassword(PasswordResetRequestDto passwordResetRequestDto) {
        if (!passwordResetRequestDto.password().equals(passwordResetRequestDto.repeatPassword())) {
            throw new DataNotValidException(MimosaExceptionMessage.REPEAT_PASSWORD_AND_PASSWORD_DOES_NOT_MATCH_THE_SAME_VALUE.getMessage());
        }

        String password = passwordResetRequestDto.password();
        String email = passwordResetRequestDto.email();
        UUID token = passwordResetRequestDto.verificationToken();
        PasswordReset passwordReset = passwordResetRepository.findByEmail(email)
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.PASSWORD_RECOVERY_REQUEST_NOT_FOUND.getMessage()));
        Timestamp currentTimestamp = Timestamp.valueOf(LocalDateTime.now());

        long expirationTimeDifferenceInMillis = currentTimestamp.getTime() - passwordReset.getExpirationDate().getTime();
        if (expirationTimeDifferenceInMillis >= PASSWORD_EXPIRATION_TIME) {
            passwordResetRepository.delete(passwordReset);
            throw new BadRequest400Exception(MimosaExceptionMessage.PASSWORD_RECOVERY_TOKEN_IS_EXPIRED.getMessage());
        } else if (!passwordReset.getVerificationToken().equals(token)) {
            throw new DataNotValidException(MimosaExceptionMessage.PASSWORD_RECOVERY_TOKEN_IS_NOT_VALID.getMessage());
        }

        User recoveringUser =
                userRepository.findByEmail(email).orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.USER_NOT_FOUND.getMessage()));

        if (passwordEncoder.matches(password, recoveringUser.getPassword())) {
            throw new DataNotValidException(MimosaExceptionMessage.PASSWORD_CANNOT_BE_THE_SAME_AS_PREVIOUS_ONE.getMessage());
        }

        recoveringUser.setPassword(passwordEncoder.encode(password));
        passwordResetRepository.delete(passwordReset);
    }
}
