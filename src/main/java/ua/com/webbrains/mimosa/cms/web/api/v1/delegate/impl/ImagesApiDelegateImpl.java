package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import ua.com.webbrains.mimosa.cms.generated.api.ImagesApiDelegate;
import ua.com.webbrains.mimosa.cms.generated.dto.AvatarMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.DeletePostImagesRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImageMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImagesMetaResponseDto;
import ua.com.webbrains.mimosa.cms.services.ImageService;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ImagesApiDelegateImpl implements ImagesApiDelegate {
    private final ImageService imageService;

    @Override
    public ResponseEntity<ImagesMetaResponseDto> uploadPostImages(UUID project, UUID post, List<MultipartFile> images) {
        return ResponseEntity.status(HttpStatus.CREATED).body(imageService.uploadImages(project, post, images));
    }

    @Override
    public ResponseEntity<Void> deletePostImages(UUID project, UUID post, DeletePostImagesRequestDto deletePostImagesRequestDto) {
        imageService.deleteImages(project, post, deletePostImagesRequestDto.images());
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Resource> getPostImage(UUID project, UUID post, UUID image) {
        return ResponseEntity.ok(imageService.findImage(project, post, image));
    }

    @Override
    public ResponseEntity<ImageMetaResponseDto> getPostImageMeta(UUID project, UUID post, UUID image) {
        return ResponseEntity.ok(imageService.findImageMeta(project, post, image));
    }

    @Override
    public ResponseEntity<ImagesMetaResponseDto> getPostImages(UUID project, UUID post) {
        return ResponseEntity.ok(imageService.findImagesMeta(project, post));
    }

    @Override
    public ResponseEntity<Resource> getAvatar() {
        return ResponseEntity.ok(imageService.findAvatar());
    }

    @Override
    public ResponseEntity<AvatarMetaResponseDto> getAvatarMeta() {
        return ResponseEntity.ok(imageService.findAvatarMeta());
    }

    @Override
    public ResponseEntity<AvatarMetaResponseDto> uploadAvatar(MultipartFile avatar) {
        return ResponseEntity.ok(imageService.uploadAvatar(avatar));
    }

    @Override
    public ResponseEntity<Void> deleteAvatar() {
        imageService.deleteAvatar();
        return ResponseEntity.ok().build();
    }
}
