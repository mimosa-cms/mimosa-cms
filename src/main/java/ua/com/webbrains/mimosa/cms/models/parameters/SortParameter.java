package ua.com.webbrains.mimosa.cms.models.parameters;

/**
 * Interface for defining sorting parameters structure
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface SortParameter {
    String getValue();

    String getFieldName();
}
