package ua.com.webbrains.mimosa.cms.commons.utils;

import lombok.experimental.UtilityClass;
import net.glxn.qrgen.javase.QRCode;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.APP_NAME;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.QR_CODE_SUFIX;

@UtilityClass
public class QrCodeUtil {
    public byte[] generateQrUrl(String email, String secret) throws UnsupportedEncodingException {
        return stringToQrCode(QR_CODE_SUFIX.formatted(APP_NAME, email, secret, APP_NAME));
    }

    private byte[] stringToQrCode(String url) {
        ByteArrayOutputStream stream = QRCode.from(url).withSize(200, 200).stream();
        return stream.toByteArray();
    }
}
