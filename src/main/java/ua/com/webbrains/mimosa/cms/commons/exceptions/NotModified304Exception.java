package ua.com.webbrains.mimosa.cms.commons.exceptions;

import org.springframework.http.HttpStatus;

public class NotModified304Exception extends MimosaBaseException {
    public NotModified304Exception(String message, Exception cause) {
        super(message, cause, HttpStatus.NOT_MODIFIED);
    }

    public NotModified304Exception(String message) {
        super(message, HttpStatus.NOT_MODIFIED);
    }
}
