package ua.com.webbrains.mimosa.cms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.PostDetails;

import java.util.UUID;

public interface PostDetailsRepository extends JpaRepository<PostDetails, UUID> {}
