package ua.com.webbrains.mimosa.cms.commons.enums;

import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;

public enum PostStatus {
    PUBLISHED,
    DRAFT;

    public static PostStatus fromString(String value) {
        return switch (value) {
            case "PUBLISHED" -> PostStatus.PUBLISHED;
            case "DRAFT" -> PostStatus.DRAFT;
            default -> throw new DataNotValidException(MimosaExceptionMessage.POST_STATUS_NOT_VALID.getMessage());
        };
    }
}
