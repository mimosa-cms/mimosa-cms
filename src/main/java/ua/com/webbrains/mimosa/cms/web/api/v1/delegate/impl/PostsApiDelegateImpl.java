package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.PostsApiDelegate;
import ua.com.webbrains.mimosa.cms.generated.dto.BlockBasedPostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LinkCategoryDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostCategoriesResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostSingleDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostsResponseDto;
import ua.com.webbrains.mimosa.cms.services.CategoryService;
import ua.com.webbrains.mimosa.cms.services.PostsService;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class PostsApiDelegateImpl implements PostsApiDelegate {
    private final PostsService postsService;
    private final CategoryService categoryService;

    @Override
    public ResponseEntity<PostDto> createPostForProject(UUID project, PostRequestDto postRequestDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(postsService.createPostForProject(project, postRequestDto));
    }

    @Override
    public ResponseEntity<Void> deletePostForProject(UUID project, UUID post) {
        postsService.deletePostForProject(project, post);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<PostSingleDto> getProjectPost(UUID project, UUID post) {
        return ResponseEntity.ok(postsService.findPostFromProject(project, post));
    }

    @Override
    public ResponseEntity<PostsResponseDto> getProjectPosts(UUID project, Integer size, Integer page, String filter, String sort) {
        return ResponseEntity.ok(postsService.findPostsForProject(project, size, page, filter, sort));
    }

    @Override
    public ResponseEntity<PostDto> updatePostForProject(UUID project, UUID post, PostRequestDto postRequestDto) {
        return ResponseEntity.ok(postsService.updatePostForProject(project, post, postRequestDto));
    }

    @Override
    public ResponseEntity<PostDto> updatePostLikes(UUID project, UUID post, Integer value) {
        return ResponseEntity.ok(postsService.updatePostViews(project, post, value));
    }

    @Override
    public ResponseEntity<PostDto> updatePostStatus(UUID project, UUID post, String value) {
        return ResponseEntity.ok(postsService.updatePostStatus(project, post, value));
    }

    @Override
    public ResponseEntity<PostDto> updatePostViews(UUID project, UUID post, Integer value) {
        return ResponseEntity.ok(postsService.updatePostLikes(project, post, value));
    }

    @Override
    public ResponseEntity<List<LinkCategoryDto>> getPostLinkCategories() {
        return ResponseEntity.ok(postsService.retrieveLinkCategories());
    }

    @Override
    public ResponseEntity<PostCategoriesResponseDto> getPostCategories(Integer size, Integer page, String search) {
        return ResponseEntity.ok(categoryService.retrieveCategories(page, size, search));
    }

    @Override
    public ResponseEntity<PostDto> createBlockBasedPostForProject(UUID project, BlockBasedPostRequestDto blockBasedPostRequestDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(postsService.createPostForProject(project, blockBasedPostRequestDto));
    }

    @Override
    public ResponseEntity<PostDto> updateBlockBasedPostForProject(UUID project, UUID post, BlockBasedPostRequestDto blockBasedPostRequestDto) {
        return ResponseEntity.ok(postsService.updatePostForProject(project, post, blockBasedPostRequestDto));
    }
}
