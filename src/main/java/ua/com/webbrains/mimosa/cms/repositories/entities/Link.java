package ua.com.webbrains.mimosa.cms.repositories.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.com.webbrains.mimosa.cms.commons.enums.LinkCategory;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Link {
    private String name;
    private String value;
    private LinkCategory linkCategory;
}
