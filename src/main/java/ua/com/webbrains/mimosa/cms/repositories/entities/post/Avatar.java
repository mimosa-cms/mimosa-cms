package ua.com.webbrains.mimosa.cms.repositories.entities.post;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;

import java.util.UUID;

@Entity
@Table(name = "avatars")
@Getter
@Setter
@Builder
@NoArgsConstructor
@Accessors(chain = true)
@AllArgsConstructor
public class Avatar {
    @Id
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, insertable = false, updatable = false)
    private User user;
    @Column(name = "user_id", nullable = false)
    private UUID userId;

    @Column(name = "filename", nullable = false)
    private String filename;

    @Column(name = "size", nullable = false)
    private Long size;

    @Column(name = "media_type", nullable = false)
    private String mediaType;
}
