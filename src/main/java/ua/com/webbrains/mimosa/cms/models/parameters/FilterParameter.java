package ua.com.webbrains.mimosa.cms.models.parameters;

/**
 * Interface used for getting and wrapping complex uri filter parameters
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface FilterParameter {
    String getValue();
}
