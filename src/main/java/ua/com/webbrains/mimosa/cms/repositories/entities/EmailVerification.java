package ua.com.webbrains.mimosa.cms.repositories.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.UUID;

@Table(name = "email-verifications")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class EmailVerification {
    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "verification-token")
    private UUID verificationToken;

    @Column(name = "send-at")
    private Timestamp sendAt;
}
