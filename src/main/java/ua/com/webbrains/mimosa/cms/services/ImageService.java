package ua.com.webbrains.mimosa.cms.services;

import org.springframework.core.io.Resource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;
import ua.com.webbrains.mimosa.cms.generated.dto.AvatarMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImageMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImagesMetaResponseDto;

import java.util.List;
import java.util.UUID;

public interface ImageService {
    @PreAuthorize("hasRole('ROLE_UPDATE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    ImagesMetaResponseDto uploadImages(UUID project, UUID post, List<MultipartFile> images);

    @PreAuthorize("hasRole('ROLE_READ_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    ImagesMetaResponseDto findImagesMeta(UUID project, UUID post);

    @PreAuthorize("hasRole('ROLE_READ_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    ImageMetaResponseDto findImageMeta(UUID project, UUID post, UUID image);

    @PreAuthorize("hasRole('ROLE_READ_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    Resource findImage(UUID project, UUID post, UUID image);

    @PreAuthorize("hasRole('ROLE_DELETE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    void deleteImages(UUID project, UUID post, List<UUID> images);

    @PreAuthorize("hasRole('ROLE_READ_PROFILE') and hasRole('ROLE_EMAIL_APPROVED')")
    Resource findAvatar();

    @PreAuthorize("hasRole('ROLE_READ_PROFILE')")
    AvatarMetaResponseDto findAvatarMeta();

    @PreAuthorize("hasRole('ROLE_UPDATE_AVATAR') and hasRole('ROLE_EMAIL_APPROVED')")
    AvatarMetaResponseDto uploadAvatar(MultipartFile avatar);

    @PreAuthorize("hasRole('ROLE_UPDATE_AVATAR') and hasRole('ROLE_EMAIL_APPROVED')")
    void deleteAvatar();
}
