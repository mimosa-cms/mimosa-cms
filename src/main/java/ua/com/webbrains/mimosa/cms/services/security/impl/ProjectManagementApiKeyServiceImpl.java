package ua.com.webbrains.mimosa.cms.services.security.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.repositories.ProjectRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;
import ua.com.webbrains.mimosa.cms.services.security.JwtService;
import ua.com.webbrains.mimosa.cms.services.security.ProjectManagementApiKeyService;

import java.time.OffsetDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ProjectManagementApiKeyServiceImpl implements ProjectManagementApiKeyService {
    private final ProjectRepository projectRepository;
    private final AuthoritiesService authoritiesService;
    private final JwtService jwtService;

    @Override
    public String generateProjectManagementApiKey(UUID projectId, OffsetDateTime expirationDate) {
        UUID userId = UUID.fromString(authoritiesService.receiveCurrentUserId());
        Project project = projectRepository.findByAllowedById(userId, projectId).orElseThrow(
                () -> new DataNotFoundException(MimosaExceptionMessage.PROJECT_NOT_FOUND.getMessage()
                        .formatted(projectId))
        );

        expirationDate = expirationDate == null || OffsetDateTime.now().isAfter(expirationDate)
                ? OffsetDateTime.now().plusMonths(1) : expirationDate;

        return jwtService.createPostManagementApiKey(project.getId(), expirationDate);
    }
}
