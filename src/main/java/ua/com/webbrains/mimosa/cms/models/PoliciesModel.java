package ua.com.webbrains.mimosa.cms.models;

import lombok.Builder;

import java.util.Map;

@Builder
public record PoliciesModel(Map<String, String> policies) {}
