package ua.com.webbrains.mimosa.cms.commons.exceptions;

import org.springframework.http.HttpStatus;

/** Exception for case of 404 error */
public class NotFound404Exception extends MimosaBaseException {
    public NotFound404Exception(String message, Exception cause) {
        super(message, cause, HttpStatus.NOT_FOUND);
    }

    public NotFound404Exception(String message) {
        super(message, HttpStatus.NOT_FOUND);
    }
}
