package ua.com.webbrains.mimosa.cms.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;

import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID>, JpaSpecificationExecutor<Project> {
    Optional<Project> findByLinkAndOwnerId(String link, UUID ownerId);

    @Query("select p from Project p left join p.coworkers c where (c.id = :userId or p.owner.id = :userId) and p.id = :projectId")
    Optional<Project> findByAllowedById(UUID userId, UUID projectId);

    Optional<Project> findByIdAndOwnerId(UUID projectId, UUID ownerId);

    @Query("select count(p) from Project p left join p.coworkers c where c.id = :userId or p.owner.id = :userId")
    Integer countAllAllowed(UUID userId);

    Integer countAllByOwnerId(UUID ownerId);

    @Query("select p from Project p left join p.coworkers c where c.id = :userId or p.owner.id = :userId")
    Page<Project> findAllAllowed(UUID userId, Pageable pageable);
}
