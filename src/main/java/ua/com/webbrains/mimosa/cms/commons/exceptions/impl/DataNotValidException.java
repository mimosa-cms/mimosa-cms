package ua.com.webbrains.mimosa.cms.commons.exceptions.impl;

import ua.com.webbrains.mimosa.cms.commons.exceptions.BadRequest400Exception;

/**
 * Exception for case if data vas not valid
 */
public class DataNotValidException extends BadRequest400Exception {
    public DataNotValidException(String message, Exception cause) {
        super(message, cause);
    }

    public DataNotValidException(String message) {
        super(message);
    }
}
