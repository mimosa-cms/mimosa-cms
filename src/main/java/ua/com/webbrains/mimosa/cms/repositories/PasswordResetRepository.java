package ua.com.webbrains.mimosa.cms.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.PasswordReset;

import java.util.Optional;
import java.util.UUID;

public interface PasswordResetRepository extends JpaRepository<PasswordReset, String> {
    Optional<PasswordReset> findByEmail(String email);

    Optional<PasswordReset> findByVerificationToken(UUID verificationToken);
}
