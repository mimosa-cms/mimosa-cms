package ua.com.webbrains.mimosa.cms.models.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateUserInformationRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UserInformationResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Mapper(componentModel = "spring")
public interface UserMapper {
    @Mapping(source = "id", target = "id")
    RegisterUserResponseDto entityToRegisterResponseDto(User user);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "role", constant = "USER")
    @Mapping(target = "isEmailApproved", constant = "false")
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "projects", ignore = true)
    @Mapping(target = "isPremium", constant = "false")
    @Mapping(target = "is2FaEnabled", ignore = true)
    @Mapping(target = "twoFaSecret", ignore = true)
    User dtoToEntity(RegisterUserRequestDto dto);

    @Mapping(source = "username", target = "username")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "surname", target = "surname")
    UpdateUserInformationRequestDto entityToUpdateInformationResponseDto(User user);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "username", target = "username")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "surname", target = "surname")
    @Mapping(source = "updatedAt", target = "updatedAt", qualifiedByName = "toOffsetDateTime")
    @Mapping(source = "createdAt", target = "createdAt", qualifiedByName = "toOffsetDateTime")
    UserInformationResponseDto entityToInformationResponseDto(User user);

    @Named("toOffsetDateTime")
    default OffsetDateTime toOffsetDateTime(Timestamp value) {
        return value != null ? OffsetDateTime.ofInstant(value.toInstant(), ZoneOffset.UTC) : null;
    }
}
