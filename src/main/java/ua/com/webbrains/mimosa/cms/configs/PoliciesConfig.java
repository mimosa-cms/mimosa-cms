package ua.com.webbrains.mimosa.cms.configs;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import ua.com.webbrains.mimosa.cms.models.PoliciesModel;

import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class PoliciesConfig {
    private static final String POLICY_FILE = "classpath:dictionary/policy.json";

    private final ResourceLoader resourceLoader;
    private final ObjectMapper objectMapper;

    @Bean
    @SneakyThrows
    public PoliciesModel policiesModel() {
        Resource resource = resourceLoader.getResource(POLICY_FILE);
        Map<String, String> policies = objectMapper.readValue(resource.getInputStream(), new TypeReference<>() {
        });
        return PoliciesModel.builder().policies(policies).build();
    }
}
