package ua.com.webbrains.mimosa.cms.web.open.api.v1;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.ProjectManagementApiApiDelegate;
import ua.com.webbrains.mimosa.cms.generated.dto.ApiKeyResponseDto;
import ua.com.webbrains.mimosa.cms.services.security.ProjectManagementApiKeyService;

import java.time.OffsetDateTime;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ProjectManagementApiDelegateImpl implements ProjectManagementApiApiDelegate {
    private final ProjectManagementApiKeyService projectManagementApiKeyService;

    @Override
    public ResponseEntity<ApiKeyResponseDto> generateApiKey(UUID projectId, OffsetDateTime expirationDate) {
        expirationDate = expirationDate == null || OffsetDateTime.now().isAfter(expirationDate)
                ? OffsetDateTime.now().plusMonths(1) : expirationDate;
        return ResponseEntity.ok(ApiKeyResponseDto.builder()
                .apiKey(projectManagementApiKeyService.generateProjectManagementApiKey(projectId, expirationDate))
                .build());
    }
}
