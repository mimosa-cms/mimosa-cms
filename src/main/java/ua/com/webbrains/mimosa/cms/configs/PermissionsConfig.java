package ua.com.webbrains.mimosa.cms.configs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.com.webbrains.mimosa.cms.models.security.RolesAndPermissions;
import ua.com.webbrains.mimosa.cms.services.security.PermissionsLoaderService;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class PermissionsConfig {

    private final PermissionsLoaderService permissionsLoaderService;

    @Bean
    public RolesAndPermissions rolesAndPermissions() {
        RolesAndPermissions rolesAndPermissions = permissionsLoaderService.loadRolesAndPermissionsFromResources();
        log.info(String.format("Roles and permissions are initialized: %s", rolesAndPermissions.toString()));
        return rolesAndPermissions;
    }
}
