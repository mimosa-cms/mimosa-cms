package ua.com.webbrains.mimosa.cms.commons.exceptions;

import org.springframework.http.HttpStatus;

public class TooManyRequests429Exception extends MimosaBaseException {
    public TooManyRequests429Exception(String message, Exception cause) {
        super(message, cause, HttpStatus.TOO_MANY_REQUESTS);
    }

    public TooManyRequests429Exception(String message) {
        super(message, HttpStatus.TOO_MANY_REQUESTS);
    }
}
