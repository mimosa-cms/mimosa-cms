package ua.com.webbrains.mimosa.cms.commons.exceptions.impl;

import ua.com.webbrains.mimosa.cms.commons.exceptions.Unauthorized401Exception;

import static ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage.USER_BY_CREDENTIALS_NOT_FOUND;

public class CredentialsNotValidException extends Unauthorized401Exception {
    public CredentialsNotValidException(Exception cause) {
        super(USER_BY_CREDENTIALS_NOT_FOUND.getMessage(), cause);
    }

    public CredentialsNotValidException() {
        super(USER_BY_CREDENTIALS_NOT_FOUND.getMessage());
    }
}
