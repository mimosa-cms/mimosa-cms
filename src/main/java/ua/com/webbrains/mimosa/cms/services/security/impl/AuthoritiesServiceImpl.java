package ua.com.webbrains.mimosa.cms.services.security.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.enums.Role;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Unauthorized401Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.models.security.RolesAndPermissions;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthoritiesServiceImpl implements AuthoritiesService {
    private static final String AUTHORITY_PREFIX = "ROLE_";

    private final RolesAndPermissions rolesAndPermissions;

    @Override
    public List<GrantedAuthority> fetchAuthoritiesFromPermissions(List<String> permissions) {
        return new ArrayList<>(permissions.stream().map(element -> new SimpleGrantedAuthority(AUTHORITY_PREFIX + element)).toList());
    }

    @Override
    public List<String> fetchPermissionsFromRole(Role role) {
        List<String> permissions;
        switch (role) {
            case ADMIN -> permissions = rolesAndPermissions.admin();
            case MODERATOR -> permissions = rolesAndPermissions.moderator();
            case USER -> permissions = rolesAndPermissions.user();
            case EMAIL_APPROVED -> permissions = rolesAndPermissions.emailApproved();
            default -> throw new DataNotValidException(MimosaExceptionMessage.ROLE_NOT_FOUND.getMessage());
        }
        return permissions;
    }

    @Override
    public String receiveCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!isUserAuthenticated(authentication)) {
            throw new Unauthorized401Exception(MimosaExceptionMessage.USER_NOT_AUTHORIZED.getMessage());
        }
        return authentication.getName();
    }

    private boolean isUserAuthenticated(Authentication authentication) {
        if (authentication == null || AnonymousAuthenticationToken.class.isAssignableFrom(authentication.getClass())) {
            return false;
        }
        return authentication.isAuthenticated();
    }
}
