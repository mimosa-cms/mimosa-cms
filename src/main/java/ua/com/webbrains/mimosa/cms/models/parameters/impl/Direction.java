package ua.com.webbrains.mimosa.cms.models.parameters.impl;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Direction {
    public static final String ASC = "asc";
    public static final String DESC = "desc";
}
