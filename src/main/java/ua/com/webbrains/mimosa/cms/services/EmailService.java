package ua.com.webbrains.mimosa.cms.services;

/**
 * Service for sending emails
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface EmailService {
    void sendEmail(String emailTo, String subject, String content);
}
