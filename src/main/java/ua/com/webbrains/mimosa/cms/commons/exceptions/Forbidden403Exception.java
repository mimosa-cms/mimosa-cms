package ua.com.webbrains.mimosa.cms.commons.exceptions;

import org.springframework.http.HttpStatus;

public class Forbidden403Exception extends MimosaBaseException {
    public Forbidden403Exception(String message, Exception cause) {
        super(message, cause, HttpStatus.FORBIDDEN);
    }

    public Forbidden403Exception(String message) {
        super(message, HttpStatus.FORBIDDEN);
    }
}
