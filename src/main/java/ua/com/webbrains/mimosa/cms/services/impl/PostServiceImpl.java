package ua.com.webbrains.mimosa.cms.services.impl;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ua.com.webbrains.mimosa.cms.commons.enums.LinkCategory;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.enums.PostStatus;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.commons.utils.MultipleUriParameterParser;
import ua.com.webbrains.mimosa.cms.generated.dto.BlockBasedPostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LinkCategoryDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LinkDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostSingleDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostsResponseDto;
import ua.com.webbrains.mimosa.cms.models.PostType;
import ua.com.webbrains.mimosa.cms.models.converters.PostSingleConverter;
import ua.com.webbrains.mimosa.cms.models.mappers.PostMapper;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.PostFilterParameter;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.PostSortParameter;
import ua.com.webbrains.mimosa.cms.repositories.PostDetailsRepository;
import ua.com.webbrains.mimosa.cms.repositories.PostRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Link;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.PostDetails;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl.BlockBasedPostDetails;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl.SimplePostDetails;
import ua.com.webbrains.mimosa.cms.repositories.querybuilder.PostQueryBuilder;
import ua.com.webbrains.mimosa.cms.services.PostsService;
import ua.com.webbrains.mimosa.cms.services.ProjectService;
import ua.com.webbrains.mimosa.cms.services.UserService;
import ua.com.webbrains.mimosa.cms.services.validation.SubscriptionValidator;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.AUTHOR_PATTERN;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.LINK_PATTERN;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_FREE_POSTS_PER_PROJECT_AMOUNT;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.SORTING_PATTERN;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.SQL_ATTACK_PATTERN;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostsService {
    private static final String AUTHORS_SEPARATOR = ",";

    private final PostRepository postRepository;
    private final PostDetailsRepository postDetailsRepository;
    private final PostMapper postMapper;
    private final SubscriptionValidator subscriptionValidator;
    private final ProjectService projectService;
    private final PostSingleConverter postSingleConverter;
    private final PostQueryBuilder postQueryBuilder;
    private final UserService userService;
    private final EntityManager entityManager;

    @Override
    public List<LinkCategoryDto> retrieveLinkCategories() {
        return LinkCategory.toDtoList();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "project", key = "'projectId-' + #projectId")
    public PostDto createPostForProject(UUID projectId, PostRequestDto creationDto) {
        User user = userService.findCurrentUser();
        Project userProject = projectService.findProjectByIdAndUserId(projectId, user.getId());

        subscriptionValidator.validatePostForSubscriptionLimits(userProject, creationDto);

        if (isPostAllowedToBeCreated(userProject)) {
            throw new Forbidden403Exception(MimosaExceptionMessage.REACHED_AMOUNT_OF_FREE_POSTS.getMessage());
        }

        List<Link> links = validateLinksAndMapToEntities(creationDto.links());
        validateAuthors(creationDto.authors());
        
        PostDetails postDetails = createSimplePostDetails(creationDto, links);

        Post post = createPostEntity(user, userProject, PostStatus.fromString(creationDto.postStatus()), creationDto.title(), creationDto.description(), postDetails,
                creationDto.authors(), creationDto.category(), creationDto.tags(), PostType.SIMPLE);

        postDetailsRepository.save(postDetails);
        post = postRepository.save(post);

        return postMapper.fromEntityToPostDto(post);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(value = "project", key = "'projectId-' + #projectId")
    public PostDto createPostForProject(UUID projectId, BlockBasedPostRequestDto creationDto) {
        validateAuthors(creationDto.authors());

        User user = userService.findCurrentUser();
        Project userProject = projectService.findProjectByIdAndUserId(projectId, user.getId());

        subscriptionValidator.validatePostForSubscriptionLimits(userProject, creationDto);

        if (isPostAllowedToBeCreated(userProject)) {
            throw new Forbidden403Exception(MimosaExceptionMessage.REACHED_AMOUNT_OF_FREE_POSTS.getMessage());
        }

        PostDetails postDetails = createBlockBasedPostDetails(creationDto);

        Post post = createPostEntity(user, userProject, PostStatus.fromString(creationDto.postStatus()), creationDto.title(), creationDto.description(), postDetails,
                creationDto.authors(), creationDto.category(), creationDto.tags(), PostType.BLOCK_BASED);

        postDetailsRepository.save(postDetails);
        post = postRepository.save(post);

        return postMapper.fromEntityToPostDto(post);
    }

    private PostDetails createSimplePostDetails(PostRequestDto creationDto, List<Link> links) {
        return new SimplePostDetails()
                .setLinks(links)
                .setText(creationDto.text())
                .setId(UUID.randomUUID())
                .setMetaWords(creationDto.metaWords())
                .setMetaAuthors(creationDto.metaAuthors())
                .setMetaDescription(creationDto.metaDescription());
    }
    
    private boolean isPostAllowedToBeCreated(Project project) {
        return !subscriptionValidator.isProjectIssuedByPremiumUser(project)
                && postRepository.countAllByProjectId(project.getId()) >= MAX_FREE_POSTS_PER_PROJECT_AMOUNT;
    }

    private PostDetails createBlockBasedPostDetails(
            BlockBasedPostRequestDto creationDto) {
        return new BlockBasedPostDetails().setContent(new BlockBasedPostDetails.Content().setTime(creationDto.content().time())
                        .setBlocks(creationDto.content()
                                .blocks()
                                .stream()
                                .map(blockDto -> new BlockBasedPostDetails.Block().setType(blockDto.type()).setData(blockDto.data()))
                                .toList()))
                .setId(UUID.randomUUID())
                .setMetaWords(creationDto.metaWords())
                .setMetaAuthors(creationDto.metaAuthors())
                .setMetaDescription(creationDto.metaDescription());
    }

    private Post createPostEntity(
            User user, Project userProject, PostStatus postStatus,
            String title, String description, PostDetails postDetails,
            String authors, String category, List<String> tags, PostType postType) {
        return Post.builder()
                .id(UUID.randomUUID())
                .project(userProject)
                .creatorId(user.getId())
                .postStatus(postStatus)
                .postDetails(postDetails)
                .title(title)
                .description(description)
                .authors(authors)
                .createdAt(OffsetDateTime.now())
                .updatedAt(OffsetDateTime.now())
                .category(category)
                .tags(tags)
                .postType(postType)
                .likes(0)
                .views(0)
                .build();
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "post", key = "'projectId-' + #projectId + '-postId-' + #postId"),
            @CacheEvict(value = "project", key = "'projectId-' + #projectId")
    })
    @Transactional(rollbackFor = Exception.class)
    public PostDto updatePostForProject(UUID projectId, UUID postId, PostRequestDto updatingDto) {
        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        subscriptionValidator.validatePostForSubscriptionLimits(project, updatingDto);
        validateAuthors(updatingDto.authors());

        Post post = postRepository.findPostByIdAndProjectIdAndPostType(postId, project.getId(), PostType.SIMPLE)
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.POST_NOT_FOUND.getMessage()));
        PostDetails postDetails = post.getPostDetails();
        List<Link> links = validateLinksAndMapToEntities(updatingDto.links());

        updatePostSimpleInformation(post, postDetails, updatingDto, links);

        post = postRepository.save(post);
        postDetailsRepository.save(postDetails);

        return postMapper.fromEntityToPostDto(post);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "post", key = "'projectId-' + #projectId + '-postId-' + #postId"),
            @CacheEvict(value = "project", key = "'projectId-' + #projectId")
    })
    @Transactional(rollbackFor = Exception.class)
    public PostDto updatePostForProject(UUID projectId, UUID postId, BlockBasedPostRequestDto updatingDto) {
        validateAuthors(updatingDto.authors());

        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        subscriptionValidator.validatePostForSubscriptionLimits(project, updatingDto);

        Post post = postRepository.findPostByIdAndProjectIdAndPostType(postId, project.getId(), PostType.BLOCK_BASED)
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.POST_NOT_FOUND.getMessage()));
        PostDetails postDetails = post.getPostDetails();

        updatePostInformation(post, postDetails, updatingDto.title(), updatingDto.description(), PostStatus.fromString(updatingDto.postStatus()), updatingDto.authors(),
                updatingDto.category(), updatingDto.metaWords(), updatingDto.metaAuthors(), updatingDto.metaDescription(), updatingDto.tags());
        updateBlockBasedPostInformation(postDetails, updatingDto);

        post = postRepository.save(post);
        postDetailsRepository.save(postDetails);

        return postMapper.fromEntityToPostDto(post);
    }

    private void updatePostSimpleInformation(
            Post post, PostDetails postDetails, PostRequestDto updatingDto, List<Link> links) {
        updatePostInformation(post, postDetails, updatingDto.title(), updatingDto.description(), PostStatus.fromString(updatingDto.postStatus()), updatingDto.authors(),
                updatingDto.category(), updatingDto.metaWords(), updatingDto.metaAuthors(), updatingDto.metaDescription(), updatingDto.tags());

        if (postDetails instanceof SimplePostDetails simplePostDetails) {
            simplePostDetails.setLinks(links);
            simplePostDetails.setText(updatingDto.text());
        } else {
            throw new Forbidden403Exception(MimosaExceptionMessage.WRONG_POST_TYPE_FOR_THE_ACTION.getMessage().formatted("simple post"));
        }
    }

    private void updatePostInformation(
            Post post, PostDetails postDetails, String title,
            String description, PostStatus postStatus, String authors,
            String category, String metaWords, String metaAuthors,
            String metaDescription, List<String> tags) {
        post.setTitle(title);
        post.setDescription(description);
        post.setPostStatus(postStatus);
        post.setAuthors(authors);
        post.setCategory(category);

        postDetails.setMetaWords(metaWords);
        postDetails.setMetaAuthors(metaAuthors);
        postDetails.setMetaDescription(metaDescription);

        post.setTags(tags);
    }

    private void updateBlockBasedPostInformation(PostDetails postDetails, BlockBasedPostRequestDto updatingDto) {
        if (postDetails instanceof BlockBasedPostDetails blockBasedPostDetails) {
            blockBasedPostDetails.setContent(new BlockBasedPostDetails.Content().setTime(updatingDto.content().time())
                    .setBlocks(updatingDto.content()
                            .blocks()
                            .stream()
                            .map(blockDto -> new BlockBasedPostDetails.Block().setData(blockDto.data()).setType(blockDto.type()))
                            .toList()));
        } else {
            throw new Forbidden403Exception(MimosaExceptionMessage.WRONG_POST_TYPE_FOR_THE_ACTION.getMessage().formatted("block based"));
        }
    }

    private void validateAuthors(String authors) {
        Arrays.stream(authors.split(AUTHORS_SEPARATOR)).forEach((author) -> {
            if (!validateAuthor(author)) {
                throw new DataNotValidException(String.format(MimosaExceptionMessage.AUTHOR_IS_NOT_VALID.getMessage(), author));
            }
        });
    }

    private boolean validateAuthor(String author) {
        return author.trim().matches(AUTHOR_PATTERN);
    }

    private List<Link> validateLinksAndMapToEntities(List<LinkDto> linkDtos) {
        return linkDtos.stream().map(linkDto -> {
            if (!validateLinkDto(linkDto)) {
                throw new DataNotValidException(String.format(MimosaExceptionMessage.LINK_NOT_VALID.getMessage(), linkDto.value()));
            }
            return Link.builder()
                    .value(linkDto.value())
                    .name(linkDto.name())
                    .linkCategory(LinkCategory.fromString(linkDto.linkCategory()))
                    .build();
        }).toList();
    }

    private boolean validateLinkDto(LinkDto linkDto) {
        return linkDto.value().matches(LINK_PATTERN);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "post", key = "'projectId-' + #projectId + '-postId-' + #postId"),
            @CacheEvict(value = "project", key = "'projectId-' + #projectId")
    })
    @Transactional(rollbackFor = Exception.class)
    public void deletePostForProject(UUID projectId, UUID postId) {
        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        Post post = postRepository.findPostByIdAndProjectId(postId, project.getId())
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.POST_NOT_FOUND.getMessage(), postId)));

        postRepository.delete(post);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PostsResponseDto findPostsForProject(UUID projectId, Integer size, Integer page, String filter, String sort) {
        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        Integer maxPosts = subscriptionValidator.isProjectIssuedByPremiumUser(project) ? null : MAX_FREE_POSTS_PER_PROJECT_AMOUNT;
        Integer postsAmount = postRepository.countAllByProjectId(project.getId());

        if (StringUtils.hasText(filter) || StringUtils.hasText(sort)) {
            Map<PostFilterParameter, String> validatedFilterParameters = validateFilterParameters(MultipleUriParameterParser.parseComplexParameter(filter));
            Map<PostSortParameter, String> validatedSortParameters = validateSortParameters(MultipleUriParameterParser.parseComplexParameter(sort));

            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Post> query = postQueryBuilder.buildSearchProcessor(cb, project.getId(), validatedFilterParameters);
            Root<Post> root = query.from(Post.class);

            // Apply sorting
            List<Order> orders = validatedSortParameters.entrySet().stream()
                    .map(entry -> {
                        String direction = entry.getValue();
                        String fieldName = entry.getKey().getFieldName();
                        return direction.equalsIgnoreCase("ASC") ? cb.asc(root.get(fieldName)) : cb.desc(root.get(fieldName));
                    })
                    .collect(Collectors.toList());
            query.orderBy(orders);

            // Execute query
            TypedQuery<Post> typedQuery = entityManager.createQuery(query);
            long count = typedQuery.getResultList().size();
            typedQuery.setFirstResult(page * size);
            typedQuery.setMaxResults(size);
            List<Post> postPage = typedQuery.getResultList();

            return PostsResponseDto.builder()
                    .posts(postPage.stream().map(postMapper::fromEntityToPostDto).toList())
                    .size(size)
                    .page(page)
                    .numberOfPages(calculateTotalPages(size, count))
                    .maxPosts(maxPosts)
                    .postsAmount(postsAmount)
                    .build();
        } else {
            Page<Post> postPage = postRepository.findPostsByProjectId(project.getId(), PageRequest.of(page, size));

            return PostsResponseDto.builder()
                    .posts(postPage.map(postMapper::fromEntityToPostDto).toList())
                    .size(size)
                    .page(page)
                    .numberOfPages(BigDecimal.valueOf(postPage.getTotalPages()))
                    .maxPosts(maxPosts)
                    .postsAmount(postsAmount)
                    .build();
        }
    }

    private BigDecimal calculateTotalPages(long pageSize, long totalElements) {
        if (pageSize <= 0 || totalElements < 0) {
            throw new IllegalArgumentException("Invalid input values");
        }

        long totalPages = totalElements / pageSize;
        if (totalElements % pageSize != 0) {
            totalPages++;
        }

        return BigDecimal.valueOf(totalPages);
    }

    private Map<PostFilterParameter, String> validateFilterParameters(Map<String, String> filterParameters) {
        return filterParameters.entrySet().stream().map(this::validateFilterParameter).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map.Entry<PostFilterParameter, String> validateFilterParameter(Map.Entry<String, String> filterParameter) {
        PostFilterParameter keyFilter = PostFilterParameter.fromString(filterParameter.getKey());
        String valueFilter = filterParameter.getValue();

        if (valueFilter.matches(SQL_ATTACK_PATTERN)) {
            throw new Forbidden403Exception(String.format(MimosaExceptionMessage.POTENTIAL_SQL_ATTACK_DETECTED.getMessage(), valueFilter));
        }

        return new AbstractMap.SimpleEntry<>(keyFilter, valueFilter);
    }

    private Map<PostSortParameter, String> validateSortParameters(Map<String, String> sortParameters) {
        return sortParameters.entrySet().stream().map(this::validateSortParameter).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private Map.Entry<PostSortParameter, String> validateSortParameter(Map.Entry<String, String> sortParameter) {
        PostSortParameter keyFilter = PostSortParameter.fromString(sortParameter.getKey());
        String valueFilter = sortParameter.getValue();

        if (!valueFilter.matches(SORTING_PATTERN)) {
            throw new DataNotValidException(MimosaExceptionMessage.SORTING_PARAMETER_NOT_VALID.getMessage());
        }

        return new AbstractMap.SimpleEntry<>(keyFilter, valueFilter);
    }

    @Override
    @Cacheable(value = "post", key = "'projectId-' + #projectId + '-postId-' + #postId")
    public PostSingleDto findPostFromProject(UUID projectId, UUID postId) {
        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        Post post = postRepository.findPostByIdAndProjectId(postId, project.getId())
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.POST_NOT_FOUND.getMessage()));
        PostDetails postDetails = post.getPostDetails();

        return postSingleConverter.createPostSingleDto(post, postDetails);
    }

    @Override
    public Post findPostEntityFromProject(UUID projectId, UUID postId) {
        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        return postRepository.findPostByIdAndProjectId(postId, project.getId())
                .orElseThrow(() -> new DataNotFoundException(MimosaExceptionMessage.POST_NOT_FOUND.getMessage()));
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "post", key = "'projectId-' + #projectId + '-postId-' + #postId"),
            @CacheEvict(value = "project", key = "'projectId-' + #projectId")
    })
    @Transactional(rollbackFor = Exception.class)
    public PostDto updatePostViews(UUID projectId, UUID postId, Integer views) {
        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        Post post = postRepository.findPostByIdAndProjectId(postId, project.getId())
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.POST_NOT_FOUND.getMessage(), postId)));

        int result = post.getViews() + views;
        post.setViews(Math.max(result, 0));
        post = postRepository.save(post);

        return postMapper.fromEntityToPostDto(post);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "post", key = "'projectId-' + #projectId + '-postId-' + #postId"),
            @CacheEvict(value = "project", key = "'projectId-' + #projectId")
    })
    @Transactional(rollbackFor = Exception.class)
    public PostDto updatePostLikes(UUID projectId, UUID postId, Integer likes) {
        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        Post post = postRepository.findPostByIdAndProjectId(postId, project.getId())
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.POST_NOT_FOUND.getMessage(), postId)));

        int result = post.getLikes() + likes;
        post.setLikes(Math.max(result, 0));
        post = postRepository.save(post);

        return postMapper.fromEntityToPostDto(post);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "post", key = "'projectId-' + #projectId + '-postId-' + #postId"),
            @CacheEvict(value = "project", key = "'projectId-' + #projectId")
    })
    @Transactional(rollbackFor = Exception.class)
    public PostDto updatePostStatus(UUID projectId, UUID postId, String status) {
        PostStatus postStatus = PostStatus.fromString(status.toUpperCase());
        User user = userService.findCurrentUser();
        Project project = projectService.findProjectByIdAndUserId(projectId, user.getId());

        Post post = postRepository.findPostByIdAndProjectId(postId, project.getId())
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.POST_NOT_FOUND.getMessage(), postId)));

        post.setPostStatus(postStatus);
        post = postRepository.save(post);

        return postMapper.fromEntityToPostDto(post);
    }
}
