package ua.com.webbrains.mimosa.cms.commons.exceptions.impl;

import ua.com.webbrains.mimosa.cms.commons.exceptions.NotFound404Exception;

public class DataNotFoundException extends NotFound404Exception {
    public DataNotFoundException(String message, Exception cause) {
        super(message, cause);
    }

    public DataNotFoundException(String message) {
        super(message);
    }
}
