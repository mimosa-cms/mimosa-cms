package ua.com.webbrains.mimosa.cms.services.files.impl;

import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.MinioException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.OperationFailedException;
import ua.com.webbrains.mimosa.cms.services.files.FileService;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.AVATARS_IMAGES_PATH;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.POSTS_IMAGES_PATH;

@Service
@RequiredArgsConstructor
@Slf4j
public class FileServiceImpl implements FileService {
    private static final String DELIMITER = "_";

    private final MinioClient minioClient;

    @Value("${minio.bucket.name}")
    private String bucketName;

    @Override
    public String uploadPostImage(String uniqueIdentifier, MultipartFile file) {
        return uploadFile(POSTS_IMAGES_PATH, uniqueIdentifier, file);
    }

    @Override
    public String uploadAvatarImage(String uniqueIdentifier, MultipartFile file) {
        return uploadFile(AVATARS_IMAGES_PATH, uniqueIdentifier, file);
    }

    @Override
    public Resource retrievePostImage(String filename) {
        try {
            return new InputStreamResource(
                    minioClient.getObject(GetObjectArgs.builder()
                            .bucket(bucketName)
                            .object(filename)
                            .build()));
        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("Error retrieving post image: {}", e.getMessage());
            throw new OperationFailedException(MimosaExceptionMessage.FAILED_TO_RETRIEVE_IMAGE.getMessage(), e);
        }
    }

    @Override
    public void deletePostImage(String filename) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder()
                            .bucket(bucketName)
                            .object(filename)
                    .build());
        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("Error deleting post image: {}", e.getMessage());
            throw new OperationFailedException(MimosaExceptionMessage.FAILED_TO_DELETE_IMAGE.getMessage(), e);
        }
    }

    @Override
    public Resource retrieveAvatarImage(String filename) {
        try {
            return new InputStreamResource(
                    minioClient.getObject(GetObjectArgs.builder()
                            .bucket(bucketName)
                            .object(filename)
                            .build()));
        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("Error retrieving avatar image: {}", e.getMessage());
            throw new OperationFailedException(MimosaExceptionMessage.FAILED_TO_RETRIEVE_IMAGE.getMessage(), e);
        }
    }

    @Override
    public void deleteAvatarImage(String filename) {
        try {
            minioClient.removeObject(RemoveObjectArgs.builder()
                    .bucket(bucketName)
                    .object(filename)
                    .build());
        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("Error deleting avatar image: {}", e.getMessage());
            throw new OperationFailedException(MimosaExceptionMessage.FAILED_TO_DELETE_IMAGE.getMessage(), e);
        }
    }

    private String uploadFile(String folderName, String uniqueIdentifier, MultipartFile file) {
        try (InputStream inputStream = file.getInputStream()) {
            String filename = folderName + uniqueIdentifier + DELIMITER + OffsetDateTime.now() + DELIMITER + file.getOriginalFilename();
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object(filename)
                    .stream(inputStream, inputStream.available(), -1)
                    .contentType(file.getContentType())
                    .build());
            return filename;
        } catch (IOException | InternalException
                 | XmlParserException | InvalidResponseException
                 | InvalidKeyException | NoSuchAlgorithmException
                 | ErrorResponseException | InsufficientDataException
                 | ServerException e) {
            log.error("%s: %s".formatted(MimosaExceptionMessage.FAILED_TO_UPLOAD_FILE.getMessage(), e.getMessage()), e);
            throw new OperationFailedException(MimosaExceptionMessage.FAILED_TO_UPLOAD_FILE.getMessage(), e);
        }
    }
}
