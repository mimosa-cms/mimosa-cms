package ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.PostDetails;

import java.util.List;
import java.util.Map;

@Entity
@DiscriminatorValue("BLOCK_BASED")
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class BlockBasedPostDetails extends PostDetails {
    @Column(name = "content", nullable = false, columnDefinition = "jsonb")
    @JdbcTypeCode(SqlTypes.JSON)
    private Content content;

    @Data
    @Accessors(chain = true)
    public static class Content {
        private long time;
        private List<Block> blocks;
    }

    @Data
    @Accessors(chain = true)
    public static class Block {
        private String type;
        private Map<String, Object> data;
    }
}
