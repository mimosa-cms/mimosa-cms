package ua.com.webbrains.mimosa.cms.commons.exceptions;

import org.springframework.http.HttpStatus;

public class BadRequest400Exception extends MimosaBaseException {
    public BadRequest400Exception(String message, Exception cause) {
        super(message, cause, HttpStatus.BAD_REQUEST);
    }

    public BadRequest400Exception(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }
}
