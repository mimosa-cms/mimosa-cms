package ua.com.webbrains.mimosa.cms.models.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ua.com.webbrains.mimosa.cms.generated.dto.PostDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostSingleDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl.BlockBasedPostDetails;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl.SimplePostDetails;

@Mapper(componentModel = "spring")
public interface PostMapper {
    @Mapping(source = "post.id", target = "id")
    @Mapping(source = "postDetails.metaWords", target = "metaWords")
    @Mapping(source = "postDetails.metaDescription", target = "metaDescription")
    @Mapping(source = "postDetails.metaAuthors", target = "metaAuthors")
    @Mapping(source = "post.title", target = "title")
    @Mapping(source = "post.description", target = "description")
    @Mapping(source = "post.authors", target = "authors")
    @Mapping(source = "post.postStatus", target = "postStatus")
    @Mapping(source = "post.createdAt", target = "createdAt")
    @Mapping(source = "post.updatedAt", target = "updatedAt")
    @Mapping(source = "post.likes", target = "likes")
    @Mapping(source = "post.views", target = "views")
    @Mapping(source = "postDetails.content", target = "content")
    @Mapping(target = "links", ignore = true)
    @Mapping(target = "text", ignore = true)
    PostSingleDto toPostSingleDto(Post post, BlockBasedPostDetails postDetails);

    @Mapping(source = "post.id", target = "id")
    @Mapping(source = "postDetails.metaWords", target = "metaWords")
    @Mapping(source = "postDetails.metaDescription", target = "metaDescription")
    @Mapping(source = "postDetails.metaAuthors", target = "metaAuthors")
    @Mapping(source = "postDetails.text", target = "text")
    @Mapping(source = "post.title", target = "title")
    @Mapping(source = "post.description", target = "description")
    @Mapping(source = "post.authors", target = "authors")
    @Mapping(source = "post.postStatus", target = "postStatus")
    @Mapping(source = "post.createdAt", target = "createdAt")
    @Mapping(source = "post.updatedAt", target = "updatedAt")
    @Mapping(source = "post.likes", target = "likes")
    @Mapping(source = "post.views", target = "views")
    @Mapping(source = "postDetails.links", target = "links")
    @Mapping(target = "content", ignore = true)
    PostSingleDto toPostSingleDto(Post post, SimplePostDetails postDetails);

    PostDto fromEntityToPostDto(Post post);
}
