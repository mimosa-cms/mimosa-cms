package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.ProfilesApiDelegate;
import ua.com.webbrains.mimosa.cms.generated.dto.DeleteProfileRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateUserInformationRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UserInformationResponseDto;
import ua.com.webbrains.mimosa.cms.services.UserService;

@Component
@RequiredArgsConstructor
public class ProfilesApiDelegateImpl implements ProfilesApiDelegate {
    private final UserService userService;

    @Override
    public ResponseEntity<UserInformationResponseDto> seeOwnBasicProfile() {
        return ResponseEntity.ok(userService.findAuthenticatedUserInfo());
    }

    @Override
    public ResponseEntity<UserInformationResponseDto> updateOwnProfile(UpdateUserInformationRequestDto updateUserInformationRequestDto) {
        return ResponseEntity.ok(userService.updateUserInformation(updateUserInformationRequestDto));
    }

    @Override
    public ResponseEntity<Void> deleteOwnProfile(DeleteProfileRequestDto deleteProfileRequestDto) {
        userService.deleteCurrentUserProfile(deleteProfileRequestDto);
        return ResponseEntity.noContent().build();
    }
}
