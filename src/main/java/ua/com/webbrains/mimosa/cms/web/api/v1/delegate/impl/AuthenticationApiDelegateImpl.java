package ua.com.webbrains.mimosa.cms.web.api.v1.delegate.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.generated.api.AuthenticationApiDelegate;
import ua.com.webbrains.mimosa.cms.generated.dto.LoginRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LoginResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PasswordResetRequestDto;
import ua.com.webbrains.mimosa.cms.services.AuthService;

@Component
@RequiredArgsConstructor
public class AuthenticationApiDelegateImpl implements AuthenticationApiDelegate {
    private final AuthService authService;

    @Override
    public ResponseEntity<LoginResponseDto> userLogin(LoginRequestDto loginRequestDto, String verificationCode) {
        return ResponseEntity.ok(authService.login(loginRequestDto, verificationCode));
    }

    @Override
    public ResponseEntity<LoginResponseDto> userLoginThroughRefreshToken(String refreshToken) {
        return ResponseEntity.ok(authService.refreshToken(refreshToken));
    }

    @Override
    public ResponseEntity<Void> passwordReset(PasswordResetRequestDto passwordResetRequestDto) {
        authService.recoverPassword(passwordResetRequestDto);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> passwordResetSend(String emailOrUsername) {
        authService.sendPasswordRecoveryEmail(emailOrUsername);
        return ResponseEntity.ok().build();
    }
}
