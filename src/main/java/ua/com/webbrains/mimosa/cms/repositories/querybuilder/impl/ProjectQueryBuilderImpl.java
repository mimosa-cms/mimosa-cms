package ua.com.webbrains.mimosa.cms.repositories.querybuilder.impl;

import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.Direction;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.ProjectFilterParameter;
import ua.com.webbrains.mimosa.cms.models.parameters.impl.ProjectSortParameter;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.repositories.querybuilder.ProjectQueryBuilder;

import java.util.Map;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class ProjectQueryBuilderImpl implements ProjectQueryBuilder {
    private static final String USER_FIELD = "owner";
    private static final String COWORKERS_FIELD = "coworkers";
    private static final String USER_ID_FIELD = "id";


    @Override
    public Specification<Project> buildSearchProcessor(UUID userId, Map<ProjectFilterParameter, String> filters) {
        Specification<Project> specification = Specification.where((root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get(USER_FIELD).get(USER_ID_FIELD), userId));
        specification = specification.or((root, query, criteriaBuilder) -> {
            Join<Project, User> coworkersJoin = root.join(COWORKERS_FIELD, JoinType.LEFT);
            return criteriaBuilder.equal(coworkersJoin.get(USER_ID_FIELD), userId);
        });


        for (Map.Entry<ProjectFilterParameter, String> entry : filters.entrySet()) {
            ProjectFilterParameter filterParameter = entry.getKey();
            String value = entry.getValue();

            switch (filterParameter) {
                case ProjectFilterParameter.LINK:
                    specification = specification.and((root, query, criteriaBuilder) ->
                            criteriaBuilder.equal(root.get(filterParameter.getFieldName()), value));
                    break;
                case ProjectFilterParameter.NAME:
                    specification = specification.and((root, query, criteriaBuilder) ->
                            criteriaBuilder.like(criteriaBuilder.lower(root.get(filterParameter.getFieldName())), "%" + value.toLowerCase() + "%"));
                    break;
                default:
                    break;
            }
        }

        return specification;
    }

    @Override
    public Sort buildSortProcessor(Map<ProjectSortParameter, String> sortingParameters) {
        return Sort.by(sortingParameters.entrySet().stream().map(element -> switch (element.getValue()) {
            case Direction.ASC -> Sort.Order.asc(element.getKey().getFieldName());
            case Direction.DESC -> Sort.Order.desc(element.getKey().getFieldName());
            default -> throw new DataNotValidException(MimosaExceptionMessage.SORTING_PARAMETER_NOT_VALID.getMessage());
        }).toList());
    }
}
