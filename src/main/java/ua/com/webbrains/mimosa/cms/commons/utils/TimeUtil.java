package ua.com.webbrains.mimosa.cms.commons.utils;

import lombok.experimental.UtilityClass;

import java.time.OffsetDateTime;
import java.time.ZoneId;

@UtilityClass
public class TimeUtil {
    public static final String TIMEZONE = "Europe/Kiev";

    public OffsetDateTime now() {
        return toMimosaTimezone(OffsetDateTime.now());
    }

    public OffsetDateTime toMimosaTimezone(OffsetDateTime time) {
        return time.atZoneSameInstant(ZoneId.of(TIMEZONE)).toOffsetDateTime();
    }
}
