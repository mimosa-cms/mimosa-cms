package ua.com.webbrains.mimosa.cms.models.parameters.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.models.parameters.FilterParameter;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum ProjectFilterParameter implements FilterParameter {
    LINK("link", "link"),
    NAME("name", "name");

    private final String value;
    private final String fieldName;

    public static ProjectFilterParameter fromString(String value) {
        return Arrays.stream(values())
                .filter((parameter -> value.equalsIgnoreCase(parameter.getValue())))
                .findFirst()
                .orElseThrow(() -> new DataNotFoundException(String.format(MimosaExceptionMessage.FILTER_VALUE_NOT_FOUND.getMessage())));
    }
}
