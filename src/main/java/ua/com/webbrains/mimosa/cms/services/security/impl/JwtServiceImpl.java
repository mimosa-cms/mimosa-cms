package ua.com.webbrains.mimosa.cms.services.security.impl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.com.webbrains.mimosa.cms.commons.enums.MimosaExceptionMessage;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Unauthorized401Exception;
import ua.com.webbrains.mimosa.cms.models.security.JwtUserDetails;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;
import ua.com.webbrains.mimosa.cms.services.security.JwtService;

import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.ACCESS_TOKEN_EXPIRATION;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.ACCESS_TOKEN_PREFIX;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.REFRESH_TOKEN_EXPIRATION;

@Service
@RequiredArgsConstructor
@Slf4j
public class JwtServiceImpl implements JwtService {
    private final AuthoritiesService authoritiesService;

    @Value("${application.secretKey}")
    private String secretKey;

    @Override
    public String createAccessToken(String userId, List<String> permissions) {
        Date expiration = new Date(new Date().getTime() + ACCESS_TOKEN_EXPIRATION);
        return Jwts.builder()
                .claims()
                .subject(userId)
                .add("permissions", permissions)
                .and()
                .expiration(expiration)
                .signWith(Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8)))
                .compact();
    }

    @Override
    public String createRefreshToken(String userId) {
        Date expiration = new Date(new Date().getTime() + REFRESH_TOKEN_EXPIRATION);
        return Jwts.builder()
                .claims()
                .subject(userId)
                .and()
                .expiration(expiration)
                .signWith(Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8)))
                .compact();
    }

    @Override
    public boolean validateAccessToken(String fullAccessToken) {
        return isAccessTokenValid(fullAccessToken);
    }

    @Override
    public String createPostManagementApiKey(UUID projectId, OffsetDateTime offsetDateTime) {
        return Jwts.builder()
                .claims()
                .subject(projectId.toString())
                .and()
                .expiration(Date.from(offsetDateTime.toInstant()))
                .signWith(Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8)))
                .compact();
    }

    private boolean isAccessTokenValid(String fullAccessToken) {
        if (fullAccessToken == null || !fullAccessToken.startsWith(ACCESS_TOKEN_PREFIX)) {
            return false;
        }
        try {
            String token = fullAccessToken.substring(ACCESS_TOKEN_PREFIX.length());
            return isTokenExpired(token);
        } catch (JwtException e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean validateRefreshToken(String refreshToken) {
        return isRefreshTokenValid(refreshToken);
    }

    private boolean isRefreshTokenValid(String refreshToken) {
        try {
            return isTokenExpired(refreshToken);
        } catch (JwtException e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public JwtUserDetails parseAccessToken(String fullAccessToken) {
        if (!isAccessTokenValid(fullAccessToken)) {
            throw new Unauthorized401Exception(MimosaExceptionMessage.TOKEN_IS_NOT_VALID.getMessage());
        }
        String token = fullAccessToken.substring(ACCESS_TOKEN_PREFIX.length());
        String userId = getClaimFromToken(token, Claims::getSubject);
        List<String> permissions = getClaimFromToken(token, claims -> {
            try {
                return claims.get("permissions", List.class);
            } catch (ClassCastException | NullPointerException e) {
                throw new Unauthorized401Exception(MimosaExceptionMessage.TOKEN_IS_NOT_VALID.getMessage());
            }
        });
        return new JwtUserDetails(userId, authoritiesService.fetchAuthoritiesFromPermissions(permissions));
    }

    @Override
    public String parseRefreshToken(String refreshToken) {
        if (!isRefreshTokenValid(refreshToken)) {
            throw new Unauthorized401Exception(MimosaExceptionMessage.TOKEN_IS_NOT_VALID.getMessage());
        }
        return getClaimFromToken(refreshToken, Claims::getSubject);
    }

    private <T> T getClaimFromToken(String token, Function<Claims, T> claimResolver) {
        Claims claims = getAllClaimsFromToken(token);
        return claimResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().verifyWith(Keys.hmacShaKeyFor(secretKey.getBytes(StandardCharsets.UTF_8))).build().parseSignedClaims(token).getPayload();
    }

    private Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    private boolean isTokenExpired(String token) {
        Date expirationDate = getExpirationDateFromToken(token);
        return !expirationDate.before(new Date());
    }
}
