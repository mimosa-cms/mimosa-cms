package ua.com.webbrains.mimosa.cms.configs.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "application.cache")
public class CacheProperties {
    private Integer maximumSize;
    private Integer expirationMinutes;
    private String[] cacheNames;
}
