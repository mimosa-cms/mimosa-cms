package ua.com.webbrains.mimosa.cms.services;

import org.springframework.security.access.prepost.PreAuthorize;
import ua.com.webbrains.mimosa.cms.generated.dto.BlockBasedPostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LinkCategoryDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostSingleDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostsResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;

import java.util.List;
import java.util.UUID;

/**
 * Service for managing actions with posts
 *
 * @author yegorchevardin
 * @version 0.0.1
 */
public interface PostsService {
    @PreAuthorize("hasRole('ROLE_CREATE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostDto createPostForProject(UUID projectId, PostRequestDto creationDto);

    @PreAuthorize("hasRole('ROLE_CREATE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostDto createPostForProject(UUID projectId, BlockBasedPostRequestDto blockBasedPostRequestDto);

    @PreAuthorize("hasRole('ROLE_UPDATE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostDto updatePostForProject(UUID projectId, UUID postId, BlockBasedPostRequestDto updatingDto);

    @PreAuthorize("hasRole('ROLE_UPDATE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostDto updatePostForProject(UUID projectId, UUID postId, PostRequestDto updatingDto);

    @PreAuthorize("hasRole('ROLE_DELETE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    void deletePostForProject(UUID projectId, UUID postId);

    @PreAuthorize("hasRole('ROLE_READ_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostsResponseDto findPostsForProject(UUID projectId, Integer size, Integer page, String filter, String sort);

    @PreAuthorize("hasRole('ROLE_READ_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostSingleDto findPostFromProject(UUID projectId, UUID postId);

    @PreAuthorize("hasRole('ROLE_READ_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    Post findPostEntityFromProject(UUID projectId, UUID postId);

    @PreAuthorize("hasRole('ROLE_UPDATE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostDto updatePostViews(UUID projectId, UUID postId, Integer views);

    @PreAuthorize("hasRole('ROLE_UPDATE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostDto updatePostLikes(UUID projectId, UUID postId, Integer likes);

    @PreAuthorize("hasRole('ROLE_UPDATE_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    PostDto updatePostStatus(UUID projectId, UUID postId, String status);

    @PreAuthorize("hasRole('ROLE_READ_POSTS') and hasRole('ROLE_EMAIL_APPROVED')")
    List<LinkCategoryDto> retrieveLinkCategories();
}
