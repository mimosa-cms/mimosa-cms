package ua.com.webbrains.mimosa.cms.commons.exceptions;

import org.springframework.http.HttpStatus;

public class Conflict409Exception extends MimosaBaseException {
    public Conflict409Exception(String message, Exception cause) {
        super(message, cause, HttpStatus.CONFLICT);
    }

    public Conflict409Exception(String message) {
        super(message, HttpStatus.CONFLICT);
    }
}
