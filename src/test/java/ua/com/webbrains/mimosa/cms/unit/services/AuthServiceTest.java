package ua.com.webbrains.mimosa.cms.unit.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import ua.com.webbrains.mimosa.cms.commons.exceptions.BadRequest400Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.TooManyRequests429Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.CredentialsNotValidException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.LoginRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LoginResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PasswordResetRequestDto;
import ua.com.webbrains.mimosa.cms.repositories.PasswordResetRepository;
import ua.com.webbrains.mimosa.cms.repositories.UserRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.PasswordReset;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.EmailService;
import ua.com.webbrains.mimosa.cms.services.impl.AuthServiceImpl;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;
import ua.com.webbrains.mimosa.cms.services.security.JwtService;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private JwtService jwtService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private AuthoritiesService authoritiesService;
    @Mock
    private EmailService emailService;
    @Mock
    private TemplateEngine templateEngine;
    @Mock
    private PasswordResetRepository passwordResetRepository;

    @Captor
    private ArgumentCaptor<PasswordReset> passwordResetArgumentCaptor;

    @InjectMocks
    private AuthServiceImpl authService;

    private User user;
    private final String userEmail = "user@example.com";
    private final String userPassword = "password";
    private final String encodedPassword = "encodedPassword";
    private final UUID userId = UUID.randomUUID();

    private static Stream<Arguments> provideEmailApprovals() {
        return Stream.of(
                Arguments.of(true),
                Arguments.of(false)
        );
    }

    @BeforeEach
    void setUp() {
        user = new User();
        user.setId(userId);
        user.setEmail(userEmail);
        user.setPassword(encodedPassword);
        user.setIsEmailApproved(false);
        user.setIs2FaEnabled(false);
    }

    @ParameterizedTest
    @MethodSource("provideEmailApprovals")
    void loginWithValidCredentialsReturnsLoginResponse(Boolean isEmailApproved) {
        User user = new User();
        user.setId(userId);
        user.setEmail(userEmail);
        user.setPassword(encodedPassword);
        user.setIsEmailApproved(isEmailApproved);
        user.setIs2FaEnabled(false);

        when(userRepository.findByUsernameOrEmail(anyString(), anyString())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(jwtService.createAccessToken(anyString(), any())).thenReturn("accessToken");
        when(jwtService.createRefreshToken(anyString())).thenReturn("refreshToken");

        LoginRequestDto loginRequest = new LoginRequestDto(userEmail, userPassword);
        LoginResponseDto response = authService.login(loginRequest, "");

        assertNotNull(response.accessToken());
        assertNotNull(response.refreshToken());
    }

    @Test
    void loginWithInvalidCredentialsThrowsException() {
        when(userRepository.findByUsernameOrEmail(anyString(), anyString())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);

        LoginRequestDto loginRequest = new LoginRequestDto(userEmail, userPassword);

        assertThrows(CredentialsNotValidException.class, () -> authService.login(loginRequest, ""));
    }

    @Test
    void loginWithUserNotFound() {
        when(userRepository.findByUsernameOrEmail(anyString(), anyString())).thenReturn(Optional.empty());

        LoginRequestDto loginRequest = new LoginRequestDto(userEmail, userPassword);

        assertThrows(DataNotFoundException.class, () -> authService.login(loginRequest, null));
    }

    @ParameterizedTest
    @MethodSource("provideEmailApprovals")
    void refreshTokenWithValidTokenReturnsNewAccessToken(Boolean isEmailApproved) {
        User user = new User();
        user.setId(userId);
        user.setEmail(userEmail);
        user.setPassword(encodedPassword);
        user.setIsEmailApproved(isEmailApproved);

        when(jwtService.parseRefreshToken(anyString())).thenReturn(userId.toString());
        when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));
        when(jwtService.createAccessToken(anyString(), any())).thenReturn("newAccessToken");

        LoginResponseDto response = authService.refreshToken("validRefreshToken");

        assertNotNull(response.accessToken());
        assertEquals("validRefreshToken", response.refreshToken());
    }

    @Test
    void refreshTokenWithUserNotFoundThrowsException() {
        when(jwtService.parseRefreshToken(anyString())).thenReturn(userId.toString());
        when(userRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> authService.refreshToken("validRefreshToken"));
    }

    @Test
    void sendPasswordRecoveryEmailWithValidEmailSendsEmail() {
        String emailTemplate = "emailTemplate";
        when(templateEngine.process(any(String.class), any(Context.class))).thenReturn(emailTemplate);
        when(userRepository.findByUsernameOrEmail(anyString(), anyString())).thenReturn(Optional.of(user));
        doNothing().when(emailService).sendEmail(anyString(), anyString(), anyString());

        assertDoesNotThrow(() -> authService.sendPasswordRecoveryEmail(userEmail));
        verify(emailService, times(1)).sendEmail(anyString(), anyString(), anyString());
    }

    @Test
    void sendPasswordRecoveryEmailWhenPasswordResetExistsUpdatesTokenAndSendsEmail() {
        String email = "user@example.com";
        String emailTemplate = "emailTemplate";
        when(templateEngine.process(any(String.class), any(Context.class))).thenReturn(emailTemplate);
        UUID existingToken = UUID.randomUUID();
        Timestamp expirationTimestamp = Timestamp.valueOf(LocalDateTime.now().plusDays(1));
        Timestamp sendAtTimestamp = Timestamp.valueOf(LocalDateTime.now().minusMinutes(5));
        PasswordReset existingPasswordReset = PasswordReset.builder()
                .email(email)
                .verificationToken(existingToken)
                .expirationDate(expirationTimestamp)
                .sendAt(sendAtTimestamp)
                .build();

        when(userRepository.findByUsernameOrEmail(email, email)).thenReturn(Optional.of(user));
        when(passwordResetRepository.findByEmail(email)).thenReturn(Optional.of(existingPasswordReset));
        doNothing().when(emailService).sendEmail(anyString(), anyString(), anyString());

        authService.sendPasswordRecoveryEmail(email);

        verify(passwordResetRepository, times(1)).save(passwordResetArgumentCaptor.capture());
        PasswordReset capturedPasswordReset = passwordResetArgumentCaptor.getValue();
        assertNotEquals(existingToken, capturedPasswordReset.getVerificationToken());
        assertNotNull(capturedPasswordReset.getSendAt());

        verify(emailService, times(1)).sendEmail(eq(email), anyString(), anyString());
    }

    @Test
    void sendPasswordRecoveryEmailTooSoonAfterPreviousRequestThrowsTooManyRequestsException() {
        String email = "user@example.com";
        UUID existingToken = UUID.randomUUID();
        Timestamp expirationTimestamp = Timestamp.valueOf(LocalDateTime.now().plusDays(1));
        Timestamp sendAtTimestamp = Timestamp.valueOf(LocalDateTime.now());
        PasswordReset existingPasswordReset = PasswordReset.builder()
                .email(email)
                .verificationToken(existingToken)
                .expirationDate(expirationTimestamp)
                .sendAt(sendAtTimestamp)
                .build();

        when(userRepository.findByUsernameOrEmail(email, email)).thenReturn(Optional.of(user));
        when(passwordResetRepository.findByEmail(email)).thenReturn(Optional.of(existingPasswordReset));

        assertThrows(TooManyRequests429Exception.class, () -> authService.sendPasswordRecoveryEmail(email));
    }

    @Test
    void sendPasswordRecoveryEmailWithExpiredTokenUpdatesTokenAndSendsEmail() {
        String email = "user@example.com";
        UUID existingToken = UUID.randomUUID();
        Timestamp expirationTimestamp = Timestamp.valueOf(LocalDateTime.now().minusDays(1));
        Timestamp sendAtTimestamp = Timestamp.valueOf(LocalDateTime.now().minusMinutes(5));
        PasswordReset existingPasswordReset = PasswordReset.builder()
                .email(email)
                .verificationToken(existingToken)
                .expirationDate(expirationTimestamp)
                .sendAt(sendAtTimestamp)
                .build();
        String emailTemplate = "emailTemplate";
        when(templateEngine.process(any(String.class), any(Context.class))).thenReturn(emailTemplate);

        when(userRepository.findByUsernameOrEmail(email, email)).thenReturn(Optional.of(user));
        when(passwordResetRepository.findByEmail(email)).thenReturn(Optional.of(existingPasswordReset));
        doNothing().when(emailService).sendEmail(anyString(), anyString(), anyString());

        authService.sendPasswordRecoveryEmail(email);

        verify(passwordResetRepository, times(1)).save(passwordResetArgumentCaptor.capture());
        PasswordReset capturedPasswordReset = passwordResetArgumentCaptor.getValue();
        assertEquals(existingToken, capturedPasswordReset.getVerificationToken(), "Verification token should be updated for expired tokens");
        assertNotNull(capturedPasswordReset.getSendAt());

        verify(emailService, times(1)).sendEmail(eq(email), anyString(), anyString());
    }

    @Test
    void recoverPasswordWithValidTokenUpdatesPassword() {
        UUID token = UUID.randomUUID();
        PasswordReset passwordReset = new PasswordReset();
        passwordReset.setEmail(userEmail);
        passwordReset.setVerificationToken(token);
        passwordReset.setExpirationDate(Timestamp.valueOf(LocalDateTime.now().plusDays(1)));
        when(passwordResetRepository.findByEmail(anyString())).thenReturn(Optional.of(passwordReset));
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));
        when(passwordEncoder.encode(anyString())).thenReturn("newEncodedPassword");

        PasswordResetRequestDto requestDto = new PasswordResetRequestDto(token, userEmail, "newPassword", "newPassword");

        assertDoesNotThrow(() -> authService.recoverPassword(requestDto));
        verify(passwordResetRepository, times(1)).delete(any(PasswordReset.class));
    }

    @Test
    void recoverPasswordWithMismatchedPasswordsThrowsDataNotValidException() {
        UUID token = UUID.randomUUID();
        PasswordResetRequestDto requestDto = new PasswordResetRequestDto(token, userEmail, "newPassword", "differentNewPassword");

        assertThrows(DataNotValidException.class, () -> authService.recoverPassword(requestDto));
    }

    @Test
    void recoverPasswordWithSameAsOldPasswordThrowsDataNotValidException() {
        UUID token = UUID.randomUUID();
        PasswordReset passwordReset = new PasswordReset();
        passwordReset.setEmail(userEmail);
        passwordReset.setVerificationToken(token);
        passwordReset.setExpirationDate(Timestamp.valueOf(LocalDateTime.now().plusDays(1)));
        when(passwordResetRepository.findByEmail(anyString())).thenReturn(Optional.of(passwordReset));
        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);

        PasswordResetRequestDto requestDto = new PasswordResetRequestDto(token, userEmail, userPassword, userPassword);

        assertThrows(DataNotValidException.class, () -> authService.recoverPassword(requestDto));
    }

    @Test
    void recoverPasswordWithInvalidTokenThrowsDataNotValidException() {
        PasswordReset passwordReset = new PasswordReset();
        passwordReset.setEmail(userEmail);
        UUID differentToken = UUID.randomUUID();
        passwordReset.setVerificationToken(differentToken);
        passwordReset.setExpirationDate(Timestamp.valueOf(LocalDateTime.now().plusDays(1)));
        when(passwordResetRepository.findByEmail(anyString())).thenReturn(Optional.of(passwordReset));

        UUID token = UUID.randomUUID();
        PasswordResetRequestDto requestDto = new PasswordResetRequestDto(token, userEmail, "newPassword", "newPassword");

        assertThrows(DataNotValidException.class, () -> authService.recoverPassword(requestDto));
    }

    @Test
    void recoverPasswordWithNonExistentEmailThrowsDataNotFoundException() {
        UUID token = UUID.randomUUID();
        PasswordResetRequestDto requestDto = new PasswordResetRequestDto(token, "nonexistent@example.com", "newPassword", "newPassword");

        when(passwordResetRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> authService.recoverPassword(requestDto));
    }

    @Test
    void recoverPasswordWithExpiredTokenThrowsBadRequestException() {
        PasswordReset passwordReset = new PasswordReset();
        passwordReset.setEmail(userEmail);
        passwordReset.setExpirationDate(Timestamp.valueOf(LocalDateTime.now().minusDays(1)));
        when(passwordResetRepository.findByEmail(anyString())).thenReturn(Optional.of(passwordReset));

        UUID token = UUID.randomUUID();
        PasswordResetRequestDto requestDto = new PasswordResetRequestDto(token, userEmail, "newPassword", "newPassword");

        assertThrows(BadRequest400Exception.class, () -> authService.recoverPassword(requestDto));
    }

    @Test
    void sendPasswordRecoveryEmailWithNonExistentUserThrowsDataNotFoundException() {
        String nonExistentEmail = "nonexistent@example.com";

        when(userRepository.findByUsernameOrEmail(anyString(), anyString())).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> authService.sendPasswordRecoveryEmail(nonExistentEmail));
    }
}
