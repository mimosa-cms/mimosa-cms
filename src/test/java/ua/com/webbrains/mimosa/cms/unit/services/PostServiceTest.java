package ua.com.webbrains.mimosa.cms.unit.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.cache.CacheManager;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.BlockBasedPostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.BlockDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ContentDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LinkDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostRequestDto;
import ua.com.webbrains.mimosa.cms.models.PostType;
import ua.com.webbrains.mimosa.cms.models.converters.PostSingleConverter;
import ua.com.webbrains.mimosa.cms.models.mappers.PostMapper;
import ua.com.webbrains.mimosa.cms.repositories.ImageRepository;
import ua.com.webbrains.mimosa.cms.repositories.PostDetailsRepository;
import ua.com.webbrains.mimosa.cms.repositories.PostRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.PostDetails;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl.BlockBasedPostDetails;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.details.impl.SimplePostDetails;
import ua.com.webbrains.mimosa.cms.repositories.querybuilder.PostQueryBuilder;
import ua.com.webbrains.mimosa.cms.repositories.querybuilder.impl.PostQueryBuilderImpl;
import ua.com.webbrains.mimosa.cms.services.ProjectService;
import ua.com.webbrains.mimosa.cms.services.UserService;
import ua.com.webbrains.mimosa.cms.services.impl.PostServiceImpl;
import ua.com.webbrains.mimosa.cms.services.validation.SubscriptionValidator;
import ua.com.webbrains.mimosa.cms.services.validation.impl.SubscriptionValidatorImpl;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_FREE_POSTS_PER_PROJECT_AMOUNT;

@ExtendWith(MockitoExtension.class)
public class PostServiceTest {
    @Mock
    private UserService userService;
    @Mock
    private PostDetailsRepository postDetailsRepository;
    @Mock
    private PostRepository postRepository;
    @Mock
    private ProjectService projectService;
    @Mock
    private ImageRepository imageRepository;
    @Mock
    private CacheManager cacheManager;

    @Spy
    private PostQueryBuilder postQueryBuilder = new PostQueryBuilderImpl();
    @Spy
    private PostMapper postMapper = Mappers.getMapper(PostMapper.class);
    @Spy
    private PostSingleConverter postSingleConverter = new PostSingleConverter(postMapper);
    @Spy
    private SubscriptionValidator subscriptionValidator = new SubscriptionValidatorImpl();

    @InjectMocks
    private PostServiceImpl postService;

    private Project project;
    private User user;
    private UUID projectId;
    private UUID userId;

    @BeforeEach
    void setUp() {
        postMapper = Mockito.spy(Mappers.getMapper(PostMapper.class));
        postSingleConverter = new PostSingleConverter(postMapper);

        userId = UUID.randomUUID();
        user = User.builder()
                .id(userId)
                .email("test@test.com")
                .name("Test")
                .surname("Test")
                .username("Test.test")
                .isEmailApproved(false)
                .isPremium(false)
                .build();

        projectId = UUID.randomUUID();
        project = Project.builder()
                .id(projectId)
                .name("ProjectName")
                .link("https://projectLink.com")
                .owner(user)
                .build();
    }

    @Test
    void createPostForProject_withValidData_createsPostSuccessfully() {
        UUID projectId = UUID.randomUUID();
        PostRequestDto creationDto = createPostRequestDto();
        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);
        Mockito.when(postRepository.countAllByProjectId(Mockito.any(UUID.class)))
                .thenReturn(0);
        Mockito.when(postRepository.save(Mockito.any(Post.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        Mockito.when(postDetailsRepository.save(Mockito.any(PostDetails.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));

        PostDto result = postService.createPostForProject(projectId, creationDto);

        assertNotNull(result);
        Mockito.verify(postRepository).save(Mockito.any(Post.class));
    }

    @Test
    void createPostForProject_whenExceedingFreePostsLimit_throwsForbidden403Exception() {
        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);
        Mockito.when(postRepository.countAllByProjectId(Mockito.any(UUID.class)))
                .thenReturn(MAX_FREE_POSTS_PER_PROJECT_AMOUNT + 1);

        UUID projectId = UUID.randomUUID();
        PostRequestDto creationDto = createPostRequestDto();
        assertThrows(Forbidden403Exception.class, () -> postService.createPostForProject(projectId, creationDto));
    }

    @Test
    void createPostForProject_withInvalidAuthors_throwsDataNotValidException() {
        UUID projectId = UUID.randomUUID();
        PostRequestDto creationDto = createPostRequestDtoWithInvalidAuthors();

        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);

        assertThrows(DataNotValidException.class, () -> postService.createPostForProject(projectId, creationDto));
    }

    @Test
    void createPostForProject_withInvalidLinks_throwsDataNotValidException() {
        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);

        UUID projectId = UUID.randomUUID();
        PostRequestDto creationDto = createPostRequestDtoWithInvalidLinks();
        assertThrows(DataNotValidException.class, () -> postService.createPostForProject(projectId, creationDto));
    }

    @Test
    void createPostForProject_withBlockBasedPost_createsPostSuccessfully() {
        UUID projectId = UUID.randomUUID();
        BlockBasedPostRequestDto creationDto = createBlockBasedPostRequestDto();
        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);
        Mockito.when(postRepository.countAllByProjectId(Mockito.any(UUID.class)))
                .thenReturn(0);
        Mockito.when(postRepository.save(Mockito.any(Post.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        Mockito.when(postDetailsRepository.save(Mockito.any(PostDetails.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));

        PostDto result = postService.createPostForProject(projectId, creationDto);

        assertNotNull(result);
        Mockito.verify(postRepository).save(Mockito.any(Post.class));
    }

    @Test
    void updatePostForProject_withValidData_updatesPostSuccessfully() {
        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);
        Post post = new Post();
        post.setPostDetails(new SimplePostDetails());
        Mockito.when(postRepository.findPostByIdAndProjectIdAndPostType(Mockito.any(UUID.class), Mockito.any(UUID.class), Mockito.any(PostType.class)))
                .thenReturn(Optional.of(post));
        Mockito.when(postRepository.save(Mockito.any(Post.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        Mockito.when(postDetailsRepository.save(Mockito.any(PostDetails.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));

        UUID projectId = UUID.randomUUID();
        UUID postId = UUID.randomUUID();
        PostRequestDto updatingDto = createPostRequestDto();
        PostDto result = postService.updatePostForProject(projectId, postId, updatingDto);

        assertNotNull(result);
        Mockito.verify(postRepository).save(Mockito.any(Post.class));
    }

    @Test
    void updatePostForProject_withBlockBasedPost_updatesPostSuccessfully() {
        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);
        Post post = new Post();
        post.setPostDetails(new BlockBasedPostDetails());
        Mockito.when(postRepository.findPostByIdAndProjectIdAndPostType(Mockito.any(UUID.class), Mockito.any(UUID.class), Mockito.any(PostType.class)))
                .thenReturn(Optional.of(post));
        Mockito.when(postRepository.save(Mockito.any(Post.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));
        Mockito.when(postDetailsRepository.save(Mockito.any(PostDetails.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));

        UUID projectId = UUID.randomUUID();
        UUID postId = UUID.randomUUID();
        BlockBasedPostRequestDto updatingDto = createBlockBasedPostRequestDto();
        PostDto result = postService.updatePostForProject(projectId, postId, updatingDto);

        assertNotNull(result);
        Mockito.verify(postRepository).save(Mockito.any(Post.class));
    }

    @Test
    void updatePostForProject_whenPostNotFound_throwsDataNotFoundException() {
        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);
        Mockito.when(postRepository.findPostByIdAndProjectIdAndPostType(Mockito.any(UUID.class), Mockito.any(UUID.class), Mockito.any(PostType.class)))
                .thenReturn(Optional.empty());

        UUID projectId = UUID.randomUUID();
        UUID postId = UUID.randomUUID();
        BlockBasedPostRequestDto updatingDto = createBlockBasedPostRequestDto();
        assertThrows(DataNotFoundException.class, () -> postService.updatePostForProject(projectId, postId, updatingDto));
    }

    @Test
    void updatePostForProject_whenUserNotAuthorized_throwsForbidden403Exception() {
        Mockito.when(userService.findCurrentUser()).thenReturn(user);
        Mockito.when(projectService.findProjectByIdAndUserId(Mockito.any(UUID.class), Mockito.any(UUID.class)))
                .thenReturn(project);
        Post post = new Post();
        post.setPostDetails(new SimplePostDetails());
        Mockito.when(postRepository.findPostByIdAndProjectIdAndPostType(Mockito.any(UUID.class), Mockito.any(UUID.class), Mockito.any(PostType.class)))
                .thenReturn(Optional.of(post));

        UUID projectId = UUID.randomUUID();
        UUID postId = UUID.randomUUID();
        BlockBasedPostRequestDto updatingDto = createBlockBasedPostRequestDto();
        assertThrows(Forbidden403Exception.class, () -> postService.updatePostForProject(projectId, postId, updatingDto));
    }

    private BlockBasedPostRequestDto createBlockBasedPostRequestDto() {
        BlockDto paragraphBlock = new BlockDto("paragraph", Map.of("text", "Example paragraph text"));
        BlockDto listBlock = new BlockDto("list", Map.of("items", Arrays.asList("Item 1", "Item 2")));

        ContentDto contentDto = new ContentDto(System.currentTimeMillis(), Arrays.asList(paragraphBlock, listBlock));

        return BlockBasedPostRequestDto.builder()
                .category("Test Category")
                .tags(Arrays.asList("Tag1", "Tag2"))
                .metaWords("Test Meta Words")
                .metaDescription("Test Meta Description")
                .metaAuthors("Test Author")
                .title("Test Title")
                .description("Test Description")
                .authors("Test Authors")
                .postStatus("DRAFT")
                .content(contentDto)
                .build();
    }

    private PostRequestDto createPostRequestDto() {
        return PostRequestDto.builder()
                .category("Technology")
                .tags(Arrays.asList("Java", "Spring Boot"))
                .metaWords("java, spring boot, web development")
                .metaDescription("A comprehensive guide to building web applications with Java and Spring Boot.")
                .metaAuthors("Jane Doe, John Smith")
                .text("This is a detailed post about Java and Spring Boot development...")
                .title("Java and Spring Boot Web Development")
                .description("Learn how to develop web applications using Java and Spring Boot.")
                .authors("Jane Doe")
                .postStatus("PUBLISHED")
                .links(Arrays.asList(new LinkDto("https://github.com/example", "GitHub", "none"),
                        new LinkDto("https://www.linkedin.com/in/example", "LinkedIn", "none")))
                .build();
    }

    private PostRequestDto createPostRequestDtoWithInvalidAuthors() {
        return PostRequestDto.builder()
                .category("Technology")
                .tags(Arrays.asList("Java", "Spring Boot"))
                .metaWords("java, spring boot, web development")
                .metaDescription("A comprehensive guide to building web applications with Java and Spring Boot.")
                .metaAuthors("Jane Doe, John Smith")
                .text("This is a detailed post about Java and Spring Boot development...")
                .title("Java and Spring Boot Web Development")
                .description("Learn how to develop web applications using Java and Spring Boot.")
                .authors("\"DROP * FROM users\"")
                .postStatus("PUBLISHED")
                .links(Arrays.asList(new LinkDto("https://github.com/example", "GitHub", "none"),
                        new LinkDto("https://www.linkedin.com/in/example", "LinkedIn", "none")))
                .build();
    }

    private PostRequestDto createPostRequestDtoWithInvalidLinks() {
        return PostRequestDto.builder()
                .category("Technology")
                .tags(Arrays.asList("Java", "Spring Boot"))
                .metaWords("java, spring boot, web development")
                .metaDescription("A comprehensive guide to building web applications with Java and Spring Boot.")
                .metaAuthors("Jane Doe, John Smith")
                .text("This is a detailed post about Java and Spring Boot development...")
                .title("Java and Spring Boot Web Development")
                .description("Learn how to develop web applications using Java and Spring Boot.")
                .authors("Jane Doe")
                .postStatus("PUBLISHED")
                .links(Arrays.asList(new LinkDto("https://github.com/example", "GitHub", "jfsldfjdls"),
                        new LinkDto("https:/jljgldjglgj", "ghfdlgfdlg", "none")))
                .build();
    }
}
