package ua.com.webbrains.mimosa.cms.integration.config;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.junit.jupiter.Testcontainers;
import ua.com.webbrains.mimosa.cms.services.impl.EmailServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@MockBean(EmailServiceImpl.class)
@Testcontainers
@AutoConfigureMockMvc
@ComponentScan(basePackages = "ua.com.webbrains.mimosa.cms")
public abstract class ItConfig {

}
