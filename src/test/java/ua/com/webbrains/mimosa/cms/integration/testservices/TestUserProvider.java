package ua.com.webbrains.mimosa.cms.integration.testservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestComponent;
import ua.com.webbrains.mimosa.cms.models.security.RolesAndPermissions;
import ua.com.webbrains.mimosa.cms.repositories.UserRepository;
import ua.com.webbrains.mimosa.cms.services.security.JwtService;

import java.util.ArrayList;
import java.util.List;

@TestComponent("testUserProvider")
public class TestUserProvider {
    @Value("${application.admin.username}")
    private String username;

    @Autowired
    private JwtService jwtService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RolesAndPermissions rolesAndPermissions;

    public String getUserToken() {
        List<String> permissions = new ArrayList<>();
        permissions.addAll(rolesAndPermissions.admin());
        permissions.addAll(rolesAndPermissions.emailApproved());
        return jwtService.createAccessToken(
                userRepository.findByUsername(username).orElseThrow().getId().toString(),
                permissions
        );
    }
}
