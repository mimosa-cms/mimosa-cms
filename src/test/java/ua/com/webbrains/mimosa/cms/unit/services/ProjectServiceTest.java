package ua.com.webbrains.mimosa.cms.unit.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataExistException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectsResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.models.mappers.ProjectMapper;
import ua.com.webbrains.mimosa.cms.models.mappers.UserMapper;
import ua.com.webbrains.mimosa.cms.repositories.PostRepository;
import ua.com.webbrains.mimosa.cms.repositories.ProjectRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.repositories.querybuilder.ProjectQueryBuilder;
import ua.com.webbrains.mimosa.cms.repositories.querybuilder.impl.ProjectQueryBuilderImpl;
import ua.com.webbrains.mimosa.cms.services.UserService;
import ua.com.webbrains.mimosa.cms.services.impl.ProjectServiceImpl;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static ua.com.webbrains.mimosa.cms.commons.MimosaConstants.MAX_FREE_PROJECTS_AMOUNT;

@ExtendWith(MockitoExtension.class)
public class ProjectServiceTest {
    @Mock
    private ProjectRepository projectRepository;
    @Mock
    private UserService userService;
    @Mock
    private AuthoritiesService authoritiesService;
    @Mock
    private PostRepository postRepository;

    @Spy
    private ProjectMapper projectMapper = Mappers.getMapper(ProjectMapper.class);

    @Spy
    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @Spy
    private ProjectQueryBuilder projectQueryBuilder = new ProjectQueryBuilderImpl();

    @InjectMocks
    private ProjectServiceImpl projectService;

    private User testUser;
    private Project testProject;
    private final UUID testUserId = UUID.randomUUID();
    private final UUID testProjectId = UUID.randomUUID();

    @BeforeEach
    void setUp() {
        testUser = User.builder()
                .id(testUserId)
                .email("test@test.com")
                .name("Test")
                .surname("Test")
                .username("Test.test")
                .isEmailApproved(false)
                .isPremium(false)
                .build();

        projectMapper = Mockito.spy(Mappers.getMapper(ProjectMapper.class));
        userMapper = Mockito.spy(Mappers.getMapper(UserMapper.class));

        testProject = new Project();
        testProject.setId(testProjectId);
        testProject.setName("Test Project");
        testProject.setLink("test-link");
        testProject.setOwner(testUser);
    }

    @Test
    void createProjectExceedsFreeLimitThrowsForbidden403Exception() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        when(projectRepository.countAllByOwnerId(eq(testUserId))).thenReturn(MAX_FREE_PROJECTS_AMOUNT + 1);

        assertThrows(Forbidden403Exception.class, () -> projectService.createProject(CreateProjectRequestDto.builder()
                .name("New Project")
                .link("new-link")
                .build()));
    }

    @Test
    void createProjectSuccessfully() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        when(projectRepository.findByLinkAndOwnerId(anyString(), any())).thenReturn(Optional.empty());
        when(projectRepository.save(any(Project.class))).thenReturn(Project.builder().id(UUID.randomUUID()).build());

        assertDoesNotThrow(() -> projectService.createProject(new CreateProjectRequestDto("New Project", "unique-link")));
    }

    @Test
    void createProjectWithDuplicateLinkThrowsDataExistException() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        when(projectRepository.findByLinkAndOwnerId(eq("duplicate-link"), any())).thenReturn(Optional.of(new Project()));

        assertThrows(DataExistException.class, () -> projectService.createProject(new CreateProjectRequestDto("New Project", "duplicate-link")));
    }

    @Test
    void findAllUserProjectsAppliesFiltersAndSortsCorrectly() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        Page<Project> projectPage = new PageImpl<>(Collections.singletonList(testProject));
        when(projectRepository.findAll(any(Specification.class), any(PageRequest.class))).thenReturn(projectPage);

        ProjectsResponseDto response = projectService.findAllUserProjects(10, 0, "name", "name=desc");

        assertNotNull(response);
        assertFalse(response.projects().isEmpty());
        assertEquals("Test Project", response.projects().get(0).name());
    }

    @Test
    void findAllUserProjectsAppliesFiltersAndSortsIncorrectly() {
        when(userService.findCurrentUser()).thenReturn(testUser);

        assertThrows(DataNotValidException.class, () -> projectService.findAllUserProjects(10, 0, "name", "name=cool"));
    }

    @Test
    void findAllUserProjects() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        Page<Project> projectPage = new PageImpl<>(Collections.singletonList(testProject));
        when(projectRepository.findAllAllowed(eq(testUserId), any(PageRequest.class))).thenReturn(projectPage);

        ProjectsResponseDto response = projectService.findAllUserProjects(10, 0, "", "");

        assertNotNull(response);
        assertFalse(response.projects().isEmpty());
        assertEquals("Test Project", response.projects().get(0).name());
    }

    @Test
    void updateProjectNotFoundThrowsDataNotFoundException() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        when(projectRepository.findByAllowedById(eq(testUserId), any(UUID.class))).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> projectService.updateProject(testProjectId, new UpdateProjectRequestDto("Updated Project", "updated-link")));
    }

    @Test
    void findProjectReturnsCorrectProject() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        when(projectRepository.findByAllowedById(eq(testUserId), eq(testProjectId))).thenReturn(Optional.of(testProject));
        when(postRepository.countAllByProjectId(eq(testProjectId))).thenReturn(0);

        ProjectResponseDto response = projectService.findProject(testProjectId);

        assertNotNull(response);
        assertEquals("Test Project", response.name());
    }

    @Test
    void deleteProjectSuccessfully() {
        when(authoritiesService.receiveCurrentUserId()).thenReturn(testUserId.toString());
        when(authoritiesService.receiveCurrentUserId()).thenReturn(testUserId.toString());
        when(projectRepository.findByAllowedById(eq(testUserId), any(UUID.class))).thenReturn(Optional.of(testProject));

        assertDoesNotThrow(() -> projectService.deleteProject(testProjectId));
    }

    @Test
    void deleteProjectThrowsDataNotFoundExceptionWhenProjectDoesNotExist() {
        when(authoritiesService.receiveCurrentUserId()).thenReturn(testUserId.toString());
        when(projectRepository.findByAllowedById(eq(testUserId), any(UUID.class))).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> projectService.deleteProject(testProjectId));
    }

    @Test
    void updateProjectSuccessfully() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        when(projectRepository.findByAllowedById(eq(testUserId), any(UUID.class))).thenReturn(Optional.of(testProject));
        when(projectRepository.findByLinkAndOwnerId(anyString(), any())).thenReturn(Optional.empty());

        assertDoesNotThrow(() -> projectService.updateProject(testProjectId, new UpdateProjectRequestDto("Updated Project", "unique-link")));
    }

    @Test
    void updateProjectThrowsDataNotFoundExceptionWhenProjectDoesNotExist() {
        when(userService.findCurrentUser()).thenReturn(testUser);
        when(projectRepository.findByAllowedById(eq(testUserId), any(UUID.class))).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> projectService.updateProject(testProjectId, new UpdateProjectRequestDto("Updated Project", "updated-link")));
    }

    @Test
    void updateProjectWithDuplicateLinkThrowsDataExistException() {
        Project anotherProject = new Project();
        anotherProject.setId(UUID.randomUUID());
        when(userService.findCurrentUser()).thenReturn(testUser);
        when(projectRepository.findByAllowedById(eq(testUserId), any(UUID.class))).thenReturn(Optional.of(testProject));
        when(projectRepository.findByLinkAndOwnerId(eq("duplicate-link"), any())).thenReturn(Optional.of(anotherProject));

        assertThrows(DataExistException.class, () -> projectService.updateProject(testProjectId, new UpdateProjectRequestDto("Updated Project", "duplicate-link")));
    }
}
