package ua.com.webbrains.mimosa.cms.unit.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.NotModified304Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataExistException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.generated.dto.DeleteProfileRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.UpdateUserInformationRequestDto;
import ua.com.webbrains.mimosa.cms.models.mappers.UserMapper;
import ua.com.webbrains.mimosa.cms.repositories.UserRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.PostsService;
import ua.com.webbrains.mimosa.cms.services.RegistrationService;
import ua.com.webbrains.mimosa.cms.services.impl.UserServiceImpl;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private AuthoritiesService authoritiesService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private RegistrationService registrationService;
    @Mock
    private PostsService postsService;

    @Spy
    private UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    @InjectMocks
    private UserServiceImpl profileService;

    private final String userId = UUID.randomUUID().toString();
    private User user;

    @BeforeEach
    void setUp() {
        userMapper = Mockito.spy(Mappers.getMapper(UserMapper.class));

        user = new User();
        user.setId(UUID.fromString(userId));
        user.setEmail("test@example.com");
        user.setPassword("password");
        user.setUsername("username");
        user.setPhoneNumber("1234567890");
        user.setName("name");
        user.setSurname("surname");
        user.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));

        when(authoritiesService.receiveCurrentUserId()).thenReturn(userId);
        when(userRepository.findById(UUID.fromString(userId))).thenReturn(Optional.of(user));
    }

    @Test
    void findAuthenticatedUserInfoReturnsCorrectInfo() {
        assertNotNull(profileService.findAuthenticatedUserInfo());
    }

    @Test
    void updateUserInformationWithNoChangesThrowsNotModifiedException() {
        UpdateUserInformationRequestDto dto = new UpdateUserInformationRequestDto("username", "test@example.com", "1234567890", "name", "surname");
        assertThrows(NotModified304Exception.class, () -> profileService.updateUserInformation(dto));
    }

    @Test
    void updateUserInformationWithNewEmailSavesUserAndSendsVerificationEmail() {
        UpdateUserInformationRequestDto dto =
                new UpdateUserInformationRequestDto("newemail@example.com", "username", "name", "surname", "1234567890");
        when(userRepository.save(any(User.class))).thenReturn(user);
        profileService.updateUserInformation(dto);
        verify(registrationService, times(1)).sendEmailVerificationLetterToUser(any(User.class));
    }

    @Test
    void deleteUserProfileWithIncorrectPasswordThrowsForbiddenException() {
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);
        DeleteProfileRequestDto deleteProfileRequestDto = new DeleteProfileRequestDto("wrongpassword");
        assertThrows(Forbidden403Exception.class, () -> profileService.deleteCurrentUserProfile(deleteProfileRequestDto));
    }

    @Test
    void findCurrentUserReturnsUserWhenFound() {
        when(authoritiesService.receiveCurrentUserId()).thenReturn(userId);
        when(userRepository.findById(UUID.fromString(userId))).thenReturn(Optional.of(user));
        User foundUser = profileService.findCurrentUser();
        assertNotNull(foundUser);
        assertEquals(userId, foundUser.getId().toString());
    }

    @Test
    void findCurrentUserThrowsDataNotFoundExceptionWhenUserNotFound() {
        when(authoritiesService.receiveCurrentUserId()).thenReturn(userId);
        when(userRepository.findById(UUID.fromString(userId))).thenReturn(Optional.empty());
        assertThrows(DataNotFoundException.class, () -> profileService.findCurrentUser());
    }

    @Test
    void updateUserInformationWithExistingUsernameThrowsDataExistException() {
        when(authoritiesService.receiveCurrentUserId()).thenReturn(userId);
        when(userRepository.findById(UUID.fromString(userId))).thenReturn(Optional.of(user));
        UpdateUserInformationRequestDto dto = new UpdateUserInformationRequestDto("username", "existingemail@example.com", "1234567890", "name", "surname");
        User user1 = User.builder().username("username").email("existingemail@example.com").id(UUID.randomUUID()).build();
        when(userRepository.findByEmail(dto.email())).thenReturn(Optional.of(user1));
        assertThrows(DataExistException.class, () -> profileService.updateUserInformation(dto));
    }

    @Test
    void updateUserInformationWithExistingEmailThrowsDataExistException() {
        when(authoritiesService.receiveCurrentUserId()).thenReturn(userId);
        when(userRepository.findById(UUID.fromString(userId))).thenReturn(Optional.of(user));
        UpdateUserInformationRequestDto dto = new UpdateUserInformationRequestDto("username", "existingemail@example.com", "name", "surname", "1234567890");
        User user1 = User.builder().username("username").email("existingemail@example.com").id(UUID.randomUUID()).build();
        when(userRepository.findByEmail(dto.email())).thenReturn(Optional.empty());
        when(userRepository.findByUsername(dto.username())).thenReturn(Optional.of(user1));
        assertThrows(DataExistException.class, () -> profileService.updateUserInformation(dto));
    }
}
