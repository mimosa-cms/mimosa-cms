package ua.com.webbrains.mimosa.cms.integration.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ua.com.webbrains.mimosa.cms.generated.dto.CreateProjectRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImagesMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.LinkDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostDto;
import ua.com.webbrains.mimosa.cms.generated.dto.PostRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ProjectResponseDto;
import ua.com.webbrains.mimosa.cms.integration.config.ItConfig;
import ua.com.webbrains.mimosa.cms.integration.testservices.TestUserProvider;
import ua.com.webbrains.mimosa.cms.services.EmailService;
import ua.com.webbrains.mimosa.cms.services.PolicyService;

import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Disabled
public class ImageControllerTest extends ItConfig {
    private static final String IMAGE_RESOURCE_PATH = "files/testImg.jpg";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TestUserProvider testUserProvider;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private EmailService emailService;
    @MockBean
    private PolicyService policyService;

    @Test
    public void testAvatarUpload() throws Exception {
        ClassPathResource classPathResource = new ClassPathResource(IMAGE_RESOURCE_PATH);
        MockMultipartFile multipartFile = new MockMultipartFile(
                "avatar",
                classPathResource.getFilename(),
                MediaType.IMAGE_JPEG_VALUE,
                classPathResource.getInputStream().readAllBytes()
        );
        String token = testUserProvider.getUserToken();
        mockMvc.perform(multipart("/api/v1/profiles/profile/avatar")
                .file(multipartFile)
                .content(MediaType.IMAGE_JPEG_VALUE)
                .header("Authorization", "Bearer " + token)
        ).andExpect(status().isOk());
        MvcResult result = mockMvc.perform(get("/api/v1/profiles/profile/avatar")
                .header("Authorization", "Bearer " + token)
        ).andExpect(status().isOk()).andReturn();
        Assertions.assertArrayEquals(result.getResponse().getContentAsByteArray(), classPathResource.getInputStream().readAllBytes());
        mockMvc.perform(delete("/api/v1/profiles/profile/avatar")
                .header("Authorization", "Bearer " + token)
        ).andExpect(status().isOk());
    }

    @Test
    public void testImageUpload() throws Exception {
        ClassPathResource classPathResource = new ClassPathResource(IMAGE_RESOURCE_PATH);
        MockMultipartFile multipartFile = new MockMultipartFile(
                "images",
                classPathResource.getFilename(),
                MediaType.IMAGE_JPEG_VALUE,
                classPathResource.getInputStream().readAllBytes()
        );
        CreateProjectRequestDto createProjectRequestDto = CreateProjectRequestDto.builder()
                .name("Test Project")
                .link("http://example.com")
                .build();
        PostRequestDto postRequestDto = createPostRequestDto();
        String token = testUserProvider.getUserToken();

        MvcResult proejctResult = mockMvc.perform(post("/api/v1/projects")
                .header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createProjectRequestDto))
        ).andExpect(status().isCreated()).andReturn();
        UUID projectId = objectMapper.readValue(proejctResult.getResponse().getContentAsString(), ProjectResponseDto.class).id();

        MvcResult postResult = mockMvc.perform(post("/api/v1/projects/%s/posts".formatted(projectId))
                .header("Authorization", "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(postRequestDto))
        ).andExpect(status().isCreated()).andReturn();

        PostDto createdPost = objectMapper.readValue(postResult.getResponse().getContentAsString(), PostDto.class);
        UUID postId = createdPost.id();

        MvcResult imageResult = mockMvc.perform(multipart("/api/v1/projects/%s/posts/%s/images".formatted(projectId, postId))
                .file(multipartFile)
                .content(MediaType.MULTIPART_FORM_DATA_VALUE)
                .header("Authorization", "Bearer " + token)
        ).andExpect(status().isCreated()).andReturn();
        UUID imageId = objectMapper.readValue(imageResult.getResponse().getContentAsString(), ImagesMetaResponseDto.class).images().get(0).id();

        MvcResult result = mockMvc.perform(get("/api/v1/projects/%s/posts/%s/images/%s".formatted(projectId, postId, imageId))
                .header("Authorization", "Bearer " + token)
        ).andExpect(status().isOk()).andReturn();
        Assertions.assertArrayEquals(result.getResponse().getContentAsByteArray(), classPathResource.getInputStream().readAllBytes());
    }

    private PostRequestDto createPostRequestDto() {
        return PostRequestDto.builder()
                .category("Technology")
                .tags(Arrays.asList("Java", "Spring", "Programming"))
                .metaWords("Java, Spring, Programming")
                .metaDescription("A detailed post about Java and Spring framework.")
                .metaAuthors("John Doe")
                .text("""
                        This is a comprehensive guide on Java and Spring framework...
                        This is a comprehensive guide on Java and Spring framework...
                        This is a comprehensive guide on Java and Spring framework...
                        This is a comprehensive guide on Java and Spring framework...
                        This is a comprehensive guide on Java and Spring framework...
                        This is a comprehensive guide on Java and Spring framework...
                        This is a comprehensive guide on Java and Spring framework...
                        """)
                .title("Java and Spring Framework")
                .description("An in-depth look at Java and Spring framework.")
                .authors("John Doe")
                .postStatus("PUBLISHED")
                .links(Arrays.asList(LinkDto.builder()
                        .value("https://example.com")
                        .name("Example Link")
                        .linkCategory("none")
                        .build()))
                .build();
    }
}
