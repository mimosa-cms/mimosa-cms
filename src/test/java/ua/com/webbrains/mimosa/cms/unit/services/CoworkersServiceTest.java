package ua.com.webbrains.mimosa.cms.unit.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Forbidden403Exception;
import ua.com.webbrains.mimosa.cms.repositories.InviteRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Invite;
import ua.com.webbrains.mimosa.cms.repositories.entities.Project;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.EmailService;
import ua.com.webbrains.mimosa.cms.services.ProjectService;
import ua.com.webbrains.mimosa.cms.services.UserService;
import ua.com.webbrains.mimosa.cms.services.impl.CoworkersServiceImpl;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CoworkersServiceTest {
    @Mock
    private ProjectService projectService;
    @Mock
    private InviteRepository inviteRepository;
    @Mock
    private UserService userService;
    @Mock
    private EmailService emailService;
    @Mock
    private TemplateEngine templateEngine;

    @InjectMocks
    private CoworkersServiceImpl coworkersService;

    private UUID projectId;
    private UUID inviteToken;
    private User user;
    private User guestUser;
    private Project project;
    private Invite invite;

    @BeforeEach
    void setUp() {
        projectId = UUID.randomUUID();
        inviteToken = UUID.randomUUID();
        user = new User();
        user.setId(UUID.randomUUID());
        user.setUsername("testUser");
        user.setEmail("test@tets.com");
        project = new Project();
        project.setCoworkers(new ArrayList<>());
        project.setId(projectId);
        guestUser = new User();
        guestUser.setId(UUID.randomUUID());
        guestUser.setUsername("testUser1");
        guestUser.setEmail("test1@test.com");
        invite = new Invite();
        invite.setToken(inviteToken);
        invite.setProject(project);
        invite.setUser(guestUser);
    }

    @Test
    void acceptInviteSuccessfully() {
        when(userService.findCurrentUser()).thenReturn(user);
        invite.setUser(user);
        when(inviteRepository.findById(any(UUID.class))).thenReturn(Optional.of(invite));

        coworkersService.acceptInvite(inviteToken);

        verify(inviteRepository).delete(invite);
    }

    @Test
    void acceptInviteThrowsForbidden403Exception() {
        when(userService.findCurrentUser()).thenReturn(user);

        when(inviteRepository.findById(any(UUID.class))).thenReturn(Optional.of(invite));
        invite.setUser(guestUser);

        assertThrows(Forbidden403Exception.class, () -> coworkersService.acceptInvite(inviteToken));
    }

    @Test
    void addCoworkerToProjectSuccessfully() {
        when(templateEngine.process(anyString(), any(Context.class))).thenReturn("test");
        when(userService.findCurrentUser()).thenReturn(user);
        when(projectService.findProjectByIdAndOwnerId(any(UUID.class), any(UUID.class))).thenReturn(project);
        when(userService.findCurrentUser()).thenReturn(user);
        when(userService.findUserByUsernameOrEmail(anyString())).thenReturn(guestUser);
        when(inviteRepository.findInviteByProjectIdAndUserId(any(UUID.class), any(UUID.class))).thenReturn(Optional.empty());

        coworkersService.addCoworkerToProject(projectId, "testUser");

        verify(inviteRepository).save(any(Invite.class));
        verify(emailService).sendEmail(anyString(), anyString(), anyString());
    }

    @Test
    void addCoworkerToProjectThrowsForbidden403Exception() {
        when(userService.findCurrentUser()).thenReturn(user);
        when(userService.findUserByUsernameOrEmail(anyString())).thenReturn(guestUser);
        when(projectService.findProjectByIdAndOwnerId(any(UUID.class), any(UUID.class))).thenReturn(project);
        when(inviteRepository.findInviteByProjectIdAndUserId(any(UUID.class), any(UUID.class))).thenReturn(Optional.of(invite));

        assertThrows(Forbidden403Exception.class, () -> coworkersService.addCoworkerToProject(projectId, "testUser"));
    }

    @Test
    void deleteCoworkerFromProjectSuccessfully() {
        when(userService.findCurrentUser()).thenReturn(user);
        when(projectService.findProjectByIdAndOwnerId(any(UUID.class), any(UUID.class))).thenReturn(project);
        when(userService.findUserByUsernameOrEmail(anyString())).thenReturn(user);

        coworkersService.deleteCoworkerFromProject(projectId, "testUser");

        verify(projectService).findProjectByIdAndOwnerId(any(UUID.class), any(UUID.class));
    }

    @Test
    void getProjectCoworkersSuccessfully() {
        when(userService.findCurrentUser()).thenReturn(user);
        when(projectService.findProjectByIdAndOwnerId(any(UUID.class), any(UUID.class))).thenReturn(project);

        var response = coworkersService.getProjectCoworkers(projectId);

        assertNotNull(response);
        verify(projectService).findProjectByIdAndOwnerId(any(UUID.class), any(UUID.class));
    }
}
