package ua.com.webbrains.mimosa.cms.unit.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.AvatarMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImageMetaResponseDto;
import ua.com.webbrains.mimosa.cms.generated.dto.ImagesMetaResponseDto;
import ua.com.webbrains.mimosa.cms.models.mappers.ImageMapper;
import ua.com.webbrains.mimosa.cms.repositories.AvatarRepository;
import ua.com.webbrains.mimosa.cms.repositories.ImageRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Avatar;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Image;
import ua.com.webbrains.mimosa.cms.repositories.entities.post.Post;
import ua.com.webbrains.mimosa.cms.services.ImageDeleteService;
import ua.com.webbrains.mimosa.cms.services.PostsService;
import ua.com.webbrains.mimosa.cms.services.files.FileService;
import ua.com.webbrains.mimosa.cms.services.impl.ImageServiceImpl;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ImageServiceTest {
    @Mock
    private AuthoritiesService authoritiesService;
    @Mock
    private PostsService postsService;
    @Mock
    private ImageDeleteService imageDeleteService;
    @Mock
    private FileService fileService;
    @Mock
    private ImageRepository imageRepository;
    @Mock
    private AvatarRepository avatarRepository;
    @Mock
    private ImageMapper imageMapper;

    @InjectMocks
    private ImageServiceImpl imageService;

    @Test
    void uploadImages_withValidData_returnsImagesMetaResponseDto() {
        UUID project = UUID.randomUUID();
        UUID post = UUID.randomUUID();
        List<MultipartFile> images = List.of(mock(MultipartFile.class));
        Post currentPost = mock(Post.class);
        Image image = mock(Image.class);
        ImageMetaResponseDto imageMetaResponseDto = mock(ImageMetaResponseDto.class);

        when(postsService.findPostEntityFromProject(project, post)).thenReturn(currentPost);
        when(images.get(0).isEmpty()).thenReturn(false);
        when(images.get(0).getContentType()).thenReturn("image/png");
        when(fileService.uploadPostImage(anyString(), any(MultipartFile.class))).thenReturn("filename");
        when(imageRepository.save(any(Image.class))).thenReturn(image);
        when(imageMapper.entityToDto(any(Image.class))).thenReturn(imageMetaResponseDto);

        ImagesMetaResponseDto result = imageService.uploadImages(project, post, images);

        assertNotNull(result);
        assertEquals(1, result.images().size());
    }

    @Test
    void uploadImages_withEmptyImages_throwsDataNotValidException() {
        UUID project = UUID.randomUUID();
        UUID post = UUID.randomUUID();
        List<MultipartFile> images = List.of();

        assertThrows(DataNotValidException.class, () -> imageService.uploadImages(project, post, images));
    }

    @Test
    void findImagesMeta_withValidData_returnsImagesMetaResponseDto() {
        UUID project = UUID.randomUUID();
        UUID post = UUID.randomUUID();
        Post currentPost = new Post();
        currentPost.setId(post);
        List<Image> postImages = List.of(mock(Image.class));
        ImageMetaResponseDto imageMetaResponseDto = mock(ImageMetaResponseDto.class);

        when(postsService.findPostEntityFromProject(project, post)).thenReturn(currentPost);
        when(imageRepository.findAllByPostId(any(UUID.class))).thenReturn(postImages);
        when(imageMapper.entityToDto(any(Image.class))).thenReturn(imageMetaResponseDto);

        ImagesMetaResponseDto result = imageService.findImagesMeta(project, post);

        assertNotNull(result);
        assertEquals(1, result.images().size());
    }

    @Test
    void findImageMeta_withValidData_returnsImageMetaResponseDto() {
        UUID project = UUID.randomUUID();
        UUID postId = UUID.randomUUID();
        Post post = new Post();
        post.setId(postId);
        Image imageEntity = mock(Image.class);
        ImageMetaResponseDto imageMetaResponseDto = mock(ImageMetaResponseDto.class);

        when(postsService.findPostEntityFromProject(project, postId)).thenReturn(post);
        when(imageRepository.findImageByIdAndPostId(any(UUID.class), any(UUID.class))).thenReturn(Optional.of(imageEntity));
        when(imageMapper.entityToDto(any(Image.class))).thenReturn(imageMetaResponseDto);

        UUID image = UUID.randomUUID();
        ImageMetaResponseDto result = imageService.findImageMeta(project, postId, image);

        assertNotNull(result);
    }

    @Test
    void findImageMeta_withInvalidImage_throwsDataNotFoundException() {
        UUID project = UUID.randomUUID();
        UUID postId = UUID.randomUUID();
        Post post = new Post();
        post.setId(postId);

        when(postsService.findPostEntityFromProject(project, postId)).thenReturn(post);
        when(imageRepository.findImageByIdAndPostId(any(UUID.class), any(UUID.class))).thenReturn(Optional.empty());

        UUID image = UUID.randomUUID();
        assertThrows(DataNotFoundException.class, () -> imageService.findImageMeta(project, postId, image));
    }

    @Test
    void findAvatar_withValidData_returnsResource() {
        UUID currentUserId = UUID.randomUUID();
        Avatar avatar = new Avatar().setFilename("SomefileName");
        Resource resource = new ByteArrayResource(new byte[0]);

        when(authoritiesService.receiveCurrentUserId()).thenReturn(currentUserId.toString());
        when(avatarRepository.findByUserId(currentUserId)).thenReturn(Optional.of(avatar));
        when(fileService.retrieveAvatarImage(anyString())).thenReturn(resource);

        Resource result = imageService.findAvatar();

        assertNotNull(result);
    }

    @Test
    void findAvatar_withInvalidUser_throwsDataNotFoundException() {
        UUID currentUserId = UUID.randomUUID();

        when(authoritiesService.receiveCurrentUserId()).thenReturn(currentUserId.toString());
        when(avatarRepository.findByUserId(currentUserId)).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> imageService.findAvatar());
    }

    @Test
    void uploadAvatar_withValidData_returnsAvatarMetaResponseDto() {
        MultipartFile avatar = mock(MultipartFile.class);
        UUID currentUserId = UUID.randomUUID();
        Avatar avatarEntity = mock(Avatar.class);
        AvatarMetaResponseDto avatarMetaResponseDto = mock(AvatarMetaResponseDto.class);

        when(authoritiesService.receiveCurrentUserId()).thenReturn(currentUserId.toString());
        when(avatar.getContentType()).thenReturn("image/png");
        when(fileService.uploadAvatarImage(anyString(), any(MultipartFile.class))).thenReturn("filename");
        when(avatarRepository.save(any(Avatar.class))).thenReturn(avatarEntity);
        when(imageMapper.entityToDto(any(Avatar.class))).thenReturn(avatarMetaResponseDto);

        AvatarMetaResponseDto result = imageService.uploadAvatar(avatar);

        assertNotNull(result);
    }

    @Test
    void uploadAvatar_withEmptyAvatar_throwsDataNotValidException() {
        MultipartFile avatar = mock(MultipartFile.class);

        when(avatar.isEmpty()).thenReturn(true);

        assertThrows(DataNotValidException.class, () -> imageService.uploadAvatar(avatar));
    }
}
