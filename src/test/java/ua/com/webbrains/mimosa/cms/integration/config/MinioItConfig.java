package ua.com.webbrains.mimosa.cms.integration.config;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.testcontainers.containers.MinIOContainer;
import org.testcontainers.utility.DockerImageName;

@TestConfiguration
@Profile("test")
public class MinioItConfig {
    private static final int MINIO_PORT = 9000;

    @Value("${testcontainers.images.minio}")
    private String minioImage;
    @Value("${minio.access.name}")
    private String minioAccessKey;
    @Value("${minio.access.secret}")
    private String minioSecretKey;
    @Value("${minio.bucket.name}")
    private String bucketName;

    @Bean
    MinIOContainer minioContainer() {
        MinIOContainer minIoContainer = new MinIOContainer(DockerImageName.parse(minioImage))
                .withUserName(minioAccessKey).withPassword(minioSecretKey)
                .withExposedPorts(MINIO_PORT);
        minIoContainer.start();
        return minIoContainer;
    }

    @Bean
    MinioClient minioClient(MinIOContainer minIoContainer) throws Exception {
        MinioClient minioClient = MinioClient.builder()
                .endpoint("http://%s:%s".formatted(minIoContainer.getHost(), minIoContainer.getMappedPort(MINIO_PORT)))
                .credentials(minioAccessKey, minioSecretKey)
                .build();
        if (!minioClient.bucketExists(BucketExistsArgs.builder()
                .bucket(bucketName).build())) {
            minioClient.makeBucket(MakeBucketArgs.builder()
                    .bucket(bucketName).build());
        }
        return minioClient;
    }
}
