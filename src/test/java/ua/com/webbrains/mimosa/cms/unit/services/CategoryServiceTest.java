package ua.com.webbrains.mimosa.cms.unit.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.OperationFailedException;
import ua.com.webbrains.mimosa.cms.generated.dto.PostCategoriesResponseDto;
import ua.com.webbrains.mimosa.cms.repositories.CategoryRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.Category;
import ua.com.webbrains.mimosa.cms.services.impl.CategoryServiceImpl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {
    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private CategoryServiceImpl categoryService;

    @Test
    void loadCategoriesSuccessfully() throws Exception {
        // Given
        when(objectMapper.readValue(any(InputStream.class), eq(List.class))).thenReturn(Arrays.asList("Category1", "Category2"));
        doNothing().when(categoryRepository).deleteAll();
        doAnswer(invocation -> invocation.getArgument(0)).when(categoryRepository).saveAll(any());

        // When
        categoryService.loadCategories();

        // Then
        verify(categoryRepository, times(1)).deleteAll();
        verify(categoryRepository, times(1)).saveAll(any());
    }

    @Test
    void loadCategoriesWithIoException() throws Exception {
        // Given
        when(objectMapper.readValue(any(InputStream.class), eq(List.class))).thenThrow(new IOException());

        // When & Then
        assertThrows(OperationFailedException.class, () -> categoryService.loadCategories());
    }

    @Test
    void retrieveCategoriesWithNonEmptySearch() {
        // Given
        String search = "test";
        Page<Category> expectedPage = new PageImpl<>(Collections.singletonList(new Category(UUID.randomUUID(), "test")));
        when(categoryRepository.findAllByNameLike(eq(search), any(PageRequest.class))).thenReturn(expectedPage);

        // When
        PostCategoriesResponseDto result = categoryService.retrieveCategories(0, 10, search);

        // Then
        assertNotNull(result);
        assertEquals(1, result.total());
        assertEquals(Collections.singletonList("test"), result.categories());
    }

    @Test
    void retrieveCategoriesWithEmptySearch() {
        // Given
        Page<Category> expectedPage = new PageImpl<>(Arrays.asList(new Category(UUID.randomUUID(), "Category1"),
                new Category(UUID.randomUUID(), "Category2")));
        when(categoryRepository.findAll(any(PageRequest.class))).thenReturn(expectedPage);

        // When
        PostCategoriesResponseDto result = categoryService.retrieveCategories(0, 10, "");

        // Then
        assertNotNull(result);
        assertEquals(1, result.total());
        assertEquals(Arrays.asList("Category1", "Category2"), result.categories());
    }

    @Test
    void retrieveCategoriesWithNullSearch() {
        // Given
        Page<Category> expectedPage = new PageImpl<>(Arrays.asList(new Category(UUID.randomUUID(), "Category1"),
                new Category(UUID.randomUUID(), "Category2")));
        when(categoryRepository.findAll(any(PageRequest.class))).thenReturn(expectedPage);

        // When
        PostCategoriesResponseDto result = categoryService.retrieveCategories(0, 10, null);

        // Then
        assertNotNull(result);
        assertEquals(1, result.total());
        assertEquals(Arrays.asList("Category1", "Category2"), result.categories());
    }
}
