package ua.com.webbrains.mimosa.cms.unit.services;

import jakarta.mail.MessagingException;
import jakarta.mail.Session;
import jakarta.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.util.ReflectionTestUtils;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.OperationFailedException;
import ua.com.webbrains.mimosa.cms.services.impl.EmailServiceImpl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTest {
    @Mock
    private JavaMailSender javaMailSender;

    @InjectMocks
    private EmailServiceImpl emailService;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(emailService, "sendFrom", "noreply@example.com");
    }

    @Test
    void shouldSendEmailSuccessfully() throws Exception {
        String emailTo = "test@example.com";
        String subject = "Test Subject";
        String content = "Test Content";
        MimeMessage mimeMessage = new MimeMessage((Session) null);
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        emailService.sendEmail(emailTo, subject, content);

        verify(javaMailSender, times(1)).send(mimeMessage);
    }

    @Test
    void shouldThrowExceptionWhenEmailSendingFails() throws Exception {
        String emailTo = "test@example.com";
        String subject = "Test Subject";
        String content = "Test Content";
        MimeMessage mimeMessage = new MimeMessage((Session) null);
        when(javaMailSender.createMimeMessage()).thenReturn(mimeMessage);
        doAnswer((invocationOnMock) -> {
            throw new MessagingException("Test Exception");
        }).when(javaMailSender).send(any(MimeMessage.class));

        assertThrows(OperationFailedException.class, () -> emailService.sendEmail(emailTo, subject, content));
    }
}
