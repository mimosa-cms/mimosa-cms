package ua.com.webbrains.mimosa.cms.unit.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import ua.com.webbrains.mimosa.cms.commons.exceptions.Conflict409Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.TooManyRequests429Exception;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataExistException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotFoundException;
import ua.com.webbrains.mimosa.cms.commons.exceptions.impl.DataNotValidException;
import ua.com.webbrains.mimosa.cms.generated.dto.EmailApproveRequestDto;
import ua.com.webbrains.mimosa.cms.generated.dto.RegisterUserRequestDto;
import ua.com.webbrains.mimosa.cms.models.mappers.UserMapper;
import ua.com.webbrains.mimosa.cms.repositories.EmailVerificationRepository;
import ua.com.webbrains.mimosa.cms.repositories.UserRepository;
import ua.com.webbrains.mimosa.cms.repositories.entities.EmailVerification;
import ua.com.webbrains.mimosa.cms.repositories.entities.User;
import ua.com.webbrains.mimosa.cms.services.EmailService;
import ua.com.webbrains.mimosa.cms.services.impl.RegistrationServiceImpl;
import ua.com.webbrains.mimosa.cms.services.security.AuthoritiesService;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RegistrationServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private EmailVerificationRepository emailVerificationRepository;
    @Mock
    private EmailService emailService;
    @Mock
    private TemplateEngine templateEngine;
    @Mock
    private AuthoritiesService authoritiesService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Spy
    private UserMapper userRegisterRequestDtoToEntity = Mappers.getMapper(UserMapper.class);

    @InjectMocks
    private RegistrationServiceImpl registrationService;

    @BeforeEach
    void setUp() {
        userRegisterRequestDtoToEntity = Mockito.spy(Mappers.getMapper(UserMapper.class));
    }

    @Test
    void registerUserSuccessfullyCreatesNewUser() {
        Mockito.when(userRepository.findByUsernameOrEmail(Mockito.anyString(), Mockito.anyString())).thenReturn(Optional.empty());
        Mockito.when(passwordEncoder.encode(any(String.class))).thenReturn("encodedPassword");
        Mockito.when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

        String emailTemplate = "emailTemplate";
        Mockito.when(templateEngine.process(any(String.class), any(Context.class))).thenReturn(emailTemplate);

        RegisterUserRequestDto requestDto = RegisterUserRequestDto.builder()
                .username("username")
                .email("email@example.com")
                .password("password")
                .build();

        registrationService.registerUser(requestDto);

        Mockito.verify(userRepository).save(any(User.class));
        Mockito.verify(emailService).sendEmail(eq("email@example.com"), Mockito.anyString(), eq(emailTemplate));
    }

    @Test
    void registerUserFailsWhenUserAlreadyExists() {
        Mockito.when(userRepository.findByUsernameOrEmail(Mockito.anyString(), Mockito.anyString())).thenReturn(Optional.of(new User()));

        RegisterUserRequestDto requestDto = RegisterUserRequestDto.builder()
                .username("username")
                .email("email@example.com")
                .password("password")
                .build();

        assertThrows(DataExistException.class, () -> registrationService.registerUser(requestDto));
    }

    @Test
    void sendEmailVerificationLetterToUserSendsEmailWhenNotPreviouslyVerified() {
        User user = new User();
        user.setEmail("email@example.com");
        user.setIsEmailApproved(false);

        Mockito.when(emailVerificationRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.empty());

        String emailTemplate = "emailTemplate";
        Mockito.when(templateEngine.process(any(String.class), any(Context.class))).thenReturn(emailTemplate);

        registrationService.sendEmailVerificationLetterToUser(user);

        Mockito.verify(emailService).sendEmail(eq("email@example.com"), Mockito.anyString(), Mockito.anyString());
    }

    @Test
    void sendEmailVerificationLetterToUserThrowsConflictWhenEmailAlreadyVerified() {
        User user = new User();
        user.setEmail("email@example.com");
        user.setIsEmailApproved(true);

        assertThrows(Conflict409Exception.class, () -> registrationService.sendEmailVerificationLetterToUser(user));
    }

    @Test
    void approveUserEmailSuccessfullyApprovesEmail() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setEmail("email@example.com");
        user.setIsEmailApproved(false);

        EmailVerification emailVerification = new EmailVerification();
        emailVerification.setEmail("email@example.com");
        emailVerification.setVerificationToken(UUID.randomUUID());

        Mockito.when(authoritiesService.receiveCurrentUserId()).thenReturn(user.getId().toString());
        Mockito.when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));
        Mockito.when(emailVerificationRepository.findByVerificationToken(any(UUID.class))).thenReturn(Optional.of(emailVerification));

        EmailApproveRequestDto requestDto = new EmailApproveRequestDto(emailVerification.getVerificationToken());
        registrationService.approveUserEmail(requestDto);

        Mockito.verify(emailVerificationRepository).delete(any(EmailVerification.class));
        assertTrue(user.getIsEmailApproved());
    }

    @Test
    void approveUserEmailThrowsDataNotFoundExceptionWhenTokenNotFound() {
        UUID randomUserId = UUID.randomUUID();
        UUID randomToken = UUID.randomUUID();
        Mockito.when(authoritiesService.receiveCurrentUserId()).thenReturn(randomUserId.toString());
        Mockito.when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(new User()));
        Mockito.when(emailVerificationRepository.findByVerificationToken(any(UUID.class))).thenReturn(Optional.empty());

        EmailApproveRequestDto requestDto = new EmailApproveRequestDto(randomToken);

        assertThrows(DataNotFoundException.class, () -> registrationService.approveUserEmail(requestDto));
    }

    @Test
    void approveUserEmail_EmailAlreadyVerified_ThrowsConflict409Exception() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setEmail("email@example.com");
        user.setIsEmailApproved(true);

        EmailVerification emailVerification = new EmailVerification();
        emailVerification.setEmail("email@example.com");
        emailVerification.setVerificationToken(UUID.randomUUID());

        Mockito.when(authoritiesService.receiveCurrentUserId()).thenReturn(user.getId().toString());
        Mockito.when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));
        Mockito.when(emailVerificationRepository.findByVerificationToken(any(UUID.class))).thenReturn(Optional.of(emailVerification));

        assertThrows(Conflict409Exception.class, () -> registrationService.approveUserEmail(new EmailApproveRequestDto(emailVerification.getVerificationToken())));
    }

    @Test
    void approveUserEmail_TokenNotValid_ThrowsDataNotValidException() {
        User user = new User();
        user.setId(UUID.randomUUID());
        user.setEmail("email@example.com");
        user.setIsEmailApproved(false);

        EmailVerification emailVerification = new EmailVerification();
        emailVerification.setEmail("email1@example.com");
        emailVerification.setVerificationToken(UUID.randomUUID());

        Mockito.when(authoritiesService.receiveCurrentUserId()).thenReturn(user.getId().toString());
        Mockito.when(userRepository.findById(any(UUID.class))).thenReturn(Optional.of(user));
        Mockito.when(emailVerificationRepository.findByVerificationToken(any(UUID.class))).thenReturn(Optional.of(emailVerification));

        assertThrows(DataNotValidException.class, () -> registrationService.approveUserEmail(new EmailApproveRequestDto(emailVerification.getVerificationToken())));
    }

    @Test
    void sendEmailVerificationLetter_UserNotFound_ThrowsDataNotFoundException() {
        when(authoritiesService.receiveCurrentUserId()).thenReturn(UUID.randomUUID().toString());
        when(userRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> registrationService.sendEmailVerificationLetter());
    }

    @Test
    void sendEmailVerificationLetterToUser_EmailAlreadyVerified_ThrowsConflict409Exception() {
        User user = new User();
        user.setEmail("email@example.com");
        user.setIsEmailApproved(true);

        assertThrows(Conflict409Exception.class, () -> registrationService.sendEmailVerificationLetterToUser(user));
    }

    @Test
    void sendEmailVerificationLetterToUser_TooManyRequests_ThrowsTooManyRequests429Exception() {
        User user = new User();
        user.setEmail("email@example.com");
        user.setIsEmailApproved(false);

        EmailVerification emailVerification = new EmailVerification();
        emailVerification.setEmail(user.getEmail());
        emailVerification.setSendAt(Timestamp.valueOf(LocalDateTime.now()));

        when(emailVerificationRepository.findByEmail(anyString())).thenReturn(Optional.of(emailVerification));

        assertThrows(TooManyRequests429Exception.class, () -> registrationService.sendEmailVerificationLetterToUser(user));
    }
}