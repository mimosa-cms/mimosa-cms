# Mimosa CMS API
`Monolithic version`

Free CMS, that is easy to use!

## Features:

- Convenient panel for managing projects and posts.
- Easy to use, documented API for integration with your websites.
- Analytics for your projects and posts that you can export.
- Customizable template maker to view your project and posts.

## How to run?

- Prepare .env file with needed secret properties that required to start up the project (checkout `.env-example` file)
- Use `docker-compose --env-file .your-env -f docker/docker-compose.yaml up --build` command in the project root
  directory to start up backend server and it's dependencies.
  Server available at port `localhost:8080`.
- To run project locally, enable services as described in `application-local.yaml`
  at resource folder or at `.env-local-example` file and set env variable `SPRING_PROFILES-ACTIVE` to `local` value.

> If you running project locally, and enabled `local` profile, docker compose will run automatically, when you start the compiled app, just ensure that you running `java --jar` command from the same dir as your compose file and required env variables are prepared:
> ```text
>MAIL_HOST=smtp-example.com;
>MAIL_PASSWORD=examplePassword;
>MAIL_PORT=587;
>MAIL_SEND_FROM=example@example.com
>MAIL_USERNAME=example@example.com
>```

- You can always change dependencies credentials to your own,
  in .env file or application-local.yaml.
- You can run dependencies for the project and start the app separately by yourself. Just
  run `docker-compose -f docker/dependencies-compose.yaml up --build` command in the project root directory.
- When starting project in local profile (`SPRING_PROFILES_ACTIVE=local`), dependencies from `dependencies-compose.yaml` is started automatically. Just ensure your Docker is currently running.